set -o pipefail

if [ "$(LC_ALL=C type -t current_log_level)" != "function" ]
then
  source /usr/local/lib/helpers/common
fi

set -o pipefail

if [ "$(LC_ALL=C type -t check_oauth_authentication_environment)" != "function" ]
then
  source /usr/local/lib/helpers/oauth_api
fi

check_required_oauth_environment() {
  check_oauth_authentication_environment || return 1
  check_required_environment "OIDC_CLIENT_ID CI_ENVIRONMENT_URL OAUTH_SIGN_IN_PATH" || return 1
}

run_main() {
  check_required_oauth_environment || return 1
  remove_oauth_redirect_uri "${OIDC_CLIENT_ID}" "${CI_ENVIRONMENT_URL}${OAUTH_SIGN_IN_PATH}" || return 1
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
  run_main
  if [ $? -gt 0 ]
  then
    exit 1
  fi
fi