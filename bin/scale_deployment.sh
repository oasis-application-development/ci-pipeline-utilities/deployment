#!/bin/bash
if [ "$(LC_ALL=C type -t current_log_level)" != "function" ]
then
  source /usr/local/lib/helpers/common
fi

if [ "$(LC_ALL=C type -t check_required_cluster_login_environment)" != "function" ]
then
  source /usr/local/lib/helpers/helm
fi

if [ "$(LC_ALL=C type -t check_required_project_environment)" != "function" ]
then
  source /usr/local/lib/helpers/deploy
fi

check_scale_deployment_requirements() {
  check_orbs_label_requirements || return 1
  check_required_cluster_login_environment || return 1
}

run_main() {
  deployment_image_version_info
  check_scale_deployment_requirements || return 1
  cluster_login
  if [ $? -gt 0 ]
  then
    error "could not login kubectl"
    return 1
  fi
  restart_orbs_deployments "oa_scale_deployment_restart=true" || return 1
  echo "ALL COMPLETE"
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
  run_main
  if [ $? -gt 0 ]
  then
    exit 1
  fi
fi