#!/usr/bin/python

import os
import sys
import getopt
from typing import OrderedDict
import oyaml as yaml
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
from collections import OrderedDict

def build_include():
  return {
      'project': 'utility/project-templates/ci-templates',
      'file': '/docker4.yml'
    }

def get_build_include(gitlab_ci):
  index = 0
  for x in gitlab_ci['include']:
    if x['project'] == 'utility/project-templates/ci-templates':
      return [x, index]
    index += 1
  return [None, None]

def update_build_include(gitlab_ci):
  (current_include, index) = get_build_include(gitlab_ci)
  if index == None:
    gitlab_ci['include'].insert(0, build_include())
  else:
    if current_include['file'] == '/docker4.yml':
      return False
    gitlab_ci['include'][index] = build_include()
  return True

def generate_build_include(gitlab_ci):
  if 'include' in gitlab_ci and len(gitlab_ci['include']) > 0:
    return update_build_include(gitlab_ci)
  else:
    gitlab_ci['include'] = [build_include()]
    return True

def deployment_include():
  return {
      'project': 'orbs/shared/ci-pipeline-utilities/deployment',
      'file': '/deployment.yml',
      'ref': os.environ.get('DEPLOYMENT_IMAGE_VERSION')
    }

def get_deployment_include(gitlab_ci):
  index = 0
  for x in gitlab_ci['include']:
     if x['project'] == 'orbs/shared/ci-pipeline-utilities/deployment':
       return [x, index]
     index += 1
  return [None, None]

def update_deployment_include(gitlab_ci):
  (current_include, index) = get_deployment_include(gitlab_ci)
  if index == None:
    gitlab_ci['include'].append(deployment_include())
  else:
    if current_include['ref'] == os.environ.get('DEPLOYMENT_IMAGE_VERSION'):
      return False
    else:
      gitlab_ci['include'][index] = deployment_include()
  return True

def generate_deployment_include(gitlab_ci):
  if 'include' in gitlab_ci and len(gitlab_ci['include']) > 0:
    return update_deployment_include(gitlab_ci)
  else:
    gitlab_ci['include'] = [deployment_include()]
    return True

def generate_stage(gitlab_ci, stage, prepend=False):
  if 'stages' in gitlab_ci:
    if stage in gitlab_ci['stages']:
      return False
    if prepend:
      gitlab_ci['stages'].insert(0, stage)
    else:
      gitlab_ci['stages'].append(stage)
  else:
    gitlab_ci['stages'] = [stage]  
  return True

def retry_logic():
  return {
    'max': 2,
    'when': 'runner_system_failure'}

def kaniko_build_job():
  return {
    'stage': 'build',
    'before_script':[
        'export THE_IMAGE="${CI_REGISTRY_IMAGE}/${CI_PROJECT_NAME}:${CI_COMMIT_SHORT_SHA}"',
        'export THE_DOCKERFILE="Dockerfile"'],
    'extends': '.kaniko_build',
    'retry': retry_logic()
  }

def generate_build(gitlab_ci):
  changed = generate_stage(gitlab_ci, 'build', True)
  changed = generate_build_include(gitlab_ci) or changed
  if 'build' in gitlab_ci:
    return changed
  gitlab_ci['build'] = kaniko_build_job()
  return True

def deployment_job(name, skip_decom):
  job = OrderedDict({
    'stage': 'deploy',
    'extends': '.deploy',
    'environment':{
      'name': name,
      'url': 'https://${PROJECT_NAMESPACE}-${CI_ENVIRONMENT_SLUG}-${HELM_CHART_NAME}.ocp.dhe.duke.edu'},
    'retry': retry_logic()
  })
  if skip_decom:
    return job
  job['environment']['on_stop'] = f'decommission_{name}'
  return job

def decommission_job(name):
  return {
    'stage': 'deploy',
    'extends': '.decommission',
    'environment':{
      'name': name,
      'action': 'stop'},
    'when': 'manual',
    'retry': retry_logic()
  }

def generate_deploy(gitlab_ci, name, skip_decom):
  changed = generate_stage(gitlab_ci, 'deploy')
  changed = generate_deployment_include(gitlab_ci) or changed
  deploy_job_name = f'deploy_{name}'
  decommission_job_name = f'decommission_{name}'
  if deploy_job_name not in gitlab_ci:
    gitlab_ci[deploy_job_name] = deployment_job(name, skip_decom)
    changed = True
  if skip_decom:
    return changed
  if decommission_job_name not in gitlab_ci:
    gitlab_ci[decommission_job_name] = decommission_job(name)
    changed = True
  return changed

def get_gitlab_ci(gitlab_path):
  if os.path.exists(gitlab_path):
    rstream = open(gitlab_path, 'r')
    gitlab_ci = load(rstream, Loader=Loader)
    rstream.close()
    return gitlab_ci
  else:
    return OrderedDict({'stages':[],'include':[]})

def write_gitlab_ci(gitlab_path, gitlab_ci):
  wstream = open(gitlab_path, 'w+')
  wstream.truncate(0)
  dump(gitlab_ci, wstream)
  wstream.close()

def main():
  usage = f'usage: {sys.argv[0]} [-b|--build] [-d|--deploy name [-s|--skip-decom]]'
  build = False
  deploy = False
  skip_decom = False
  gitlab_path = './.gitlab-ci.yml'

  try:
    opts, _ = getopt.getopt(
      sys.argv[1:],
      'bd:s',
      ["build","deploy","skip-decom"])
  except getopt.GetoptError as err:
    print(err)
    print(usage)
    sys.exit(2)

  if len(opts) < 1:
    print(usage)
    sys.exit()
  
  for o, a in opts:
    if o in ("-b", "--build"):
      build = True
    if o in ("-s", "--skip-decom"):
      skip_decom = True
    if o in ("-d", "--deploy"):
      deploy_name = a
      deploy = True
  
  gitlab_ci = get_gitlab_ci(gitlab_path)

  gitlab_ci_changed = False
  if build or deploy:
    gitlab_ci_changed = generate_build(gitlab_ci)

  if deploy:
    gitlab_ci_changed = generate_deploy(gitlab_ci,deploy_name,skip_decom)

  if gitlab_ci_changed:
    write_gitlab_ci(gitlab_path, gitlab_ci)

if __name__ == "__main__":
  main()

