#!/bin/bash
set -e
tag=$1
target=gitlab.dhe.duke.edu:4567/orbs/shared/ci-pipeline-utilities/deployment:${tag}
if [ -z "${GITLAB_AUTH_TOKEN}" ]
then
  echo "GITLAB_AUTH_TOKEN environment not set" >&2
  exit 1
fi

# check if tag exists
curl -H "Authorization: Bearer ${GITLAB_AUTH_TOKEN}" --fail-with-body "https://gitlab.dhe.duke.edu/api/v4/projects/orbs%2Fshared%2Fci-pipeline-utilities%2Fdeployment/repository/tags/${tag}"

# check if tagged image exists
repository_id=$(curl -H "Authorization: Bearer ${GITLAB_AUTH_TOKEN}" --fail-with-body "https://gitlab.dhe.duke.edu/api/v4/projects/orbs%2Fshared%2Fci-pipeline-utilities%2Fdeployment/registry/repositories" | jq -r '.[0].id')
curl -H "Authorization: Bearer ${GITLAB_AUTH_TOKEN}" --fail-with-body "https://gitlab.dhe.duke.edu/api/v4/projects/orbs%2Fshared%2Fci-pipeline-utilities%2Fdeployment/registry/repositories/${repository_id}/tags/${tag}"

set +e
docker run --rm ${target} bash -c '/bin/cowsay ${DEPLOYMENT_IMAGE_VERSION}' > /dev/null 2>&1
exit_status=$?

set -e
if [ $exit_status -gt 0 ]
then
  docker build -t ${target} -f Dockerfile-cowsay --build-arg TARGET_IMAGE=${target} .
  docker push ${target}
  docker rmi ${target}
  docker run --rm ${target} bash -c '/bin/cowsay ${DEPLOYMENT_IMAGE_VERSION}'
fi
docker rmi ${target}

git fetch origin +refs/tags/${tag}:refs/dep/origin/${tag}
git checkout -b "deprecated-${tag}" refs/dep/origin/${tag}
if [ ! -f deployment.deprecate.patch ]
then
  git show master:deployment.deprecate.patch > deployment.deprecate.patch
fi
patch -i deployment.deprecate.patch deployment.yml
git add deployment.yml
git commit -m 'deprecation warning'
git tag -d ${tag}
git tag -m 'deprecation' ${tag}

echo "removing ${tag} in repo"
curl -H "Authorization: Bearer ${GITLAB_AUTH_TOKEN}" -X DELETE --fail-with-body "https://gitlab.dhe.duke.edu/api/v4/projects/orbs%2Fshared%2Fci-pipeline-utilities%2Fdeployment/repository/tags/${tag}"
git push origin ${tag}

#cleanup
git checkout -f master
git tag -d ${tag}
git branch -D deprecated-${tag}
