#!/bin/bash

synchronize_git() {
  git push oit master --tags
}

new_tags() {
  DHE_TAGS=$(mktemp)
  git ls-remote --tags origin | grep -v '\{\}$' | cut -f2 > $DHE_TAGS
  OIT_TAGS=$(mktemp)
  git ls-remote --tags oit | grep -v '\{\}$' | cut -f2 > $OIT_TAGS
  comm -23 $DHE_TAGS $OIT_TAGS | cut -d'/' -f3
}

synchronize_docker() {
  for tag in $(new_tags)
  do
    echo "pushing new tag ${tag} docker image"
    docker run -ti --rm -v`pwd`:/workdir --workdir /workdir -e REGISTRY_AUTH_FILE=/workdir/auth.nocommit.json quay.io/skopeo/stable:latest copy docker://gitlab.dhe.duke.edu:4567/orbs/shared/ci-pipeline-utilities/deployment:${tag} docker://gitlab-registry.oit.duke.edu/oasis-application-development/ci-pipeline-utilities/deployment:${tag}
  done
}

run_main() {
  git fetch
  synchronize_docker
  synchronize_git
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
  run_main
  if [ $? -gt 0 ]
  then
    exit 1
  fi
fi