if [ "$(LC_ALL=C type -t current_log_level)" != "function" ]
then
  echo "source helpers/common before helpers/helm" >&2
fi

check_required_cluster_login_environment() {
  check_required_environment "HELM_TOKEN HELM_USER PROJECT_NAMESPACE CLUSTER_SERVER" || return 1
}

check_helm_chart_requirements() {
  check_default_environment "HELM_CHART_NAME:CI_PROJECT_NAME" || return 1

  if [ ! -d "helm-chart" ]
  then
    error "this project does not have a helm-chart directory!"
    return 1
  fi

  helm_chart_name="${CI_PROJECT_NAME}"
  if [ -n "${HELM_CHART_NAME}" ]
  then
    helm_chart_name="${HELM_CHART_NAME}"
  fi

  if [ ! -d  "helm-chart/${helm_chart_name}" ]
  then
    error "helm-chart does not have the expected ${helm_chart_name} chart directory!"
    return 1
  fi  
}

helm_chart_name() {
  if [ -n "${HELM_CHART_NAME}" ]
  then
    echo "${HELM_CHART_NAME}"
  else
    echo "${CI_PROJECT_NAME}"
  fi
}

cluster_login() {
  info "authenticating ${HELM_USER} in ${PROJECT_NAMESPACE}"
  dry_run && return

  cluster_name="${CLUSTER_NAME:-ci_kube}"
  context_name="${CONTEXT_NAME:-${HELM_USER}-${cluster_name}-${PROJECT_NAMESPACE}}"
  kubectl config set-cluster ${cluster_name} --server="${CLUSTER_SERVER}" --insecure-skip-tls-verify=true || return 1
  kubectl config set-credentials "${HELM_USER}" --token="${HELM_TOKEN}" || return 1
  kubectl config set-context ${context_name}  --cluster=${cluster_name} --namespace=${PROJECT_NAMESPACE} --user=${HELM_USER} || return 1
  kubectl config use-context ${context_name} || return 1
}

check_orbs_label_requirements() {
  check_required_environment "HELM_CHART_NAME CI_ENVIRONMENT_SLUG" || return 1
}

orbs_labels() {
  echo "oa_application_name=${HELM_CHART_NAME},oa_application_environment=${CI_ENVIRONMENT_SLUG}"
}

orbs_deployment_names() {
  local extra_labels=${1}
  kubectl get deployments -l "$(orbs_labels)${extra_labels:+",${extra_labels}"}" -o=name
}

restart_orbs_deployments() {
  extra_labels=${1}
  restart_deployments=$(orbs_deployment_names ${extra_labels})
  if [ -z "${restart_deployments}" ]
  then
    error "no deployments found with labels $(orbs_labels)${extra_labels:+",${extra_labels}"}"
    return 1
  fi

  for deployment_name in ${restart_deployments}
  do
    info "restarting ${deployment_name}"
    kubectl rollout restart ${deployment_name}
    if [ $? -gt 0 ]
    then
      error 'could not restart deployment'
      return 1
    fi
    kubectl rollout status ${deployment_name}
    if [ $? -gt 0 ]
    then
      error 'could not get deployment restart status'
      return 1
    fi
  done
}
