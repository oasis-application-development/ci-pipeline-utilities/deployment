stages:
  - prevent_rebuild
  - version_parity
  - build
  - test
  - tag
  - publish

variables:
  LOG_LEVEL: "info"
  VERSION_TAG: "v3.4.7"
  CANDIDATE_IMAGE: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}
  FROM_TAG: ${CI_COMMIT_BRANCH}

include:
  - project: 'utility/project-templates/ci-templates'
    file: '/docker-ci.yml'
    ref: 'v2.2'
  - project: 'utility/images/auto-version'
    file: '/auto-version.yml'
    ref: 'v2.4.2'

# if REBUILD is set exit clean
# if tag info does not return 404 not found, exit non zero
prevent_version_overwrite:
  stage: prevent_rebuild
  image: gitlab.dhe.duke.edu:4567/utility/images/basic-shell-executor:latest
  script:
    - if [ -n "${REBUILD}" ]; then exit; fi;
    - echo "curling ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories"
    - repository_id=$(curl ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories | jq -r '.[] | select(.location=="'${CI_REGISTRY_IMAGE}'") | .id')
    - echo "curling repository ${repository_id}"
    - resp=$(curl ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/${repository_id}/tags/${VERSION_TAG})
    - if [[ "${resp}" =~ .*404.* ]]; then exit; fi
    - echo "You must pass REBUILD=1 environment variable to force a rebuild of an existing tag"
    - exit 1
  rules:
    - if: '$CI_COMMIT_BRANCH'
  tags:
    - kube-executor

check_readme_deployment_version:
  stage: version_parity
  image: gitlab.dhe.duke.edu:4567/utility/images/basic-shell-executor:latest
  script:
    - 'grep "Image Version: ${VERSION_TAG}" README.md'
    - grep $(grep 'Image Version' README.md | awk '{print $NF}') deployment.yml
    - echo "README.md Image Version matches deployment.yml tag"
    - repository_id=$(curl ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories | jq -r '.[] | select(.location=="'${CI_REGISTRY_IMAGE}'") | .id')
    - 'resp=$(curl --Write-out "\nCODE:%{http_code}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/${repository_id}/tags/${VERSION_TAG} | grep CODE | cut -d: -f2)'
    - if [[  "${resp}" == "200" ]]; then echo "${VERSION_TAG} image already exists!"; exit 1; fi
  rules:
    - if: '$CI_COMMIT_BRANCH'
  tags:
    - kube-executor

check_dockerfile_version:
  stage: version_parity
  image: image-mirror-prod-registry.cloud.duke.edu/library/alpine
  script:
    - grep ${VERSION_TAG} Dockerfile
    - echo "Dockerfile version matches"
  rules:
    - if: '$CI_COMMIT_BRANCH'
  tags:
    - kube-executor
      
build:
  stage: build
  extends: .kaniko_build
  before_script:
    - export THE_IMAGE=${CANDIDATE_IMAGE}
    - export THE_DOCKERFILE="Dockerfile"
    - export BUILD_ARGS="--build-arg CI_COMMIT_SHA=${CI_COMMIT_SHA} --build-arg CI_PROJECT_URL=${CI_PROJECT_URL} --build-arg IMAGE_VERSION=${VERSION_TAG}"
  rules:
    - if: '$CI_COMMIT_BRANCH'

bats:
  stage: test
  image: ${CANDIDATE_IMAGE}
  script:
    - /usr/local/bin/bats /usr/local/bin/test
  rules:
    - if: '$CI_COMMIT_BRANCH'
  tags:
    - kube-executor

tag_version:
  stage: tag
  extends: .registry_image_tag
  before_script:
    - export TO_TAG=${VERSION_TAG}
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'

tag_decommission:
  stage: tag
  extends: .registry_image_tag
  before_script:
    - export TO_TAG=decommission
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'

# Automatically generate the next semantic verson and create an annotated tag
increment_version:
  extends: .merge_request_version
  stage: publish
  variables:
    merge_request_arguments: '--include-addressed-issues'
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
  retry:
    max: 2
    when: runner_system_failure
