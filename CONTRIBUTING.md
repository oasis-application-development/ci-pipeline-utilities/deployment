Contributers should fork this project into their personal project, make
changes, test them, commit them and push the changes to your
personal fork, then create a Merge Request of the changes back
to the upstream project master branch, and request a maintainer
(see Maintainers.md) to review the merge request.

### Build Process
The Dockerfile is built into a candidate image using the kaniko gitlab runner tagged with the commit short sha.

It must add bin, all directories in libexec, and test to
the resulting image.

Each change MUST increment the VERSION_TAG in .gitlab-ci.yml.

The build pipeline tests the resulting image with bats. If this
test fails tag_latest and tag_version will not run and latest will remain
linked to the previous build candidate image. If the test passes, the 
candidate image is tagged with the VERSION_TAG, and latest.

deployment.yml must always refer to the :latest tag. This ensures that it 
will always work in the event of a build failure.

### Dependencies

This uses the following features of of the shared gitlab.dhe.duke.edu/utility/project-templates/ci-templates project docker.yml:
- .kaniko_build
- .registry_image_tag
