#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/bin/remove_oauth_redirect_uri"

setup() {
  export LOG_LEVEL="info"
  export CI_ENVIRONMENT_URL='http://ci-environment.url'
  export OAUTH_SIGN_IN_PATH='/OAUTH_SIGN_IN_PATH'
  export OIDC_CLIENT_ID='OIDC_CLIENT_ID'
}

@test "$(basename ${BATS_TEST_FILENAME}) check_required_oauth_environment fails if check_oauth_authentication_environment fails" {
  source ${profile_script}

  function check_oauth_authentication_environment() { echo "check_oauth_authentication_environment" >&2; return 1; }
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_required_oauth_environment
  assert_failure
  assert_output -p "check_oauth_authentication_environment"
  refute_output -p "check_required_environment OIDC_CLIENT_ID CI_ENVIRONMENT_URL OAUTH_SIGN_IN_PATH"
}

@test "$(basename ${BATS_TEST_FILENAME}) check_required_oauth_environment fails if OIDC_CLIENT_ID, CI_ENVIRONMENT_URL and OAUTH_SIGN_IN_PATH are not set" {
  source ${profile_script}

  function check_oauth_authentication_environment() { echo "check_oauth_authentication_environment" >&2; }
  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }

  run check_required_oauth_environment
  assert_failure
  assert_output -p "check_oauth_authentication_environment"
  assert_output -p "check_required_environment OIDC_CLIENT_ID CI_ENVIRONMENT_URL OAUTH_SIGN_IN_PATH"
}

@test "$(basename ${BATS_TEST_FILENAME}) check_required_oauth_environment succeeds if environment is present" {
  source ${profile_script}

  function check_oauth_authentication_environment() { echo "check_oauth_authentication_environment" >&2; }
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_required_oauth_environment
  assert_success
  assert_output -p "check_oauth_authentication_environment"
  assert_output -p "check_required_environment OIDC_CLIENT_ID CI_ENVIRONMENT_URL OAUTH_SIGN_IN_PATH"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if check_required_oauth_environment fails" {
  source ${profile_script}

  expected_redirect_uri="${CI_ENVIRONMENT_URL}${OAUTH_SIGN_IN_PATH}"
  
  function check_required_oauth_environment() { echo "check_required_oauth_environment" >&2; return 1; }
  function remove_oauth_redirect_uri() { echo "remove_oauth_redirect_uri ${*}" >&2; }

  run run_main
  assert_failure
  assert_output -p "check_required_oauth_environment"
  refute_output -p "remove_oauth_redirect_uri ${OIDC_CLIENT_ID} ${expected_redirect_uri}"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if remove_oauth_redirect_uri fails" {
  source ${profile_script}

  expected_redirect_uri="${CI_ENVIRONMENT_URL}${OAUTH_SIGN_IN_PATH}"
  
  function check_required_oauth_environment() { echo "check_required_oauth_environment" >&2; }
  function remove_oauth_redirect_uri() { echo "remove_oauth_redirect_uri ${*}" >&2; return 1; }

  run run_main
  assert_failure
  assert_output -p "check_required_oauth_environment"
  assert_output -p "remove_oauth_redirect_uri ${OIDC_CLIENT_ID} ${expected_redirect_uri}"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main succeeds if remove_oauth_redirect_uri succeeds" {
  source ${profile_script}

  expected_redirect_uri="${CI_ENVIRONMENT_URL}${OAUTH_SIGN_IN_PATH}"
  
  function check_required_oauth_environment() { echo "check_required_oauth_environment" >&2; }
  function remove_oauth_redirect_uri() { echo "remove_oauth_redirect_uri ${*}" >&2; }

  run run_main
  assert_success
  assert_output -p "check_required_oauth_environment"
  assert_output -p "remove_oauth_redirect_uri ${OIDC_CLIENT_ID} ${expected_redirect_uri}"  
}
