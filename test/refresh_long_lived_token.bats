#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/bin/refresh_long_lived_token"

setup() {
  export HELM_CHART_NAME="HELM_CHART_NAME"
  export CI_ENVIRONMENT_NAME="CI_ENVIRONMENT_NAME"
  export CI_ENVIRONMENT_SLUG="CI_ENVIRONMENT_SLUG"
  export IDMS_TOKEN_EXCHANGE_REFRESH_TOKEN="IDMS_TOKEN_EXCHANGE_REFRESH_TOKEN"
  export IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN="IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN"
}

@test "$(basename ${BATS_TEST_FILENAME})#token_keys prints the token keys" {
  source ${profile_script}

  run token_keys
  assert_output "IDMS_TOKEN_EXCHANGE_REFRESH_TOKEN IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_refresh_long_lived_token_requirements passes if requirements are present" {
  source ${profile_script}

  function check_orbs_label_requirements() { echo "check_orbs_label_requirements called" >&2; }
  function token_keys() { echo "token_keys"; }
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }
  function check_project_api_environment() { echo "check_project_api_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }

  run check_refresh_long_lived_token_requirements
  assert_success
  assert_output -p "check_orbs_label_requirements called"
  assert_output -p "check_required_environment CI_ENVIRONMENT_NAME token_keys"
  assert_output -p "check_project_api_environment called"
  assert_output -p "check_required_cluster_login_environment called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_refresh_long_lived_token_requirements fails if check_orbs_label_requirements fails" {
  source ${profile_script}

  function check_orbs_label_requirements() { echo "check_orbs_label_requirements called" >&2; return 1; }
  function token_keys() { echo "token_keys"; }
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }
  function check_project_api_environment() { echo "check_project_api_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }

  run check_refresh_long_lived_token_requirements
  assert_failure
  assert_output -p "check_orbs_label_requirements called"
  refute_output -p "check_required_environment CI_ENVIRONMENT_NAME token_keys"
  refute_output -p "check_project_api_environment called"
  refute_output -p "check_required_cluster_login_environment called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_refresh_long_lived_token_requirements fails if check_required_environment fails" {
  source ${profile_script}

  function check_orbs_label_requirements() { echo "check_orbs_label_requirements called" >&2; }
  function token_keys() { echo "token_keys"; }
  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }
  function check_project_api_environment() { echo "check_project_api_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }

  run check_refresh_long_lived_token_requirements
  assert_failure
  assert_output -p "check_orbs_label_requirements called"
  assert_output -p "check_required_environment CI_ENVIRONMENT_NAME token_keys"
  refute_output -p "check_project_api_environment called"
  refute_output -p "check_required_cluster_login_environment called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_refresh_long_lived_token_requirements fails if check_project_api_environment fails" {
  source ${profile_script}

  function check_orbs_label_requirements() { echo "check_orbs_label_requirements called" >&2; }
  function token_keys() { echo "token_keys"; }
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }
  function check_project_api_environment() { echo "check_project_api_environment called" >&2; return 1; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }

  run check_refresh_long_lived_token_requirements
  assert_failure
  assert_output -p "check_orbs_label_requirements called"
  assert_output -p "check_required_environment CI_ENVIRONMENT_NAME token_keys"
  assert_output -p "check_project_api_environment called"
  refute_output -p "check_required_cluster_login_environment called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_refresh_long_lived_token_requirements fails if check_required_cluster_login_environment fails" {
  source ${profile_script}

  function check_orbs_label_requirements() { echo "check_orbs_label_requirements called" >&2; }
  function token_keys() { echo "token_keys"; }
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }
  function check_project_api_environment() { echo "check_project_api_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; return 1; }

  run check_refresh_long_lived_token_requirements
  assert_failure
  assert_output -p "check_orbs_label_requirements called"
  assert_output -p "check_required_environment CI_ENVIRONMENT_NAME token_keys"
  assert_output -p "check_project_api_environment called"
  assert_output -p "check_required_cluster_login_environment called"
}

@test "$(basename ${BATS_TEST_FILENAME})#project_variable uses project_get to fetch the variable with the environment scope" {
  source ${profile_script}

  local expected_key="KEY"
  function project_get() { echo "project_get ${*}" >&2; }

  run project_variable "${expected_key}"
  assert_output "project_get /variables/${expected_key}?filter[environment_scope]=${CI_ENVIRONMENT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_gitlab_ci_variable_existence fails if token_keys tokens do no exist" {
  source ${profile_script}

  function token_keys() { echo "EXPECTED_KEY"; }
  function project_variable() { echo "project_variable ${*}" >&2; echo '{}'; }

  run check_gitlab_ci_variable_existence
  assert_failure
  assert_output -p "project_variable EXPECTED_KEY"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_gitlab_ci_variable_existence passes if expected keys exist" {
  source ${profile_script}

  function token_keys() { echo "EXPECTED_KEY"; }
  function project_variable() { echo "project_variable ${*}" >&2; echo '{"key":"EXPECTED_KEY"}'; }

  run check_gitlab_ci_variable_existence
  assert_success
  assert_output -p "project_variable EXPECTED_KEY"
}

@test "$(basename ${BATS_TEST_FILENAME})#update_gitlab_ci_variable fails if variable value is not updated" {
  source ${profile_script}
 
  local expected_key="EXPECTED_KEY"
  local expected_value="EXPECTED_VALUE"
  function project_put() { echo "project_put ${*}" >&2; echo '{}'; }

  run update_gitlab_ci_variable "${expected_key}" "${expected_value}"
  assert_failure
  assert_output -p "project_put {\"value\":\"${expected_value}\"} /variables/${expected_key}?filter[environment_scope]=${CI_ENVIRONMENT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#update_gitlab_ci_variable succeeds if variable value is updated" {
  source ${profile_script}

  local expected_key="EXPECTED_KEY"
  local expected_value="EXPECTED_VALUE"
  function project_put() { echo "project_put ${*}" >&2; echo '{"value":"EXPECTED_VALUE"}'; }

  run update_gitlab_ci_variable "${expected_key}" "${expected_value}"
  assert_success
  assert_output -p "project_put {\"value\":\"${expected_value}\"} /variables/${expected_key}?filter[environment_scope]=${CI_ENVIRONMENT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#refresh_access_token posts an update to the IDMS endpoint" {
  source ${profile_script}

  function curl() { echo "curl ${*}" >&2; }

  run refresh_access_token
  assert_output "curl -X POST --silent -H Content-type: application/json -d {\"longLivedRefreshToken\":\"${IDMS_TOKEN_EXCHANGE_REFRESH_TOKEN}\"} https://idms-web-ws.oit.duke.edu/idm-ws/clientSecret/refreshLongLivedToken"
}

@test "$(basename ${BATS_TEST_FILENAME})#has_error is true if exitcode is nonzero" {
  source ${profile_script}
  assert has_error 1 '{}'
}

@test "$(basename ${BATS_TEST_FILENAME})#has_error is true if exitcode is zero but input json .error is set" {
  source ${profile_script}
  assert has_error 0 '{"error":"true"}'
}

@test "$(basename ${BATS_TEST_FILENAME})#has_error is false if exitcode is zero and input json .error is not set" {
  source ${profile_script}
  refute has_error 0 '{"error":"false"}'
}

@test "$(basename ${BATS_TEST_FILENAME})#container_env_secret_name fails if kubectl fails" {
  source ${profile_script}

  local expected_container_env_secret='expected-container-env'
  function orbs_labels() { echo 'orbs_labels'; }
  function kubectl() { echo "kubectl ${*}" >&2; return 1; }
  
  run container_env_secret_name
  assert_failure
  assert_output -p "kubectl get secrets -l orbs_labels -o=name"
  refute_output -p "${expected_container_env_secret}"
}

@test "$(basename ${BATS_TEST_FILENAME})#container_env_secret_name fails if container_env does not exist" {
  source ${profile_script}

  local expected_container_env_secret='expected-container-env'
  function orbs_labels() { echo 'orbs_labels'; }
  function kubectl() { echo "kubectl ${*}" >&2; echo 'another-secret'; }
  
  run container_env_secret_name
  assert_failure
  assert_output -p "kubectl get secrets -l orbs_labels -o=name"
  refute_output -p "${expected_container_env_secret}"
}

@test "$(basename ${BATS_TEST_FILENAME})#container_env_secret_name returns container_env secret name" {
  source ${profile_script}

  local expected_container_env_secret='expected-container-env'
  function orbs_labels() { echo 'orbs_labels'; }
  function kubectl() { echo 'expected-container-env'; }
  
  run container_env_secret_name
  assert_success
  assert_output "${expected_container_env_secret}"
}

@test "$(basename ${BATS_TEST_FILENAME})#new_container_env_secret_file fails if mktemp fails" {
  source ${profile_script}

  local new_secret_value='new-secret-value'
  function mktemp() { return 1; }

  run new_container_env_secret_file "${new_secret_value}"
  assert_failure
}

@test "$(basename ${BATS_TEST_FILENAME})#new_container_env_secret_file fails if container_env_secret_name fails" {
  source ${profile_script}

  local new_secret_value='new-secret-value'
  function container_env_secret_name() { echo 'container_env_secret_name called' >&2; return 1; }

  run new_container_env_secret_file "${new_secret_value}"
  assert_failure
  assert_output -p 'container_env_secret_name called'
}

@test "$(basename ${BATS_TEST_FILENAME})#new_container_env_secret_file fails if kubectl fails" {
  source ${profile_script}

  local new_secret_value='new-secret-value'
  function container_env_secret_name() { echo 'container-env-secret-name'; }
  function kubectl() { echo "kubectl ${*}" >&2; return 1; }

  run new_container_env_secret_file "${new_secret_value}"
  assert_failure
  assert_output -p 'kubectl get container-env-secret-name -o json'
}

@test "$(basename ${BATS_TEST_FILENAME})#new_container_env_secret_file returns path to tmpfile with updated IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN" {
  source ${profile_script}

  local new_secret_value='new-secret-value'
  local encoded_new_secret=$(echo -n "${new_secret_value}" | base64)
  function container_env_secret_name() { echo 'container-env-secret-name'; }
  function kubectl() { echo "kubectl ${*}" >&2; echo '{"data":{"IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN":"old-value"}}'; }

  local new_secret_file=$(new_container_env_secret_file "${new_secret_value}")
  run jq -r '.data.IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN' ${new_secret_file}
  assert_output "${encoded_new_secret}"
}

@test "$(basename ${BATS_TEST_FILENAME})#update_container_env_secret fails if new_container_env_secret_file fails" {
  source ${profile_script}

  local new_secret_value='new-secret-value'
  function new_container_env_secret_file() { echo "new_container_env_secret_file ${*}" >&2; return 1; }
  function kubectl() { echo "kubectl called" >&2; }

  run update_container_env_secret "${new_secret_value}"
  assert_failure
  assert_output -p "new_container_env_secret_file ${new_secret_value}"
  refute_output -p "kubectl called"
}

@test "$(basename ${BATS_TEST_FILENAME})#update_container_env_secret fails if kubectl apply fails" {
  source ${profile_script}

  local new_secret_value='new-secret-value'
  function new_container_env_secret_file() { echo "new_container_env_secret_file ${*}" >&2; echo "test_file"; }
  function kubectl() { echo "kubectl ${*}" >&2; return 1; }

  run update_container_env_secret "${new_secret_value}"
  assert_failure
  assert_output -p "new_container_env_secret_file ${new_secret_value}"
  assert_output -p "kubectl apply -f test_file"
}

@test "$(basename ${BATS_TEST_FILENAME})#update_container_env_secret succeeds if new_container_env_secret_file and kubectl apply succeeds" {
  source ${profile_script}

  local new_secret_value='new-secret-value'
  function new_container_env_secret_file() { echo "new_container_env_secret_file ${*}" >&2; echo "test_file"; }
  function kubectl() { echo "kubectl ${*}" >&2; }

  run update_container_env_secret "${new_secret_value}"
  assert_success
  assert_output -p "new_container_env_secret_file ${new_secret_value}"
  assert_output -p "kubectl apply -f test_file"
}

@test "$(basename ${BATS_TEST_FILENAME})#update_access_token fails if has_error on refresh_access_token response is true" {
  source ${profile_script}

  function refresh_access_token() { echo 'refresh_access_token called' >&2; echo '{}'; return 1; }
  function has_error() { echo "has_error ${*}" >&2; }
  function update_gitlab_ci_variable() { echo "update_gitlab_ci_variable called"; }
  function update_container_env_secret() { echo "update_container_env_secret called" >&2; }
  function restart_orbs_deployments() { echo "restart_orbs_deployments ${*}" >&2; }

  run update_access_token
  assert_failure
  assert_output -p "refresh_access_token called"
  assert_output -p "has_error 1 {}"
  refute_output -p "update_gitlab_ci_variable called"
  refute_output -p "update_container_env_secret called"
  refute_output -p "restart_orbs_deployments oa_long_lived_token_restart=true"
}

@test "$(basename ${BATS_TEST_FILENAME})#update_access_token fails if IDMS_TOKEN_EXCHANGE_REFRESH_TOKEN is not stored in gitlab" {
  source ${profile_script}

  function refresh_access_token() { echo 'refresh_access_token called' >&2; echo '{"mapQueryResult":{"attributes":{"refreshToken":"NEW_REFRESH_TOKEN","accessToken":"NEW_ACCESS_TOKEN"}}}'; }
  function has_error() { echo "has_error ${*}" >&2; return 1; }
  function update_gitlab_ci_variable() { echo "update_gitlab_ci_variable ${*}"; return 1; }
  function update_container_env_secret() { echo "update_container_env_secret called" >&2; }
  function restart_orbs_deployments() { echo "restart_orbs_deployments ${*}" >&2; }

  run update_access_token
  assert_failure
  assert_output -p "refresh_access_token called"
  assert_output -p "has_error 0 {\"mapQueryResult\":{\"attributes\":{\"refreshToken\":\"NEW_REFRESH_TOKEN\",\"accessToken\":\"NEW_ACCESS_TOKEN\"}}}"
  assert_output -p "update_gitlab_ci_variable IDMS_TOKEN_EXCHANGE_REFRESH_TOKEN NEW_REFRESH_TOKEN"
  refute_output -p "update_gitlab_ci_variable IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN NEW_ACCESS_TOKEN"
  refute_output -p "update_container_env_secret called"
  refute_output -p "restart_orbs_deployments oa_long_lived_token_restart=true"
}

@test "$(basename ${BATS_TEST_FILENAME})#update_access_token fails if IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN is not stored in gitlab" {
  source ${profile_script}

  function refresh_access_token() { echo 'refresh_access_token called' >&2; echo '{"mapQueryResult":{"attributes":{"refreshToken":"NEW_REFRESH_TOKEN","accessToken":"NEW_ACCESS_TOKEN"}}}'; }
  function has_error() { echo "has_error ${*}" >&2; return 1; }
  function update_gitlab_ci_variable() { echo "update_gitlab_ci_variable ${*}"; [ "${1}" == "IDMS_TOKEN_EXCHANGE_REFRESH_TOKEN" ]; }
  function update_container_env_secret() { echo "update_container_env_secret called" >&2; }
  function restart_orbs_deployments() { echo "restart_orbs_deployments ${*}" >&2; }

  run update_access_token
  assert_failure
  assert_output -p "refresh_access_token called"
  assert_output -p "has_error 0 {\"mapQueryResult\":{\"attributes\":{\"refreshToken\":\"NEW_REFRESH_TOKEN\",\"accessToken\":\"NEW_ACCESS_TOKEN\"}}}"
  assert_output -p "update_gitlab_ci_variable IDMS_TOKEN_EXCHANGE_REFRESH_TOKEN NEW_REFRESH_TOKEN"
  assert_output -p "update_gitlab_ci_variable IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN NEW_ACCESS_TOKEN"
  refute_output -p "update_container_env_secret called"
  refute_output -p "restart_orbs_deployments oa_long_lived_token_restart=true"
}

@test "$(basename ${BATS_TEST_FILENAME})#update_access_token fails if update_container_env_secret fails" {
  source ${profile_script}

  function refresh_access_token() { echo 'refresh_access_token called' >&2; echo '{"mapQueryResult":{"attributes":{"refreshToken":"NEW_REFRESH_TOKEN","accessToken":"NEW_ACCESS_TOKEN"}}}'; }
  function has_error() { echo "has_error ${*}" >&2; return 1; }
  function update_gitlab_ci_variable() { echo "update_gitlab_ci_variable ${*}"; }
  function update_container_env_secret() { echo "update_container_env_secret called" >&2; return 1; }
  function restart_orbs_deployments() { echo "restart_orbs_deployments ${*}" >&2; }

  run update_access_token
  assert_failure
  assert_output -p "refresh_access_token called"
  assert_output -p "has_error 0 {\"mapQueryResult\":{\"attributes\":{\"refreshToken\":\"NEW_REFRESH_TOKEN\",\"accessToken\":\"NEW_ACCESS_TOKEN\"}}}"
  assert_output -p "update_gitlab_ci_variable IDMS_TOKEN_EXCHANGE_REFRESH_TOKEN NEW_REFRESH_TOKEN"
  assert_output -p "update_gitlab_ci_variable IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN NEW_ACCESS_TOKEN"
  assert_output -p "update_container_env_secret called"
  refute_output -p "restart_orbs_deployments oa_long_lived_token_restart=true"
}

@test "$(basename ${BATS_TEST_FILENAME})#update_access_token fails if restart_orbs_deployment fails" {
  source ${profile_script}

  function refresh_access_token() { echo 'refresh_access_token called' >&2; echo '{"mapQueryResult":{"attributes":{"refreshToken":"NEW_REFRESH_TOKEN","accessToken":"NEW_ACCESS_TOKEN"}}}'; }
  function has_error() { echo "has_error ${*}" >&2; return 1; }
  function update_gitlab_ci_variable() { echo "update_gitlab_ci_variable ${*}"; }
  function update_container_env_secret() { echo "update_container_env_secret called" >&2; }
  function restart_orbs_deployments() { echo "restart_orbs_deployments ${*}" >&2; return 1; }

  run update_access_token
  assert_failure
  assert_output -p "refresh_access_token called"
  assert_output -p "has_error 0 {\"mapQueryResult\":{\"attributes\":{\"refreshToken\":\"NEW_REFRESH_TOKEN\",\"accessToken\":\"NEW_ACCESS_TOKEN\"}}}"
  assert_output -p "update_gitlab_ci_variable IDMS_TOKEN_EXCHANGE_REFRESH_TOKEN NEW_REFRESH_TOKEN"
  assert_output -p "update_gitlab_ci_variable IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN NEW_ACCESS_TOKEN"
  assert_output -p "update_container_env_secret called"
  assert_output -p "restart_orbs_deployments oa_long_lived_token_restart=true"
}

@test "$(basename ${BATS_TEST_FILENAME})#update_access_token succeeds if tokens are updated and stored in gitlab, container_env_secret is updated, and deployment is restarted" {
  source ${profile_script}

  function refresh_access_token() { echo 'refresh_access_token called' >&2; echo '{"mapQueryResult":{"attributes":{"refreshToken":"NEW_REFRESH_TOKEN","accessToken":"NEW_ACCESS_TOKEN"}}}'; }
  function has_error() { echo "has_error ${*}" >&2; return 1; }
  function update_gitlab_ci_variable() { echo "update_gitlab_ci_variable ${*}"; }
  function update_container_env_secret() { echo "update_container_env_secret called" >&2; }
  function restart_orbs_deployments() { echo "restart_orbs_deployments ${*}" >&2; }

  run update_access_token
  assert_success
  assert_output -p "refresh_access_token called"
  assert_output -p "has_error 0 {\"mapQueryResult\":{\"attributes\":{\"refreshToken\":\"NEW_REFRESH_TOKEN\",\"accessToken\":\"NEW_ACCESS_TOKEN\"}}}"
  assert_output -p "update_gitlab_ci_variable IDMS_TOKEN_EXCHANGE_REFRESH_TOKEN NEW_REFRESH_TOKEN"
  assert_output -p "update_gitlab_ci_variable IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN NEW_ACCESS_TOKEN"
  assert_output -p "update_container_env_secret called"
  assert_output -p "restart_orbs_deployments oa_long_lived_token_restart=true"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if check_refresh_long_lived_token_requirements fails" {
  source ${profile_script}

  function check_refresh_long_lived_token_requirements() { echo "check_refresh_long_lived_token_requirements called" >&2; return 1; }
  function check_gitlab_ci_variable_existence() { echo 'check_gitlab_ci_variable_existence called' >&2; }
  function cluster_login() { echo 'cluster_login called' >&2; }
  function update_access_token() { echo 'update_access_token called' >&2; }

  run run_main
  assert_failure
  assert_output -p 'check_refresh_long_lived_token_requirements called'
  refute_output -p 'check_gitlab_ci_variable_existence called'
  refute_output -p 'cluster_login called'
  refute_output -p 'update_access_token called'
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if check_gitlab_ci_variable_existence fails" {
  source ${profile_script}

  function check_refresh_long_lived_token_requirements() { echo "check_refresh_long_lived_token_requirements called" >&2; }
  function check_gitlab_ci_variable_existence() { echo 'check_gitlab_ci_variable_existence called' >&2; return 1; }
  function cluster_login() { echo 'cluster_login called' >&2; }
  function update_access_token() { echo 'update_access_token called' >&2; }

  run run_main
  assert_failure
  assert_output -p 'check_refresh_long_lived_token_requirements called'
  assert_output -p 'check_gitlab_ci_variable_existence called'
  refute_output -p 'cluster_login called'
  refute_output -p 'update_access_token called'
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if cluster_login fails" {
  source ${profile_script}

  function check_refresh_long_lived_token_requirements() { echo "check_refresh_long_lived_token_requirements called" >&2; }
  function check_gitlab_ci_variable_existence() { echo 'check_gitlab_ci_variable_existence called' >&2; }
  function cluster_login() { echo 'cluster_login called' >&2; return 1; }
  function update_access_token() { echo 'update_access_token called' >&2; }

  run run_main
  assert_failure
  assert_output -p 'check_refresh_long_lived_token_requirements called'
  assert_output -p 'check_gitlab_ci_variable_existence called'
  assert_output -p 'cluster_login called'
  refute_output -p 'update_access_token called'
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if update_access_token fails" {
  source ${profile_script}

  function check_refresh_long_lived_token_requirements() { echo "check_refresh_long_lived_token_requirements called" >&2; }
  function check_gitlab_ci_variable_existence() { echo 'check_gitlab_ci_variable_existence called' >&2; }
  function cluster_login() { echo 'cluster_login called' >&2; }
  function update_access_token() { echo 'update_access_token called' >&2; return 1; }

  run run_main
  assert_failure
  assert_output -p 'check_refresh_long_lived_token_requirements called'
  assert_output -p 'check_gitlab_ci_variable_existence called'
  assert_output -p 'cluster_login called'
  assert_output -p 'update_access_token called'
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main succeeds if update_access_token succeeds" {
  source ${profile_script}

  function check_refresh_long_lived_token_requirements() { echo "check_refresh_long_lived_token_requirements called" >&2; }
  function check_gitlab_ci_variable_existence() { echo 'check_gitlab_ci_variable_existence called' >&2; }
  function cluster_login() { echo 'cluster_login called' >&2; }
  function update_access_token() { echo 'update_access_token called' >&2; }

  run run_main
  assert_success
  assert_output -p 'check_refresh_long_lived_token_requirements called'
  assert_output -p 'check_gitlab_ci_variable_existence called'
  assert_output -p 'cluster_login called'
  assert_output -p 'update_access_token called'
}
