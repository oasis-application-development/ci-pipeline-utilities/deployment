#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/lib/helpers/common"

setup() {
  unset LOG_LEVEL
}

@test "$(basename ${BATS_TEST_FILENAME})#log_level_for defines error < warn < debug < info < unsupported" {
  source ${profile_script}
  assert [ $(log_level_for "error") -lt $(log_level_for "warn") ]
  assert [ $(log_level_for "warn") -lt $(log_level_for "debug") ]
  assert [ $(log_level_for "debug") -lt $(log_level_for "info") ]
  assert [ $(log_level_for "info") -gt $(log_level_for "anything else") ]
  assert [ $(log_level_for "anything else") -eq $(log_level_for "any unsupported") ]
}

@test "$(basename ${BATS_TEST_FILENAME})#current_log_level is -1 if LOG_LEVEL is not set" {
  source ${profile_script}
  assert_empty "${LOG_LEVEL}"
  assert [ $(current_log_level) -eq -1 ]
}

@test "$(basename ${BATS_TEST_FILENAME})#current_log_level is log_level_for LOG_LEVEL if LOG_LEVEL is set" {
  for log_level in unsupported error warn debug info
  do
    export LOG_LEVEL=${log_level}
    assert [ $(current_log_level) -eq $(log_level_for "${LOG_LEVEL}") ]
  done
}

@test "$(basename ${BATS_TEST_FILENAME})#error does not print if log_level_for error is not less than or equal to current_log_level" {
  source ${profile_script}
  assert_empty "${LOG_LEVEL}"
  local expected_message="expected message"

  refute [ $(log_level_for "error") -le $(current_log_level) ]
  run error "${expected_message}"
  refute_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#error takes a string argument and prints it if log_level_for error is less than current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "warn"; }
  export -f current_log_level

  assert [ $(log_level_for "error") -lt $(current_log_level) ]
  local expected_message="expected message"
  run error "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#error takes multiple string arguments and prints them if log_level_for error is less than current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "warn"; }
  export -f current_log_level

  assert [ $(log_level_for "error") -lt $(current_log_level) ]
  local expected_message="expected message"
  run error "expected" "message"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#error takes a string argument and prints it if log_level_for error equals current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "error"; }
  export -f current_log_level

  assert [ $(log_level_for "error") -eq $(current_log_level) ]
  local expected_message="expected message"
  run error "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#error prints to STDERR" {
  source ${profile_script}
  export LOG_LEVEL="error"
  local expected_message="expected message"
  assert_equal "$(error "${expected_message}" > /dev/null)" "${actual_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#warn does not print if log_level_for warn is not less than or equal to current_log_level" {
  source ${profile_script}
  assert_empty "${LOG_LEVEL}"
  local expected_message="expected message"

  refute [ $(log_level_for "warn") -le $(current_log_level) ]
  run warn "${expected_message}"
  refute_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#warn takes a string argument and prints it if log_level_for warn is less than current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "debug"; }
  export -f current_log_level

  assert [ $(log_level_for "warn") -lt $(current_log_level) ]
  local expected_message="expected message"
  run warn "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#warn takes multiple arguments and prints them if log_level_for warn is less than current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "debug"; }
  export -f current_log_level

  assert [ $(log_level_for "warn") -lt $(current_log_level) ]
  local expected_message="expected message"
  run warn "expected" "message"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#warn takes a string argument and prints it if log_level_for warn equals current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "warn"; }
  export -f current_log_level

  assert [ $(log_level_for "warn") -eq $(current_log_level) ]
  local expected_message="expected message"
  run warn "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#warn prints to STDERR" {
  source ${profile_script}
  export LOG_LEVEL="warn"
  local expected_message="expected message"
  assert_equal "$(warn "${expected_message}" > /dev/null)" "${actual_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#debug does not print if log_level_for debug is not less than or equal to current_log_level" {
  source ${profile_script}
  assert_empty "${LOG_LEVEL}"
  local expected_message="expected message"

  refute [ $(log_level_for "debug") -le $(current_log_level) ]
  run debug "${expected_message}"
  refute_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#debug takes a string argument and prints it if log_level_for debug is less than current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "info"; }
  export -f current_log_level

  assert [ $(log_level_for "debug") -lt $(current_log_level) ]
  local expected_message="expected message"
  run debug "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#debug takes multiple arguments and prints them if log_level_for debug is less than current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "info"; }
  export -f current_log_level

  assert [ $(log_level_for "debug") -lt $(current_log_level) ]
  local expected_message="expected message"
  run debug "expected" "message"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#debug takes a string argument and prints it if log_level_for debug equals current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "debug"; }
  export -f current_log_level

  assert [ $(log_level_for "debug") -eq $(current_log_level) ]
  local expected_message="expected message"
  run debug "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#debug prints to STDERR" {
  source ${profile_script}
  export LOG_LEVEL="debug"
  local expected_message="expected message"
  assert_equal "$(debug "${expected_message}" > /dev/null)" "${actual_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#info does not print if log_level_for info is not less than or equal to current_log_level" {
  source ${profile_script}
  assert_empty "${LOG_LEVEL}"
  local expected_message="expected message"

  refute [ $(log_level_for "info") -le $(current_log_level) ]
  run info "${expected_message}"
  refute_output "${expected_message}"
}

# this allows us to add new LOG_LEVEL support later that is even greater than info
@test "$(basename ${BATS_TEST_FILENAME})#info takes a string argument and prints it if log_level_for info is less than current_log_level" {
  source ${profile_script}

  function current_log_level() { echo 5; }
  export -f current_log_level

  assert [ $(log_level_for "info") -lt $(current_log_level) ]
  local expected_message="expected message"
  run info "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#info takes multiple arguments and prints them if log_level_for info is less than current_log_level" {
  source ${profile_script}

  function current_log_level() { echo 5; }
  export -f current_log_level

  assert [ $(log_level_for "info") -lt $(current_log_level) ]
  local expected_message="expected message"
  run info "expected" "message"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#info takes a string argument and prints it if log_level_for info equals current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "info"; }
  export -f current_log_level

  assert [ $(log_level_for "info") -eq $(current_log_level) ]
  local expected_message="expected message"
  run info "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME})#info prints to STDERR" {
  source ${profile_script}
  export LOG_LEVEL="info"
  local expected_message="expected message"
  assert_equal "$(info "${expected_message}" > /dev/null)" "${actual_message}"
}


@test "$(basename ${BATS_TEST_FILENAME})#check_required_environment takes a required environment variable, and raises an error if it is missing" {
  source ${profile_script}
  local required_environment="FOO"
  assert_empty "${FOO}"

  function error() { echo "error ${*} called" >&2; }
  export -f error

  run check_required_environment "${required_environment}"
  assert_failure
  assert_output --partial "error missing ENVIRONMENT ${required_environment}! called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_environment takes a list of required environment variables, and raises an error if one is missing" {
  source ${profile_script}

  local required_environment="FOO BAR"
  local missing_environment_variable="BAR"
  local existing_environment_variable="FOO"
  export FOO="foo"
  assert_empty "${BAR}"

  function error() { echo "error ${*} called" >&2; }
  export -f error

  run check_required_environment "${required_environment}"
  assert_failure
  assert_output --partial "error missing ENVIRONMENT ${missing_environment_variable}! called"
  refute_output --partial "error missing ENVIRONMENT ${existing_environment_variable}! called"
  unset FOO
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_environment takes a list of required environment variables, and returns success if all are present" {
  local required_environment="FOO BAR"
  export FOO="foo"
  export BAR="bar"
  source ${profile_script}
  run check_required_environment "${required_environment}"
  assert_success
  unset FOO
  unset BAR
}

@test "$(basename ${BATS_TEST_FILENAME})#check_default_environment takes an allowed:default environment variable tuple, and raises an error if both are missing" {
  source ${profile_script}
  local allowed_environment="FOO"
  local default_environment="BAR"
  local environment_expectation_tuple="${allowed_environment}:${default_environment}"
  assert_empty "${FOO}"
  assert_empty "${BAR}"

  function error() { echo "error ${*} called" >&2; }
  export -f error

  run check_default_environment "${environment_expectation_tuple}"
  assert_failure
  assert_output --partial "error missing default ENVIRONMENT, set ${allowed_environment} or ${default_environment}! called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_default_environment takes an allowed:default environment variable tuple, and returns success if the allowed environment variable is set" {
  source ${profile_script}
  local allowed_environment="FOO"
  local default_environment="BAR"
  local environment_expectation_tuple="${allowed_environment}:${default_environment}"
  export FOO='FOO'
  assert_empty "${BAR}"

  function error() { echo "error ${*} called" >&2; }
  export -f error

  run check_default_environment "${environment_expectation_tuple}"
  assert_success
  refute_output --partial "error missing default ENVIRONMENT, set ${allowed_environment} or ${default_environment}! called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_default_environment takes an allowed:default environment variable tuple, and returns success if the default environment variable is set" {
  source ${profile_script}
  local allowed_environment="FOO"
  local default_environment="BAR"
  local environment_expectation_tuple="${allowed_environment}:${default_environment}"
  assert_empty "${FOO}"
  export BAR='BAR'

  function error() { echo "error ${*} called" >&2; }
  export -f error

  run check_default_environment "${environment_expectation_tuple}"
  assert_success
  refute_output --partial "error missing default ENVIRONMENT, set ${allowed_environment} or ${default_environment}! called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_default_environment takes a list of allowed:default environment tuples, and raises an error if one pair is missing" {
  source ${profile_script}

  local missing_allowed_environment="BAR"
  local missing_default_environment="BARDEF"
  local existing_allowed_environment="FOO"
  local existing_default_environment="FOODEF"
  export FOO="foo"
  assert_empty "${BAR}"
  assert_empty "${BARDEF}"
  local environment_expectation_tuple="${existing_allowed_environment}:${existing_default_environment} ${missing_allowed_environment}:${missing_default_environment}"

  function error() { echo "error ${*} called" >&2; }
  export -f error

  run check_default_environment "${environment_expectation_tuple}"
  assert_failure
  assert_output --partial "error missing default ENVIRONMENT, set ${missing_allowed_environment} or ${missing_default_environment}! called"
  refute_output --partial "error missing default ENVIRONMENT, set ${existing_allowed_environment} or ${existing_default_environment}! called"
  unset FOO
}

@test "$(basename ${BATS_TEST_FILENAME})#check_default_environment takes a list of required environment variables, and returns success if all are present" {
  local required_environment="FOO:FOODEF BAR:BARDEF"
  export FOODEF="foo"
  export BAR="bar"
  source ${profile_script}
  run check_default_environment "${required_environment}"
  assert_success
  unset FOO
  unset BAR
}

@test "$(basename ${BATS_TEST_FILENAME})#dry_run returns false if DRY_RUN environment variable is not set" {
  assert_empty "${DRY_RUN}"
  source ${profile_script}
  run dry_run
  assert_failure
}

@test "$(basename ${BATS_TEST_FILENAME})#dry_run prints an info message and returns successful if DRY_RUN environment variable is set" {
  source ${profile_script}
  export DRY_RUN=1

  function info() { echo "info ${*} called" >&2; }

  run dry_run
  assert_output "info skipping for dry run called"
  unset DRY_RUN
}

@test "$(basename ${BATS_TEST_FILENAME})#commit_has_changed takes file and prints information if dry_run is true" {
  source ${profile_script}

  local expected_file="FileToTest"
  export LOG_LEVEL="info"
  export DRY_RUN=1
  export CI_COMMIT_SHA="CI_COMMIT_SHA"
  function git() { echo "git ${*} called" >&2; }

  run commit_has_changed "${expected_file}"
  assert_success
  assert_output -p "git diff-tree --no-commit-id --name-only -r ${CI_COMMIT_SHA}"
  refute_output -p "git diff-tree --no-commit-id --name-only -r ${CI_COMMIT_SHA} called"
  unset DRY_RUN
  unset CI_COMMIT_SHA
}

@test "$(basename ${BATS_TEST_FILENAME})#commit_has_changed warns if CI_COMMIT_SHA is not set" {
  source ${profile_script}

  local expected_file="FileToTest"
  export LOG_LEVEL="info"
  unset CI_COMMIT_SHA
  function git() { echo "git ${*} called" >&2; }

  run commit_has_changed "${expected_file}"
  assert_success
  assert_output -p "CI_COMMIT_SHA is not set"
  refute_output -p "git diff-tree --no-commit-id --name-only -r ${CI_COMMIT_SHA}"
  refute_output -p "git diff-tree --no-commit-id --name-only -r ${CI_COMMIT_SHA} called"
  unset CI_COMMIT_SHA
}

@test "$(basename ${BATS_TEST_FILENAME})#commit_has_changed tests for presence of expected_file in commit" {
  source ${profile_script}

  local expected_file="FileToTest"
  export LOG_LEVEL="info"
  export CI_COMMIT_SHA="CI_COMMIT_SHA"
  function git() { echo "git ${*} called" >&2; echo "FileToTest"; }

  run commit_has_changed "${expected_file}"
  assert_success
  refute_output -p "CI_COMMIT_SHA is not set"
  assert_output -p "git diff-tree --no-commit-id --name-only -r ${CI_COMMIT_SHA} called"
  unset CI_COMMIT_SHA
}

@test "$(basename ${BATS_TEST_FILENAME})#commit_has_changed fails if expected_file is not in commit" {
  source ${profile_script}

  local expected_file="FileToTest"
  export LOG_LEVEL="info"
  export CI_COMMIT_SHA="CI_COMMIT_SHA"
  function git() { echo "git ${*} called" >&2; }

  run commit_has_changed "${expected_file}"
  assert_failure
  refute_output -p "CI_COMMIT_SHA is not set"
  assert_output -p "git diff-tree --no-commit-id --name-only -r ${CI_COMMIT_SHA} called"
  unset CI_COMMIT_SHA
}

@test "$(basename ${BATS_TEST_FILENAME})#deployment_image_version prints DEPLOYMENT_IMAGE_VERSION environment" {
  source ${profile_script}

  export DEPLOYMENT_IMAGE_VERSION='1.1.1'

  run deployment_image_version
  assert_output "${DEPLOYMENT_IMAGE_VERSION}"
}

@test "$(basename ${BATS_TEST_FILENAME})#deployment_image_version_info uses info to print deployment_image_version" {
  source ${profile_script}

  export LOG_LEVEL=info
  export DEPLOYMENT_IMAGE_VERSION='1.1.1'

  run deployment_image_version_info
  assert_output "using deployment_image version ${DEPLOYMENT_IMAGE_VERSION}"
}
