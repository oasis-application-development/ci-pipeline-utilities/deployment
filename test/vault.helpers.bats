#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/lib/helpers/vault"

setup() {
    export HELM_USER='HELM_USER'
    export HELM_TOKEN='HELM_TOKEN'
    export PROJECT_NAMESPACE='PROJECT_NAMESPACE'
    export VAULT_ADDR='http://vault'
    export CI_ENVIRONMENT_NAME='CI_ENVIRONMENT_NAME'
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_vault_environment calls check_required_cluster_login_environment, check_required_environment, and check_default_environment for environment required for vault authentication" {
    source ${profile_script}

    local expected_required_environment="VAULT_ADDR CI_ENVIRONMENT_NAME"
    local expected_default_environment="APPLICATION_STACK_ENVIRONMENT_NAME:CI_ENVIRONMENT_NAME"

    function check_required_cluster_login_environment { echo "check_required_cluster_login_environment called" >&2; }
    function check_required_environment() { echo "check_required_environment ${*}" >&2; }
    function check_default_environment() { echo "check_default_environment ${*}" >&2; }

    run check_required_vault_environment
    assert_success
    assert_output -p "check_required_cluster_login_environment called"
    assert_output -p "check_required_environment ${expected_environment}"
    assert_output -p "check_default_environment ${expected_default_environment}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_vault_environment fails if check_required_cluster_login_environment fails" {
    source ${profile_script}

    local expected_required_environment="VAULT_ADDR CI_ENVIRONMENT_NAME"
    local expected_default_environment="APPLICATION_STACK_ENVIRONMENT_NAME:CI_ENVIRONMENT_NAME"

    function check_required_cluster_login_environment { echo "check_required_cluster_login_environment called" >&2; return 1; }
    function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }
    function check_default_environment() { echo "check_default_environment ${*}" >&2; }

    run check_required_vault_environment
    assert_failure
    assert_output -p "check_required_cluster_login_environment called"
    refute_output -p "check_required_environment ${expected_environment}"
    refute_output -p "check_default_environment ${expected_default_environment}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_vault_environment fails if check_required_environment fails" {
    source ${profile_script}

    local expected_required_environment="VAULT_ADDR CI_ENVIRONMENT_NAME"
    local expected_default_environment="APPLICATION_STACK_ENVIRONMENT_NAME:CI_ENVIRONMENT_NAME"

    function check_required_cluster_login_environment { echo "check_required_cluster_login_environment called" >&2; }
    function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }
    function check_default_environment() { echo "check_default_environment ${*}" >&2; }

    run check_required_vault_environment
    assert_failure
    assert_output -p "check_required_cluster_login_environment called"
    assert_output -p "check_required_environment ${expected_environment}"
    refute_output -p "check_default_environment ${expected_default_environment}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_vault_environment fails if check_default_environment fails" {
    source ${profile_script}

    local expected_required_environment="VAULT_ADDR CI_ENVIRONMENT_NAME"
    local expected_default_environment="APPLICATION_STACK_ENVIRONMENT_NAME:CI_ENVIRONMENT_NAME"

    function check_required_cluster_login_environment { echo "check_required_cluster_login_environment called" >&2; }
    function check_required_environment() { echo "check_required_environment ${*}" >&2; }
    function check_default_environment() { echo "check_default_environment ${*}" >&2; return 1; }

    run check_required_vault_environment
    assert_failure
    assert_output -p "check_required_cluster_login_environment called"
    assert_output -p "check_required_environment ${expected_environment}"
    assert_output -p "check_default_environment ${expected_default_environment}"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_sa returns the expecte vault enabled service account name" {
    source ${profile_script}
    local expected_sa="sa/${PROJECT_NAMESPACE}-secrets"

    run vault_sa
    assert_success
    assert_output "${expected_sa}"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_sa_secret uses kubectl to get the jwt for vault authentication" {
    source ${profile_script}
    local expected_jwt="abc123"
    local expected_secret="secret/${PROJECT_NAMESPACE}-secret-foo"

    function cluster_login { echo "cluster_login called" >&2; }
    function kubectl { echo "kubectl ${*}" >&2; }
    function jq { echo "jq ${*}">&2; echo "${PROJECT_NAMESPACE}-secret-foo"; }

    run vault_sa_secret
    assert_success
    assert_output -p "cluster_login called"
    assert_output -p "kubectl get $(vault_sa) -o json"
    assert_output -p "jq -r .secrets[0].name"
    assert_output -p "${expected_secret}"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_sa_secret fails if cluster_login fails" {
    source ${profile_script}
    local expected_jwt="abc123"
    local expected_secret="secret/${PROJECT_NAMESPACE}-secret-foo"

    function cluster_login { echo "cluster_login called" >&2; return 1; }
    function kubectl { echo "kubectl ${*}" >&2;  }
    function jq { echo "jq ${*}">&2; echo "${PROJECT_NAMESPACE}-secret-foo"; }

    run vault_sa_secret
    assert_failure
    assert_output -p "cluster_login called"
    refute_output -p "kubectl get $(vault_sa) -o json"
    refute_output -p "jq -r .secrets[0].name"
    refute_output -p "${expected_secret}"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_sa_secret fails if jq fails" {
    source ${profile_script}
    local expected_jwt="abc123"
    local expected_secret="secret/${PROJECT_NAMESPACE}-secret-foo"

    function cluster_login { echo "cluster_login called" >&2; }
    function kubectl { echo "kubectl ${*}" >&2; }
    function jq { echo "jq ${*}" >&2; return 1; }

    run vault_sa_secret
    assert_failure
    assert_output -p "cluster_login called"
    assert_output -p "kubectl get $(vault_sa) -o json"
    assert_output -p "jq -r .secrets[0].name"
    refute_output -p "${expected_secret}"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_sa_secret fails if kubectl fails" {
    source ${profile_script}
    local expected_jwt="abc123"
    local expected_secret="secret/${PROJECT_NAMESPACE}-secret-foo"

    function cluster_login { echo "cluster_login called" >&2; }
    function kubectl { echo "kubectl ${*}" >&2; return 1; }
    function jq { echo "jq ${*}">&2; echo "${PROJECT_NAMESPACE}-secret-foo"; }

    run vault_sa_secret
    assert_failure
    assert_output -p "cluster_login called"
    assert_output -p "kubectl get $(vault_sa) -o json"
    refute_output -p "jq -r .secrets[0].name"
    refute_output -p "${expected_secret}"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_jwt uses kubectl to get the jwt for the vault_sa_secret" {
    source ${profile_script}

    local expected_jwt="abc123"
    local expected_secret="secret/${PROJECT_NAMESPACE}-secret-foo"

    function vault_sa_secret() { echo "vault_sa_secret called" >&2; echo "secret/${PROJECT_NAMESPACE}-secret-foo"; }
    function kubectl { echo "kubectl ${*}" >&2; }
    function jq { echo "jq ${*}">&2; }
    function base64 { echo "base64 ${*}" >&2; echo 'abc123'; }

    run vault_jwt
    assert_success
    assert_output -p "vault_sa_secret called"
    assert_output -p "kubectl get ${expected_secret} -o json"
    assert_output -p "jq -r .data.token"
    assert_output -p "base64 -d"
    assert_output -p "${expected_jwt}"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_jwt fails if vault_sa_secret fails" {
    source ${profile_script}

    local expected_jwt="abc123"
    local expected_secret="secret/${PROJECT_NAMESPACE}-secret-foo"

    function vault_sa_secret() { echo "vault_sa_secret called" >&2; return 1; }
    function kubectl { echo "kubectl ${*}" >&2; }
    function jq { echo "jq ${*}" >&2; }
    function base64 { echo "base64 ${*}" >&2; echo 'abc123'; }

    run vault_jwt
    assert_failure
    assert_output -p "vault_sa_secret called"
    assert_output -p "could not get vault_sa_secret for $(vault_sa)"
    refute_output -p "kubectl get ${expected_secret} -o json"
    refute_output -p "jq -r .data.token"
    refute_output -p "base64 -d"
    refute_output -p "${expected_jwt}"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_jwt fails if kubectl fails" {
    source ${profile_script}

    local expected_jwt="abc123"
    local expected_secret="secret/${PROJECT_NAMESPACE}-secret-foo"

    function vault_sa_secret() { echo "vault_sa_secret called" >&2; echo "secret/${PROJECT_NAMESPACE}-secret-foo"; }
    function kubectl { echo "kubectl ${*}" >&2; return 1; }
    function jq { echo "jq ${*}" >&2; }
    function base64 { echo "base64 ${*}" >&2; echo 'abc123'; }

    run vault_jwt
    assert_failure
    assert_output -p "vault_sa_secret called"
    assert_output -p "kubectl get ${expected_secret} -o json"
    assert_output -p "could not get jwt for $(vault_sa)"
    refute_output -p "jq -r .data.token"
    refute_output -p "base64 -d"
    refute_output -p "${expected_jwt}"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_token returns the vault token using the vault_jwt" {
    source ${profile_script}

    local expected_jwt='abc123'
    local expected_token='123.abc'

    function vault_jwt() { echo "vault_jwt called" >&2; echo 'abc123'; }
    function curl() { echo "curl ${*} called" >&2; }
    function jq() { echo "jq ${*} called" >&2; echo '123.abc'; }

    run vault_token
    assert_success
    assert_output -p "vault_jwt called"
    assert_output -p "curl -s ${VAULT_ADDR}/v1/auth/kubeocp-auth/login -X POST -d {\"jwt\":\"${expected_jwt}\",\"role\":\"${PROJECT_NAMESPACE}-role\"}"
    assert_output -p "jq -r .auth.client_token called"
    assert_output -p "${expected_token}"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_token fails if vault_jwt returns an empty string" {
    source ${profile_script}

    local expected_jwt='abc123'
    local expected_token='123.abc'

    function vault_jwt() { echo "vault_jwt called" >&2; return 1; }
    function curl() { echo "curl ${*} called" >&2; }
    function jq() { echo "jq ${*} called" >&2; echo '123.abc'; }

    run vault_token
    assert_failure
    assert_output -p "vault_jwt called"
    refute_output -p "curl -s ${VAULT_ADDR}/v1/auth/kubeocp-auth/login -X POST -d {\"jwt\":\"${expected_jwt}\",\"role\":\"${PROJECT_NAMESPACE}-role\"}"
    refute_output -p "jq -r .auth.client_token called"
    refute_output -p "${expected_token}"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_token fails if curl fails" {
    source ${profile_script}

    local expected_jwt='abc123'
    local expected_token='123.abc'

    function vault_jwt() { echo "vault_jwt called" >&2; echo 'abc123'; }
    function curl() { echo "curl ${*} called" >&2; return 1; }
    function jq() { echo "jq ${*} called" >&2; echo '123.abc'; }

    run vault_token
    assert_failure
    assert_output -p "vault_jwt called"
    assert_output -p "curl -s ${VAULT_ADDR}/v1/auth/kubeocp-auth/login -X POST -d {\"jwt\":\"${expected_jwt}\",\"role\":\"${PROJECT_NAMESPACE}-role\"}"
    refute_output -p "jq -r .auth.client_token called"
    refute_output -p "${expected_token}"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_keystore_exists gets the vault_token when token is not provided, and checks for the existence of the keystore, and returns true if it exists" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_token() { echo 'vault_token called' >&2; echo 'abc123'; }
    function curl() { echo "curl ${*} called" >&2; echo 'exists'; }

    run vault_keystore_exists
    assert_success
    assert_output -p 'vault_token called'
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} ${VAULT_ADDR}/v1/sys/mounts/applications/${PROJECT_NAMESPACE}/kv/tune called"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_keystore_exists returns 5 if a token is not provided and cannot be fetched" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_token() { echo 'vault_token called' >&2; return 5; }
    function curl() { echo "curl ${*} called" >&2; echo 'exists'; }

    run vault_keystore_exists
    assert_failure 5
    assert_output -p 'vault_token called'
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} ${VAULT_ADDR}/v1/sys/mounts/applications/${PROJECT_NAMESPACE}/kv/tune called"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_keystore_exists uses token when provided, checks for the existence of the keystore, and returns true if it exists" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_token() { echo 'vault_token called' >&2; echo 'abc123'; }
    function curl() { echo "curl ${*} called" >&2; echo 'exists'; }

    run vault_keystore_exists "${expected_vault_token}"
    assert_success
    refute_output -p 'vault_token called'
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} ${VAULT_ADDR}/v1/sys/mounts/applications/${PROJECT_NAMESPACE}/kv/tune called"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_keystore_exists returns 5 if it cannot determine if key exists" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function curl() { echo "curl ${*} called" >&2; return 5; }

    run vault_keystore_exists "${expected_vault_token}"
    assert_failure 5
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} ${VAULT_ADDR}/v1/sys/mounts/applications/${PROJECT_NAMESPACE}/kv/tune called"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_keystore_exists checks for the existence of the keystore, and returns 1 if it does not exist" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function curl() { echo "curl ${*} called" >&2; echo "cannot fetch sysview for path"; }

    run vault_keystore_exists "${expected_vault_token}"
    assert_failure
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} ${VAULT_ADDR}/v1/sys/mounts/applications/${PROJECT_NAMESPACE}/kv/tune called"
}

@test "$(basename ${BATS_TEST_FILENAME})#vault_keystore_exists checks for the existence of the keystore, and returns true if it does exist" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function curl() { echo "curl ${*} called" >&2; echo "exists"; }

    run vault_keystore_exists "${expected_vault_token}"
    assert_success
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} ${VAULT_ADDR}/v1/sys/mounts/applications/${PROJECT_NAMESPACE}/kv/tune called"
}

@test "$(basename ${BATS_TEST_FILENAME})#initialize_vault_keystore gets the vault_token when token is not provided, checks for the existence of the keystore, and creates it if it does not exist" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_token() { echo 'vault_token called' >&2; echo 'abc123'; }
    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; return 1; }
    function curl() { echo "curl ${*} called" >&2; }

    run initialize_vault_keystore
    assert_success
    assert_output -p 'vault_token called'
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"type\":\"kv-v2\"} ${VAULT_ADDR}/v1/sys/mounts/applications/${PROJECT_NAMESPACE}/kv"
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"max_versions\":4} ${VAULT_ADDR}/v1/$(keystore_path)/config"
}

@test "$(basename ${BATS_TEST_FILENAME})#initialize_vault_keystore returns 5 if token is not provided and it cannot be fetched" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_token() { echo 'vault_token called' >&2; return 5; }
    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; }
    function curl() { echo "curl ${*} called" >&2; }

    run initialize_vault_keystore
    assert_failure 5
    assert_output -p 'vault_token called'
    refute_output -p "vault_keystore_exists ${expected_vault_token} called"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"type\":\"kv-v2\"} ${VAULT_ADDR}/v1/sys/mounts/applications/${PROJECT_NAMESPACE}/kv"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"max_versions\":4} ${VAULT_ADDR}/v1/$(keystore_path)/config"
}

@test "$(basename ${BATS_TEST_FILENAME})#initialize_vault_keystore uses token when provided, checks for the existence of the keystore, and creates it if it does not exist" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_token() { echo 'vault_token called' >&2; echo 'abc123'; }
    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; return 1; }
    function curl() { echo "curl ${*} called" >&2; }

    run initialize_vault_keystore "${expected_vault_token}"
    assert_success
    refute_output -p 'vault_token called'
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"type\":\"kv-v2\"} ${VAULT_ADDR}/v1/sys/mounts/applications/${PROJECT_NAMESPACE}/kv"
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"max_versions\":4} ${VAULT_ADDR}/v1/$(keystore_path)/config"
}

@test "$(basename ${BATS_TEST_FILENAME})#initialize_vault_keystore does not create keystore if it already exists" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; }
    function curl() { echo "curl ${*} called" >&2; }

    run initialize_vault_keystore "${expected_vault_token}"
    assert_success
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"type\":\"kv-v2\"} ${VAULT_ADDR}/v1/sys/mounts/applications/${PROJECT_NAMESPACE}/kv"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"max_versions\":4} ${VAULT_ADDR}/v1/$(keystore_path)/config"
}

@test "$(basename ${BATS_TEST_FILENAME})#initialize_vault_keystore returns 5 if a fatal error occurs when checking for keystore existence" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; return 5; }
    function curl() { echo "curl ${*} called" >&2; }

    run initialize_vault_keystore "${expected_vault_token}"
    assert_failure 5
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"type\":\"kv-v2\"} ${VAULT_ADDR}/v1/sys/mounts/applications/${PROJECT_NAMESPACE}/kv"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"max_versions\":4} ${VAULT_ADDR}/v1/$(keystore_path)/config"
}

@test "$(basename ${BATS_TEST_FILENAME})#initialize_vault_keystore returns 5 if it cannot create the keystore" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; return 1; }
    function curl() { echo "curl ${*} called" >&2; return 5; }

    run initialize_vault_keystore "${expected_vault_token}"
    assert_failure 5
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"type\":\"kv-v2\"} ${VAULT_ADDR}/v1/sys/mounts/applications/${PROJECT_NAMESPACE}/kv"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"max_versions\":4} ${VAULT_ADDR}/v1/$(keystore_path)/config"
}

@test "$(basename ${BATS_TEST_FILENAME})#initialize_vault_keystore returns 5 if it cannot configure the max_versions for the keystore" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; return 1; }
    function curl() { echo "curl ${*} called" >&2; echo "${*}" | grep -v "max_versions"; }

    run initialize_vault_keystore "${expected_vault_token}"
    assert_failure 5
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"type\":\"kv-v2\"} ${VAULT_ADDR}/v1/sys/mounts/applications/${PROJECT_NAMESPACE}/kv"
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"max_versions\":4} ${VAULT_ADDR}/v1/$(keystore_path)/config"
}

@test "$(basename ${BATS_TEST_FILENAME})#application_stack_environment_name returns value of APPLICATION_STACK_ENVIRONMENT_NAME environment variable if set" {
    source ${profile_script}

    local expected_stack_name='abc123'
    export APPLICATION_STACK_ENVIRONMENT_NAME=${expected_stack_name}

    run application_stack_environment_name
    assert_success
    assert_output "${expected_stack_name}"
}

@test "$(basename ${BATS_TEST_FILENAME})#application_stack_environment_name returns value of CI_ENVIRONMENT_NAME if APPLICATION_STACK_ENVIRONMENT_NAME environment variable is not set" {
    source ${profile_script}

    local expected_stack_name="${CI_ENVIRONMENT_NAME}"

    run application_stack_environment_name
    assert_success
    assert_output "${expected_stack_name}"
}

@test "$(basename ${BATS_TEST_FILENAME})#get_vault_data fails if token is not supplied" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; }
    function curl() { echo "curl ${*} called" >&2; }

    run get_vault_data
    assert_failure
    refute_output -p "vault_keystore_exists ${expected_vault_token} called"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} ${VAULT_ADDR}/v1/$(keystore_path)/data/$(application_stack_environment_name) called"
}

@test "$(basename ${BATS_TEST_FILENAME})#get_vault_data fails if keystore does not exist" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; return 1; }
    function curl() { echo "curl ${*} called" >&2; }

    run get_vault_data ${expected_vault_token}
    assert_failure
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} ${VAULT_ADDR}/v1/$(keystore_path)/data/$(application_stack_environment_name) called"
}

@test "$(basename ${BATS_TEST_FILENAME})#get_vault_data returns 5 if a fatal error occurs checking for keystore" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; return 5; }
    function curl() { echo "curl ${*} called" >&2; }

    run get_vault_data ${expected_vault_token}
    assert_failure 5
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} ${VAULT_ADDR}/v1/$(keystore_path)/data/$(application_stack_environment_name) called"
}

@test "$(basename ${BATS_TEST_FILENAME})#get_vault_data returns the latest version if a version is not supplied" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; }
    function curl() { echo "curl ${*} called" >&2; }

    run get_vault_data ${expected_vault_token}
    assert_success
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} ${VAULT_ADDR}/v1/$(keystore_path)/data/$(application_stack_environment_name) called"
}

@test "$(basename ${BATS_TEST_FILENAME})#get_vault_data returns the specified version when a version is supplied" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_version=2

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; }
    function curl() { echo "curl ${*} called" >&2; }

    run get_vault_data ${expected_vault_token} ${expected_version}
    assert_success
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} ${VAULT_ADDR}/v1/$(keystore_path)/data/$(application_stack_environment_name)?version=${expected_version} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#set_vault_data fails if token is not supplied" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_data='{"test":"value"}'

    function initialize_vault_keystore() { echo "initialize_vault_keystore ${*} called" >&2; }
    function curl() { echo "curl ${*} called" >&2; }

    run set_vault_data
    assert_failure
    refute_output -p "initialize_vault_keystore ${expected_vault_token} called"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"data\":${expected_data}} ${VAULT_ADDR}/v1/$(keystore_path)/data/$(application_stack_environment_name)"
}

@test "$(basename ${BATS_TEST_FILENAME})#set_vault_data returns error from initialize_vault_keystore" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_data='{"test":"value"}'
    local expected_error=4

    function initialize_vault_keystore() { echo "initialize_vault_keystore ${*} called" >&2; return 4; }
    function curl() { echo "curl ${*} called" >&2; }

    run set_vault_data ${expected_vault_token} ${expected_data}
    assert_failure ${expected_error}
    assert_output -p "initialize_vault_keystore ${expected_vault_token} called"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"data\":${expected_data}} ${VAULT_ADDR}/v1/$(keystore_path)/data/$(application_stack_environment_name)"
}

@test "$(basename ${BATS_TEST_FILENAME})#set_vault_data returns 5 if a fatal error occurs when checking for keystore existence" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_data='{"test":"value"}'

    function initialize_vault_keystore() { echo "initialize_vault_keystore ${*} called" >&2; return 5; }
    function curl() { echo "curl ${*} called" >&2; }

    run set_vault_data ${expected_vault_token} ${expected_data}
    assert_failure 5
    assert_output -p "initialize_vault_keystore ${expected_vault_token} called"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"data\":${expected_data}} ${VAULT_ADDR}/v1/$(keystore_path)/data/$(application_stack_environment_name) "
}

@test "$(basename ${BATS_TEST_FILENAME})#set_vault_data skips and returns if no data is supplied" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_data='{"test":"value"}'

    function initialize_vault_keystore() { echo "initialize_vault_keystore ${*} called" >&2; }
    function curl() { echo "curl ${*} called" >&2; }

    run set_vault_data ${expected_vault_token}
    assert_success
    assert_output -p "skipping: no data supplied"
    refute_output -p "initialize_vault_keystore ${expected_vault_token} called"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"data\":${expected_data}} ${VAULT_ADDR}/v1/$(keystore_path)/data/$(application_stack_environment_name) "
}

@test "$(basename ${BATS_TEST_FILENAME})#set_vault_data sets the data on the keystore" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_data='{"test":"value"}'

    function initialize_vault_keystore() { echo "initialize_vault_keystore ${*} called" >&2; }
    function curl() { echo "curl ${*} called" >&2; }

    run set_vault_data ${expected_vault_token} ${expected_data}
    assert_success
    assert_output -p "initialize_vault_keystore ${expected_vault_token} called"
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"data\":${expected_data}} ${VAULT_ADDR}/v1/$(keystore_path)/data/$(application_stack_environment_name)"
}

@test "$(basename ${BATS_TEST_FILENAME})#merge_vault_data fails if token is not supplied" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local merge_data='{"test":"new_value"}'
    local expected_new_data='{"test":"new_value","other":"other_value"}'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"old_value","other":"other_value"}}}'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run merge_vault_data
    assert_failure
    assert_output -p 'vault token required!'
    refute_output -p "get_vault_data ${expected_vault_token} called"
    refute_output -p "set_vault_data ${expected_vault_token} ${expected_new_data} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#merge_vault_data fails if data is not supplied" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local merge_data='{"test":"new_value"}'
    local expected_new_data='{"test":"new_value","other":"other_value"}'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"old_value","other":"other_value"}}}'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run merge_vault_data ${expected_vault_token}
    assert_failure
    assert_output -p 'data is required!'
    refute_output -p "get_vault_data ${expected_vault_token} called"
    refute_output -p "set_vault_data ${expected_vault_token} ${expected_new_data} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#merge_vault_data fails if get_vault_data returns 5" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_data='{"test":"value"}'
    local expected_status=5

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; return 5; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run merge_vault_data ${expected_vault_token} ${expected_data}
    assert_failure ${expected_status}
    assert_output -p "get_vault_data ${expected_vault_token} called"
    refute_output -p "set_vault_data ${expected_vault_token} ${expected_data} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#merge_vault_data sets value to data if get_vault_data returns 2" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_data='{"test":"value"}'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; return 2; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run merge_vault_data ${expected_vault_token} ${expected_data}
    assert_success
    assert_output -p "get_vault_data ${expected_vault_token} called"
    assert_output -p "set_vault_data ${expected_vault_token} ${expected_data} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#merge_vault_data sets value to data if data does not already exist" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_data='{"test":"value"}'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo 'null'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run merge_vault_data ${expected_vault_token} ${expected_data}
    assert_success
    assert_output -p "get_vault_data ${expected_vault_token} called"
    assert_output -p "set_vault_data ${expected_vault_token} ${expected_data} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#merge_vault_data sets merged data" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local merge_data='{"test":"new_value"}'
    local expected_new_data='{"test":"new_value","other":"other_value"}'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"old_value","other":"other_value"}}}'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run merge_vault_data ${expected_vault_token} ${merge_data}
    assert_success
    assert_output -p "get_vault_data ${expected_vault_token} called"
    assert_output -p "set_vault_data ${expected_vault_token} ${expected_new_data} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#merge_vault_data fails if set_vault_data fails" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local merge_data='{"test":"new_value"}'
    local expected_new_data='{"test":"new_value","other":"other_value"}'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"old_value","other":"other_value"}}}'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; return 5; }

    run merge_vault_data ${expected_vault_token} ${merge_data}
    assert_failure
    assert_output -p "get_vault_data ${expected_vault_token} called"
    assert_output -p "set_vault_data ${expected_vault_token} ${expected_new_data} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#decommission_vault_data deletes the latest version of the data if a version is not supplied" {
    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; }
    function curl() { echo "curl ${*} called" >&2; }

    run decommission_vault_data ${expected_vault_token}
    assert_success
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X DELETE ${VAULT_ADDR}/v1/$(keystore_path)/data/$(application_stack_environment_name) "
}

@test "$(basename ${BATS_TEST_FILENAME})#decommission_vault_data deletes the the specified version of the data if a version is supplied" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_version=4

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; }
    function curl() { echo "curl ${*} called" >&2; }

    run decommission_vault_data ${expected_vault_token} ${expected_version}
    assert_success
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"versions\":[${expected_version}]} ${VAULT_ADDR}/v1/$(keystore_path)/delete/$(application_stack_environment_name)"
}

@test "$(basename ${BATS_TEST_FILENAME})#decommission_vault_data does not delete the the data if the keystore does not exist" {
    source ${profile_script}

    local expected_vault_token='abc123'

    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; return 1; }
    function curl() { echo "curl ${*} called" >&2; }

    run decommission_vault_data ${expected_vault_token}
    assert_success
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"versions\":[${expected_version}]} ${VAULT_ADDR}/v1/$(keystore_path)/delete/$(application_stack_environment_name)"
}

@test "$(basename ${BATS_TEST_FILENAME})#decommission_vault_data returns 5 if a fatal error occurs checking for vault keystore existence" {
    source ${profile_script}

    local expected_vault_token='abc123'

    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; return 5; }
    function curl() { echo "curl ${*} called" >&2; }

    run decommission_vault_data ${expected_vault_token}
    assert_failure 5
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"versions\":[${expected_version}]} ${VAULT_ADDR}/v1/$(keystore_path)/delete/$(application_stack_environment_name)"
}

@test "$(basename ${BATS_TEST_FILENAME})#decommission_vault_data fails if token is not provided" {
    source ${profile_script}

    local expected_vault_token='abc123'

    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; return 5; }
    function curl() { echo "curl ${*} called" >&2; }

    run decommission_vault_data
    assert_failure
    assert_output -p 'vault token required!'
    refute_output -p "vault_keystore_exists ${expected_vault_token} called"
    refute_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X POST -d {\"versions\":[${expected_version}]} ${VAULT_ADDR}/v1/$(keystore_path)/delete/$(application_stack_environment_name)"
}

@test "$(basename ${BATS_TEST_FILENAME})#decommission_vault_data fails if a fatal error occurs deleting vault data" {
    source ${profile_script}

    local expected_vault_token='abc123'

    source ${profile_script}

    local expected_vault_token='abc123'

    function vault_keystore_exists() { echo "vault_keystore_exists ${*} called" >&2; }
    function curl() { echo "curl ${*} called" >&2; return 1; }

    run decommission_vault_data ${expected_vault_token}
    assert_failure
    assert_output -p "vault_keystore_exists ${expected_vault_token} called"
    assert_output -p "curl -s -H X-Vault-Token: ${expected_vault_token} -X DELETE ${VAULT_ADDR}/v1/$(keystore_path)/data/$(application_stack_environment_name)"
}

@test "$(basename ${BATS_TEST_FILENAME})#get_vault_key_value fails if token is not supplied" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_value='value'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data:{"data":{"test":"value"}}}'; }

    run get_vault_key_value
    assert_failure
    assert_output -p 'vault token required!'
    refute_output -p "get_vault_data ${expected_vault_token} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#get_vault_key_value fails if key is not supplied" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_value='value'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data:{"data":{"test":"value"}}}'; }

    run get_vault_key_value ${expected_vault_token}
    assert_failure
    assert_output -p 'key is required!'
    refute_output -p "get_vault_data ${expected_vault_token} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#get_vault_key_value returns empty string if keystore is not initialized" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_value='value'

    function get_vault_data() { return 2; }

    returned_value=$(get_vault_key_value ${expected_vault_token} ${expected_key})
    assert [ $? -eq 0 ]
    assert [ -z "${returned_value}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#get_vault_key_value returns any error except 2 returned from get_vault_data" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_value='value'
    local expected_error=4

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; return 4; }

    run get_vault_key_value ${expected_vault_token} ${expected_key}
    assert_failure ${expected_error}
    assert_output -p "get_vault_data ${expected_vault_token} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#get_vault_key_value returns the value" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_value='the value'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"the value"}}}'; }

    returned_value=$(get_vault_key_value ${expected_vault_token} ${expected_key})
    assert [ $? -eq 0 ]
    assert_equal "${returned_value}" "${expected_value}"

    run get_vault_key_value ${expected_vault_token} ${expected_key}
    assert_success
    assert_output -p "get_vault_data ${expected_vault_token} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#get_vault_key_value returns empty string if key does not exist" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='notthere'

    function get_vault_data() { echo '{"data":{"data":{"test":"the value"}}}'; }

    returned_value=$(get_vault_key_value ${expected_vault_token} ${expected_key})
    assert [ -z "${returned_value}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#set_vault_key_value fails if token is not supplied" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_value='new_value'
    local expected_new_data='{"test":"new_value"}'

    function get_vault_key_value() { echo "get_vault_key_value ${*} called" >&2; echo "old_value"; }
    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"old_value"}}}'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run set_vault_key_value
    assert_failure
    assert_output -p 'vault token required!'
    refute_output -p "get_vault_key_value ${expected_vault_token} ${key} called"
    refute_output -p "get_vault_data ${expected_vault_token} called"
    refute_output -p "set_vault_data ${expected_vault_token} ${expected_new_data} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#set_vault_key_value fails if key is not supplied" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_value='new_value'
    local expected_new_data='{"test":"new_value"}'

    function get_vault_key_value() { echo "get_vault_key_value ${*} called" >&2; echo "old_value"; }
    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"old_value"}}}'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run set_vault_key_value ${expected_vault_token}
    assert_output -p 'key is required!'
    refute_output -p "get_vault_key_value ${expected_vault_token} ${key} called"
    refute_output -p "get_vault_data ${expected_vault_token} called"
    refute_output -p "set_vault_data ${expected_vault_token} ${expected_new_data} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#set_vault_key_value skips if value is not supplied" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_value='new_value'
    local expected_new_data='{"test":"new_value"}'

    function get_vault_key_value() { echo "get_vault_key_value ${*} called" >&2; echo "old_value"; }
    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"old_value"}}}'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run set_vault_key_value ${expected_vault_token} ${expected_key}
    assert_success
    assert_output -p 'skipping: no value supplied'
    refute_output -p "get_vault_key_value ${expected_vault_token} ${key}"
    refute_output -p "get_vault_data ${expected_vault_token} called"
    refute_output -p "set_vault_data ${expected_vault_token} ${expected_new_data}"
}

@test "$(basename ${BATS_TEST_FILENAME})#set_vault_key_value returns 5 if fatal error occurs calling get_vault_key_value" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_value='new_value'
    local expected_new_data='{"test":"new_value"}'

    function get_vault_key_value() { echo "get_vault_key_value ${*} called" >&2; return 5; }
    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"old_value"}}}'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run set_vault_key_value ${expected_vault_token} ${expected_key} ${expected_value}
    assert_failure 5
    assert_output -p "get_vault_key_value ${expected_vault_token} ${key}"
    refute_output -p "get_vault_data ${expected_vault_token} called"
    refute_output -p "set_vault_data ${expected_vault_token} ${expected_new_data}"
}

@test "$(basename ${BATS_TEST_FILENAME})#set_vault_key_value skips if value is not changed" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_value='old_value'
    local expected_new_data='{"test":"old_value"}'

    function get_vault_key_value() { echo "get_vault_key_value ${*} called" >&2; echo "old_value"; }
    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"old_value"}}}'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run set_vault_key_value ${expected_vault_token} ${expected_key} ${expected_value}
    assert_success
    assert_output -p "get_vault_key_value ${expected_vault_token} ${key}"
    assert_output -p 'skipping: value not changed'
    refute_output -p "get_vault_data ${expected_vault_token} called"
    refute_output -p "set_vault_data ${expected_vault_token} ${expected_new_data}"
}

@test "$(basename ${BATS_TEST_FILENAME})#set_vault_key_value sets new value if non fatal error is returned from get_vault_key_value and get_vault_data" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_value='new_value'
    local expected_new_data='{"test":"new_value"}'

    function get_vault_key_value() { echo "get_vault_key_value ${*} called" >&2; return 1; }
    function get_vault_data() { echo "get_vault_data ${*} called" >&2; return 1; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run set_vault_key_value ${expected_vault_token} ${expected_key} ${expected_value}
    assert_success
    assert_output -p "get_vault_key_value ${expected_vault_token} ${key}"
    assert_output -p "get_vault_data ${expected_vault_token} called"
    assert_output -p "set_vault_data ${expected_vault_token} ${expected_new_data}"
}

@test "$(basename ${BATS_TEST_FILENAME})#set_vault_key_value sets value if value is changed" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_value='new_value'
    local expected_new_data='{"test":"new_value"}'

    function get_vault_key_value() { echo "get_vault_key_value ${*} called" >&2; echo "old_value"; }
    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"old_value"}}}'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run set_vault_key_value ${expected_vault_token} ${expected_key} ${expected_value}
    assert_success
    assert_output -p "get_vault_key_value ${expected_vault_token} ${key}"
    assert_output -p "get_vault_data ${expected_vault_token} called"
    assert_output -p "set_vault_data ${expected_vault_token} ${expected_new_data}"
}

@test "$(basename ${BATS_TEST_FILENAME})#set_vault_key_value fails if set_vault_data fails" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_value='new_value'
    local expected_new_data='{"test":"new_value"}'

    function get_vault_key_value() { echo "get_vault_key_value ${*} called" >&2; echo "old_value"; }
    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"old_value"}}}'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; return 1; }

    run set_vault_key_value ${expected_vault_token} ${expected_key} ${expected_value}
    assert_failure
    assert_output -p "get_vault_key_value ${expected_vault_token} ${key}"
    assert_output -p "get_vault_data ${expected_vault_token} ${key}"
    assert_output -p "set_vault_data ${expected_vault_token} ${expected_new_data}"
}

@test "$(basename ${BATS_TEST_FILENAME})#decommission_vault_key_value fails if token is not provided" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_new_data='{"remaining":"remaining_value"}'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"new_value","remaining":"remaining_value"}}}'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run decommission_vault_key_value
    assert_failure
    assert_output -p 'vault token required!'
    refute_output -p "get_vault_data ${expected_vault_token} ${key}"
    refute_output -p "set_vault_data ${expected_vault_token} ${expected_new_data}"
}

@test "$(basename ${BATS_TEST_FILENAME})#decommission_vault_key_value fails if key is not provided" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_new_data='{"remaining":"remaining_value"}'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"new_value","remaining":"remaining_value"}}}'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run decommission_vault_key_value ${expected_vault_token}
    assert_failure
    assert_output -p 'key is required!'
    refute_output -p "get_vault_data ${expected_vault_token} ${key}"
    refute_output -p "set_vault_data ${expected_vault_token} ${expected_new_data}"
}

@test "$(basename ${BATS_TEST_FILENAME})#decommission_vault_key_value succeeds without deleting key if key_store does not exist" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_new_data='{"remaining":"remaining_value"}'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; return 2; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run decommission_vault_key_value ${expected_vault_token} ${expected_key}
    assert_success
    assert_output -p "get_vault_data ${expected_vault_token} ${key}"
    refute_output -p "set_vault_data ${expected_vault_token} ${expected_new_data}"
}

@test "$(basename ${BATS_TEST_FILENAME})#decommission_vault_key_value returns 5 if get_vault_data returns 5" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_new_data='{"remaining":"remaining_value"}'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; return 5; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run decommission_vault_key_value ${expected_vault_token} ${expected_key}
    assert_failure 5
    assert_output -p "get_vault_data ${expected_vault_token} ${key}"
    refute_output -p "set_vault_data ${expected_vault_token} ${expected_new_data}"
}

@test "$(basename ${BATS_TEST_FILENAME})#decommission_vault_key_value succeeds without setting new data if get_vault_data returns an empty string" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_new_data='{"remaining":"remaining_value"}'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo ""; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run decommission_vault_key_value ${expected_vault_token} ${expected_key}
    assert_success
    assert_output -p "get_vault_data ${expected_vault_token} ${key}"
    refute_output -p "set_vault_data ${expected_vault_token} ${expected_new_data}"
}

@test "$(basename ${BATS_TEST_FILENAME})#decommission_vault_key_value deletes the specified key from the data and creates a new version" {
    source ${profile_script}

    local expected_vault_token='abc123'
    local expected_key='test'
    local expected_new_data='{"remaining":"remaining_value"}'

    function get_vault_data() { echo "get_vault_data ${*} called" >&2; echo '{"data":{"data":{"test":"new_value","remaining":"remaining_value"}}}'; }
    function set_vault_data() { echo "set_vault_data ${*} called" >&2; }

    run decommission_vault_key_value ${expected_vault_token} ${expected_key}
    assert_success
    assert_output -p "get_vault_data ${expected_vault_token} ${key}"
    assert_output -p "set_vault_data ${expected_vault_token} ${expected_new_data}"
}
