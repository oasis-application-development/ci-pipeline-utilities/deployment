#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/bin/scale_deployment.sh"

setup() {
  export LOG_LEVEL="info"
  export HELM_CHART_NAME='HELM_CHART_NAME'
  export CI_ENVIRONMENT_SLUG='CI_ENVIRONMENT_SLUG'
  export DEFAULT_SCALE_TARGET='deployments'
  export DEFAULT_SCALE_REPLICAS=1
}

teardown() {
  unset SLEEP_BETWEEN_SCALE
  unset WAIT_SCALE_TIMEOUT
  unset SCALE_REPLICAS
}

@test "$(basename ${BATS_TEST_FILENAME})#check_scale_deployment_requirements fails if check_orbs_label_requirements fails" {
  source ${profile_script}
  
  function check_orbs_label_requirements() { echo "check_orbs_label_requirements called" >&2; return 1; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; return; }

  run check_scale_deployment_requirements
  assert_failure
  assert_output -p "check_orbs_label_requirements called"
  refute_output -p "check_required_cluster_login_environment called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_scale_deployment_requirements fails if check_required_cluster_login_environment fails" {
  source ${profile_script}
  
  function check_orbs_label_requirements() { echo "check_orbs_label_requirements called" >&2; return; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; return 1; }

  run check_scale_deployment_requirements
  assert_failure
  assert_output -p "check_orbs_label_requirements called"
  assert_output -p "check_required_cluster_login_environment called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_scale_deployment_requirements passes if all checks pass" {
  source ${profile_script}
  
  function check_orbs_label_requirements() { echo "check_orbs_label_requirements called" >&2; return; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; return; }

  run check_scale_deployment_requirements
  assert_success
  assert_output -p "check_orbs_label_requirements called"
  assert_output -p "check_required_cluster_login_environment called"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main runs checks environment, calls cluster_login, and then calls restart_orbs_deployments with oa_scale_deployment_restart=true extra_labels" {
  source ${profile_script}

  function deployment_image_version_info() { echo "deployment_image_version_info called" >&2; }
  function check_scale_deployment_requirements() { echo "check_scale_deployment_requirements called" >&2; }
  function cluster_login() { echo "cluster_login called" >&2; }
  function restart_orbs_deployments() { echo "restart_orbs_deployments ${*}" >&2; }

  run run_main
  assert_success
  assert_output -p "deployment_image_version_info called"
  assert_output -p "check_scale_deployment_requirements called"
  assert_output -p "cluster_login called"
  assert_output -p "restart_orbs_deployments oa_scale_deployment_restart=true"
  assert_output -p "ALL COMPLETE"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if check_scale_deployment_requirements fails" {
  source ${profile_script}

  function deployment_image_version_info() { echo "deployment_image_version_info called" >&2; }
  function check_scale_deployment_requirements() { echo "check_scale_deployment_requirements called" >&2; return 1; }
  function cluster_login() { echo "cluster_login called" >&2; }
  function restart_orbs_deployments() { echo "restart_orbs_deployments ${*}" >&2; }

  run run_main
  assert_failure
  assert_output -p "deployment_image_version_info called"
  assert_output -p "check_scale_deployment_requirements called"
  refute_output -p "cluster_login called"
  refute_output -p "restart_orbs_deployments oa_scale_deployment_restart=true"
  refute_output -p "ALL COMPLETE"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if cluster_login fails" {
  source ${profile_script}

  function deployment_image_version_info() { echo "deployment_image_version_info called" >&2; }
  function check_scale_deployment_requirements() { echo "check_scale_deployment_requirements called" >&2; }
  function cluster_login() { echo "cluster_login called" >&2; return 1; }
  function restart_orbs_deployments() { echo "restart_orbs_deployments ${*}" >&2; }

  run run_main
  assert_failure
  assert_output -p "deployment_image_version_info called"
  assert_output -p "check_scale_deployment_requirements called"
  assert_output -p "cluster_login called"
  refute_output -p "restart_orbs_deployments oa_scale_deployment_restart=true"
  refute_output -p "ALL COMPLETE"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if restart_orbs_deployments fails" {
  source ${profile_script}

  function deployment_image_version_info() { echo "deployment_image_version_info called" >&2; }
  function check_scale_deployment_requirements() { echo "check_scale_deployment_requirements called" >&2; }
  function cluster_login() { echo "cluster_login called" >&2; }
  function restart_orbs_deployments() { echo "restart_orbs_deployments ${*}" >&2; return 1; }

  run run_main
  assert_failure
  assert_output -p "deployment_image_version_info called"
  assert_output -p "check_scale_deployment_requirements called"
  assert_output -p "cluster_login called"
  assert_output -p "restart_orbs_deployments oa_scale_deployment_restart=true"
  refute_output -p "ALL COMPLETE"
}
