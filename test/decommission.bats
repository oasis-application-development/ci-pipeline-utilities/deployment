#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/bin/decommission"
expected_deployment_name="expected_deployment_name"

setup() {
  export LOG_LEVEL="info"
}

@test "$(basename ${BATS_TEST_FILENAME}) decommission_template prints information and returns during dry_run" {
  source ${profile_script}

  function dry_run() { return; }
  function deployment_name() { echo "deployment_name called" >&2; echo "${expected_deployment_name}"; }
  function helm() { echo "helm ${*}" >&2; }

  run decommission_template
  assert_success
  assert_output -p "decommissioning template"
  assert_output -p "deployment_name called"
  refute_output -e "helm delete"
}

@test "$(basename ${BATS_TEST_FILENAME}) decommission_template deletes the template with helm" {
  source ${profile_script}

  function deployment_name() { echo "deployment_name called" >&2; echo "${expected_deployment_name}"; }
  function helm() { echo "helm ${*}" >&2; }

  run decommission_template
  assert_success
  assert_output -p "decommissioning template"
  assert_output -p "deployment_name called"
  assert_output -p "helm delete ${expected_deployment_name}"
}

@test "$(basename ${BATS_TEST_FILENAME}) decommission_template fails if helm fails" {
  source ${profile_script}

  function deployment_name() { echo "deployment_name called" >&2; echo "${expected_deployment_name}"; }
  function helm() { echo "helm ${*}" >&2; return 1; }

  run decommission_template
  assert_failure
  assert_output -p "decommissioning template"
  assert_output -p "deployment_name called"
  assert_output -p "helm delete ${expected_deployment_name}"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main runs check_helm_decommission_requirements. logs into the cluster, and decommissions the template" {
  source ${profile_script}

  function check_helm_decommission_requirements() { echo "check_helm_decommission_requirements called" >&2; }
  function cluster_login() { echo "cluster_login" >&2; }
  function decommission_template { echo "decommission_template" >&2; }
 
  run run_main
  assert_success
  assert_output -p "check_helm_decommission_requirements called"
  assert_output -p "cluster_login"
  assert_output -p "decommission_template"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if check_helm_decommission_requirements fails" {
  source ${profile_script}

  function check_helm_decommission_requirements() { echo "check_helm_decommission_requirements called" >&2; return 1; }
  function cluster_login() { echo "cluster_login" >&2; }
  function decommission_template { echo "decommission_template" >&2; }

  run run_main
  assert_failure
  assert_output -p "check_helm_decommission_requirements called"
  refute_output -p "cluster_login"
  refute_output -p "decommission_template"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if cluster_login fails" {
  source ${profile_script}

  function check_helm_decommission_requirements() { echo "check_helm_decommission_requirements called" >&2; }
  function cluster_login() { echo "cluster_login" >&2; return 1; }
  function decommission_template { echo "decommission_template" >&2; }

  run run_main
  assert_failure
  assert_output -p "check_helm_decommission_requirements called"
  assert_output -p "cluster_login"
  refute_output -p "decommission_template"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if decommission_template fails" {
  source ${profile_script}

  function check_helm_decommission_requirements() { echo "check_helm_decommission_requirements called" >&2; }
  function cluster_login() { echo "cluster_login" >&2; }
  function decommission_template { echo "decommission_template" >&2; return 1; }

  run run_main
  assert_failure
  assert_output -p "check_helm_decommission_requirements called"
  assert_output -p "cluster_login"
  assert_output -p "decommission_template"
}
