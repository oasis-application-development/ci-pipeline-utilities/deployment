#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/lib/helpers/helm"

setup_fake_helm_chart() {
  mkdir helm-chart
  if [ -n "${HELM_CHART_NAME}" ]
  then
    mkdir helm-chart/${HELM_CHART_NAME}
  else
    mkdir helm-chart/${CI_PROJECT_NAME}
  fi
}

cleanup_fake_helm_chart() {
  helm_chart_name="${CI_PROJECT_NAME}"
  if [ -n "${HELM_CHART_NAME}" ]
  then
    helm_chart_name="${HELM_CHART_NAME}"
  fi

  if [ -d "helm-chart"/${helm_chart_name} ]
  then
    rmdir "helm-chart/${helm_chart_name}"
  fi
  if [ -d "helm-chart" ]
  then
    rmdir helm-chart
  fi
}

setup() {
  source "/usr/local/lib/helpers/common"
  export LOG_LEVEL=info
  export CI_PROJECT_NAME='CI_PROJECT_NAME'
  export HELM_TOKEN="HELM_TOKEN"
  export HELM_USER="HELM_USER"
  export PROJECT_NAMESPACE="PROJECT_NAMESPACE"
  export CLUSTER_SERVER="CLUSTER_SERVER"
  export DEFAULT_CLUSTER_NAME="ci_kube"
  export DEFAULT_CONTEXT_NAME="${HELM_USER}-${DEFAULT_CLUSTER_NAME}-${PROJECT_NAMESPACE}"
  export CI_ENVIRONMENT_SLUG='CI_ENVIRONMENT_SLUG'
}

teardown() {
  cleanup_fake_helm_chart
  unset HELM_CHART_NAME
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_cluster_login_environment calls check_required_environment with the variables required for cluster login" {
  source ${profile_script}
  local expected_cluster_login_environment="HELM_TOKEN HELM_USER PROJECT_NAMESPACE CLUSTER_SERVER"

  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_required_cluster_login_environment
  assert_success
  assert_output -p "check_required_environment ${expected_cluster_login_environment}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_cluster_login_environment fails if check_required_environment with the variables required for cluster login fails" {
  source ${profile_script}
  local expected_cluster_login_environment="HELM_TOKEN HELM_USER PROJECT_NAMESPACE CLUSTER_SERVER"

  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }

  run check_required_cluster_login_environment
  assert_failure
  assert_output -p "check_required_environment ${expected_cluster_login_environment}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_chart_requirements checks for HELM_CHART_NAME or CI_PROJECT_NAME env, checks for helm-chart and helm-chart/CI_PROJECT_NAME when HELM_CHART_NAME is not set" {
  setup_fake_helm_chart
  assert [ -d 'helm-chart' ]
  assert [ -d "helm-chart/${CI_PROJECT_NAME}" ]
  source ${profile_script}

  expected_default_environment="HELM_CHART_NAME:CI_PROJECT_NAME"
  function check_default_environment() { echo "check_default_environment ${*} called"; }
 
  run check_helm_chart_requirements
  assert_success
  assert_output -p "check_default_environment ${expected_default_environment} called";
  refute_output -p "this project does not have a helm-chart directory!"
  refute_output -p "helm-chart does not have the expected ${CI_PROJECT_NAME} chart directory!"
  refute_output -p "helm-chart does not have the expected ${HELM_CHART_NAME} chart directory!"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_chart_requirements checks for HELM_CHART_NAME or CI_PROJECT_NAME env, checks for helm-chart and helm-chart/HELM_CHART_NAME when HELM_CHART_NAME is set" {
  export HELM_CHART_NAME="helm-chart-name"
  setup_fake_helm_chart
  source ${profile_script}

  expected_default_environment="HELM_CHART_NAME:CI_PROJECT_NAME"
  function check_default_environment() { echo "check_default_environment ${*} called"; }

  assert [ -d 'helm-chart' ]
  assert [ -d "helm-chart/${HELM_CHART_NAME}" ]
  assert [ ! -d "helm-chart/${CI_PROJECT_NAME}" ]
  run check_helm_chart_requirements
  assert_success
  assert_output -p "check_default_environment ${expected_default_environment} called";
  refute_output -p "this project does not have a helm-chart directory!"
  refute_output -p "helm-chart does not have the expected ${CI_PROJECT_NAME} chart directory!"
  refute_output -p "helm-chart does not have the expected ${HELM_CHART_NAME} chart directory!"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_chart_requirements fails if check_default_environment fails" {
  setup_fake_helm_chart
  source ${profile_script}

  expected_default_environment="HELM_CHART_NAME:CI_PROJECT_NAME"
  function check_default_environment() { echo "check_default_environment ${*} called"; return 1; }
 
  run check_helm_chart_requirements
  assert_failure
  assert_output -p "check_default_environment ${expected_default_environment} called";
  refute_output -p "this project does not have a helm-chart directory!"
  refute_output -p "helm-chart does not have the expected ${CI_PROJECT_NAME} chart directory!"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_chart_requirements fails if helm-chart directory does not exist" {
  source ${profile_script}

  expected_default_environment="HELM_CHART_NAME:CI_PROJECT_NAME"
  function check_default_environment() { echo "check_default_environment ${*} called"; }
 
  assert [ ! -d "helm-chart" ]
  run check_helm_chart_requirements
  assert_failure
  assert_output -p "check_default_environment ${expected_default_environment} called";
  assert_output -p "this project does not have a helm-chart directory!"
  refute_output -p "helm-chart does not have the expected ${CI_PROJECT_NAME} chart directory!"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_chart_requirements fails if HELM_CHART_NAME is not set and helm-chart/CI_PROJECT_NAME directory does not exist" {
  setup_fake_helm_chart
  rmdir helm-chart/${CI_PROJECT_NAME}
  source ${profile_script}

  expected_default_environment="HELM_CHART_NAME:CI_PROJECT_NAME"
  function check_default_environment() { echo "check_default_environment ${*} called"; }
 
  assert [ -d "helm-chart" ]
  assert [ ! -d "helm-chart/${CI_PROJECT_NAME}" ]
  run check_helm_chart_requirements
  assert_failure
  assert_output -p "check_default_environment ${expected_default_environment} called";
  refute_output -p "this project does not have a helm-chart directory!"
  assert_output -p "helm-chart does not have the expected ${CI_PROJECT_NAME} chart directory!"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_chart_requirements fails if HELM_CHART_NAME is set and helm-chart/HELM_CHART_NAME directory does not exist" {
  export HELM_CHART_NAME="helm-chart-name"
  assert [ -n "${HELM_CHART_NAME}" ]
  setup_fake_helm_chart
  rmdir helm-chart/${HELM_CHART_NAME}
  assert [ -d "helm-chart" ]
  assert [ ! -d "helm-chart/${HELM_CHART_NAME}" ]
  source ${profile_script}

  expected_default_environment="HELM_CHART_NAME:CI_PROJECT_NAME"
  function check_default_environment() { echo "check_default_environment ${*} called"; }
 
  run check_helm_chart_requirements
  assert_failure
  assert_output -p "check_default_environment ${expected_default_environment} called";
  refute_output -p "this project does not have a helm-chart directory!"
  assert_output -p "helm-chart does not have the expected ${HELM_CHART_NAME} chart directory!"
}

@test "$(basename ${BATS_TEST_FILENAME})#helm_chart_name prints HELM_CHART_NAME if set" {
  source ${profile_script}

  export HELM_CHART_NAME='HELM_CHART_NAME'

  run helm_chart_name
  assert_output "${HELM_CHART_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#helm_chart_name prints CI_PROJECT_NAME if HELM_CHART_NAME is not set" {
  source ${profile_script}

  run helm_chart_name
  assert_output "${CI_PROJECT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#cluster_login prints information and exits when dry_run is true" {
  source ${profile_script}

  function dry_run() { return; }
  function kubectl() { echo "kubectl ${*}" >&2; }

  run cluster_login
  assert_success
  assert_output -p "authenticating ${HELM_USER} in ${PROJECT_NAMESPACE}"
  refute_output -p "kubectl config set-cluster ${DEFAULT_CLUSTER_NAME} --server=${CLUSTER_SERVER} --insecure-skip-tls-verify=true"
  refute_output -p "kubectl config set-credentials ${HELM_USER} --token=${HELM_TOKEN}"
  refute_output -p "kubectl config set-context ${DEFAULT_CONTEXT_NAME} --cluster=${DEFAULT_CLUSTER_NAME} --namespace=PROJECT_NAMESPACE --user=HELM_USER"
  refute_output -p "kubectl config use-context ${DEFAULT_CONTEXT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#cluster_login uses CLUSTER_NAME for cluster name if present" {
  source ${profile_script}

  export CLUSTER_NAME='test-cluster-name'
  export EXPECTED_CONTEXT_NAME="${HELM_USER}-${CLUSTER_NAME}-${PROJECT_NAMESPACE}"
  function kubectl() { echo "kubectl ${*}" >&2; }

  run cluster_login
  assert_success
  assert_output -p "authenticating ${HELM_USER} in ${PROJECT_NAMESPACE}"
  assert_output -p "kubectl config set-cluster ${CLUSTER_NAME} --server=${CLUSTER_SERVER} --insecure-skip-tls-verify=true"
  assert_output -p "kubectl config set-credentials ${HELM_USER} --token=${HELM_TOKEN}"
  assert_output -p "kubectl config set-context ${EXPECTED_CONTEXT_NAME} --cluster=${CLUSTER_NAME} --namespace=PROJECT_NAMESPACE --user=HELM_USER"
  assert_output -p "kubectl config use-context ${EXPECTED_CONTEXT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#cluster_login uses ci_kube for cluster name when CLUSTER_NAME is not set" {
  source ${profile_script}

  function kubectl() { echo "kubectl ${*}" >&2; }

  run cluster_login
  assert_success
  assert_output -p "authenticating ${HELM_USER} in ${PROJECT_NAMESPACE}"
  assert_output -p "kubectl config set-cluster ${DEFAULT_CLUSTER_NAME} --server=${CLUSTER_SERVER} --insecure-skip-tls-verify=true"
  assert_output -p "kubectl config set-credentials ${HELM_USER} --token=${HELM_TOKEN}"
  assert_output -p "kubectl config set-context ${DEFAULT_CONTEXT_NAME} --cluster=${DEFAULT_CLUSTER_NAME} --namespace=PROJECT_NAMESPACE --user=HELM_USER"
  assert_output -p "kubectl config use-context ${DEFAULT_CONTEXT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#cluster_login uses CONTEXT_NAME for context name if present" {
  source ${profile_script}

  export CONTEXT_NAME='test-context-name'
  function kubectl() { echo "kubectl ${*}" >&2; }

  run cluster_login
  assert_success
  assert_output -p "authenticating ${HELM_USER} in ${PROJECT_NAMESPACE}"
  assert_output -p "kubectl config set-cluster ${DEFAULT_CLUSTER_NAME} --server=${CLUSTER_SERVER} --insecure-skip-tls-verify=true"
  assert_output -p "kubectl config set-credentials ${HELM_USER} --token=${HELM_TOKEN}"
  assert_output -p "kubectl config set-context ${CONTEXT_NAME} --cluster=${DEFAULT_CLUSTER_NAME} --namespace=PROJECT_NAMESPACE --user=HELM_USER"
  assert_output -p "kubectl config use-context ${CONTEXT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#cluster_login uses HELM_USER-ci_kube-PROJECT_NAMESPACE for context name if CONTEXT_NAME not set" {
  source ${profile_script}

  function kubectl() { echo "kubectl ${*}" >&2; }

  run cluster_login
  assert_success
  assert_output -p "authenticating ${HELM_USER} in ${PROJECT_NAMESPACE}"
  assert_output -p "kubectl config set-cluster ${DEFAULT_CLUSTER_NAME} --server=${CLUSTER_SERVER} --insecure-skip-tls-verify=true"
  assert_output -p "kubectl config set-credentials ${HELM_USER} --token=${HELM_TOKEN}"
  assert_output -p "kubectl config set-context ${DEFAULT_CONTEXT_NAME} --cluster=${DEFAULT_CLUSTER_NAME} --namespace=PROJECT_NAMESPACE --user=HELM_USER"
  assert_output -p "kubectl config use-context ${DEFAULT_CONTEXT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#cluster_login connects kubectl to the cluster" {
  source ${profile_script}
  function kubectl() { echo "kubectl ${*}" >&2; }

  run cluster_login
  assert_success
  assert_output -p "authenticating ${HELM_USER} in ${PROJECT_NAMESPACE}"
  assert_output -p "kubectl config set-cluster ${DEFAULT_CLUSTER_NAME} --server=${CLUSTER_SERVER} --insecure-skip-tls-verify=true"
  assert_output -p "kubectl config set-credentials ${HELM_USER} --token=${HELM_TOKEN}"
  assert_output -p "kubectl config set-context ${DEFAULT_CONTEXT_NAME} --cluster=${DEFAULT_CLUSTER_NAME} --namespace=PROJECT_NAMESPACE --user=HELM_USER"
  assert_output -p "kubectl config use-context ${DEFAULT_CONTEXT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#cluster_login fails if kubectl config set-cluster fails" {
  source ${profile_script}
  function kubectl() { echo "kubectl ${*}" >&2; echo "${*}" | grep -v 'config set-cluster'; }

  run cluster_login
  assert_failure
  assert_output -p "authenticating ${HELM_USER} in ${PROJECT_NAMESPACE}"
  assert_output -p "kubectl config set-cluster ${DEFAULT_CLUSTER_NAME} --server=${CLUSTER_SERVER} --insecure-skip-tls-verify=true"
  refute_output -p "kubectl config set-credentials ${HELM_USER} --token=${HELM_TOKEN}"
  refute_output -p "kubectl config set-context ${DEFAULT_CONTEXT_NAME} --cluster=${DEFAULT_CLUSTER_NAME} --namespace=PROJECT_NAMESPACE --user=HELM_USER"
  refute_output -p "kubectl config use-context ${DEFAULT_CONTEXT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#cluster_login fails if kubectl config set-credentials fails" {
  source ${profile_script}
  function kubectl() { echo "kubectl ${*}" >&2; echo "${*}" | grep -v 'config set-credentials'; }

  run cluster_login
  assert_failure
  assert_output -p "authenticating ${HELM_USER} in ${PROJECT_NAMESPACE}"
  assert_output -p "kubectl config set-cluster ${DEFAULT_CLUSTER_NAME} --server=${CLUSTER_SERVER} --insecure-skip-tls-verify=true"
  assert_output -p "kubectl config set-credentials ${HELM_USER} --token=${HELM_TOKEN}"
  refute_output -p "kubectl config set-context ${DEFAULT_CONTEXT_NAME} --cluster=${DEFAULT_CLUSTER_NAME} --namespace=PROJECT_NAMESPACE --user=HELM_USER"
  refute_output -p "kubectl config use-context ${DEFAULT_CONTEXT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#cluster_login fails if kubectl config set-context fails" {
  source ${profile_script}
  function kubectl() { echo "kubectl ${*}" >&2; echo "${*}" | grep -v 'config set-context'; }

  run cluster_login
  assert_failure
  assert_output -p "authenticating ${HELM_USER} in ${PROJECT_NAMESPACE}"
  assert_output -p "kubectl config set-cluster ${DEFAULT_CLUSTER_NAME} --server=${CLUSTER_SERVER} --insecure-skip-tls-verify=true"
  assert_output -p "kubectl config set-credentials ${HELM_USER} --token=${HELM_TOKEN}"
  assert_output -p "kubectl config set-context ${DEFAULT_CONTEXT_NAME} --cluster=${DEFAULT_CLUSTER_NAME} --namespace=PROJECT_NAMESPACE --user=HELM_USER"
  refute_output -p "kubectl config use-context ${DEFAULT_CONTEXT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#cluster_login fails if kubectl config use-context fails" {
  source ${profile_script}
  function kubectl() { echo "kubectl ${*}" >&2; echo "${*}" | grep -v 'config use-context'; }

  run cluster_login
  assert_failure
  assert_output -p "authenticating ${HELM_USER} in ${PROJECT_NAMESPACE}"
  assert_output -p "kubectl config set-cluster ${DEFAULT_CLUSTER_NAME} --server=${CLUSTER_SERVER} --insecure-skip-tls-verify=true"
  assert_output -p "kubectl config set-credentials ${HELM_USER} --token=${HELM_TOKEN}"
  assert_output -p "kubectl config set-context ${DEFAULT_CONTEXT_NAME} --cluster=${DEFAULT_CLUSTER_NAME} --namespace=PROJECT_NAMESPACE --user=HELM_USER"
  assert_output -p "kubectl config use-context ${DEFAULT_CONTEXT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_orbs_label_requirements" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_orbs_label_requirements
  assert_output -p "check_required_environment HELM_CHART_NAME CI_ENVIRONMENT_SLUG"
}

@test "$(basename ${BATS_TEST_FILENAME})#orbs_labels" {
  source ${profile_script}
  run orbs_labels
  assert_output -p "oa_application_name=${HELM_CHART_NAME},oa_application_environment=${CI_ENVIRONMENT_SLUG}"
}

@test "$(basename ${BATS_TEST_FILENAME})#orbs_deployment_names returns every deployment name with the orbs_labels if extra_labels argument is not passed" {
  source ${profile_script}

  local expected_deploymenta='expected-deploymenta'
  local expected_deploymentb='expected-deploymentb'
  function orbs_labels() { echo 'orbs_labels'; }
  function kubectl() { echo "kubectl ${*}" >&2; echo 'expected-deploymenta'; echo 'expected-deploymentb'; }
  
  run orbs_deployment_names
  assert_success
  assert_output -p "kubectl get deployments -l orbs_labels -o=name"
  assert_output -p "${expected_deploymenta}"
  assert_output -p "${expected_deploymentb}"
}

@test "$(basename ${BATS_TEST_FILENAME})#orbs_deployment_names returns every deployment name with the orbs_labels pluse extra_labels if passed" {
  source ${profile_script}

  local expected_deploymenta='expected-deploymenta'
  local expected_deploymentb='expected-deploymentb'
  local extra_labels='oa_long_lived_token_restart=true'
  function orbs_labels() { echo 'orbs_labels'; }
  function kubectl() { echo "kubectl ${*}" >&2; echo 'expected-deploymenta'; echo 'expected-deploymentb'; }
  
  run orbs_deployment_names "${extra_labels}"
  assert_success
  assert_output -p "kubectl get deployments -l orbs_labels,${extra_labels} -o=name"
  assert_output -p "${expected_deploymenta}"
  assert_output -p "${expected_deploymentb}"
}

@test "$(basename ${BATS_TEST_FILENAME})#restart_orbs_deployments fails if orbs_deployment_names returns empty" {
  source ${profile_script}

  function orbs_deployment_names() { return; }
  function kubectl() { echo "kubectl called" >&2; }

  run restart_orbs_deployments
  assert_failure
  refute_output -p "kubectl called"
}

@test "$(basename ${BATS_TEST_FILENAME})#restart_orbs_deployments fails if kubectl rollout restart fails" {
  source ${profile_script}

  function orbs_deployment_names() { echo "expected_deployment_name"; }
  function kubectl() { echo "kubectl ${*}" >&2; return 1; }

  run restart_orbs_deployments
  assert_failure
  assert_output -p "kubectl rollout restart expected_deployment_name"
  refute_output -p "kubectl rollout status expected_deployment_name"
}

@test "$(basename ${BATS_TEST_FILENAME})#restart_orbs_deployments fails if kubectl rollout status fails" {
  source ${profile_script}

  function orbs_deployment_names() { echo "expected_deployment_name"; }
  function kubectl() { echo "kubectl $1 $2 $3" >&2; [ "$2" == "restart" ]; }

  run restart_orbs_deployments
  assert_failure
  assert_output -p "kubectl rollout restart expected_deployment_name"
  assert_output -p "kubectl rollout status expected_deployment_name"
}

@test "$(basename ${BATS_TEST_FILENAME})#restart_orbs_deployments passes if kubectl rollout restart and kubectl rollout status succeed" {
  source ${profile_script}

  function orbs_deployment_names() { echo "expected_deployment_name"; }
  function kubectl() { echo "kubectl ${*}" >&2; }

  run restart_orbs_deployments
  assert_success
  assert_output -p "kubectl rollout restart expected_deployment_name"
  assert_output -p "kubectl rollout status expected_deployment_name"
}

@test "$(basename ${BATS_TEST_FILENAME})#restart_orbs_deployments passes extra_labels argument to orbs_deployment_names for further filtering" {
  source ${profile_script}

  local extra_labels='oa_other_label=value'
  function orbs_deployment_names() { echo "orbs_deployment_names ${*}" >&2; echo "expected_deployment_name"; }
  function kubectl() { echo "kubectl ${*}" >&2; }

  run restart_orbs_deployments "${extra_labels}"
  assert_success
  assert_output -p "orbs_deployment_names ${extra_labels}"
  assert_output -p "kubectl rollout restart expected_deployment_name"
  assert_output -p "kubectl rollout status expected_deployment_name"
}

@test "$(basename ${BATS_TEST_FILENAME})#restart_orbs_deployments restarts each deployment returned by orbs_deployment_names" {
  source ${profile_script}

  function orbs_deployment_names() { echo "first_expected_deployment_name"; echo "second_expected_deployment_name"; }
  function kubectl() { echo "kubectl ${*}" >&2; }

  run restart_orbs_deployments
  assert_success
  assert_output -p "kubectl rollout restart first_expected_deployment_name"
  assert_output -p "kubectl rollout status first_expected_deployment_name"
  assert_output -p "kubectl rollout restart second_expected_deployment_name"
  assert_output -p "kubectl rollout status second_expected_deployment_name"
}

