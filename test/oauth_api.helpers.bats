#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/lib/helpers/oauth_api"

setup() {
  export LOG_LEVEL="info"
  export OAUTH_HOST_URL='OAUTH_HOST_URL'
  export BASE_HOST_URI="${OAUTH_HOST_URL}/idm-ws"
  export OAUTH_USER='OAUTH_USER'
  export OAUTH_PASSWORD='OAUTH_PASSWORD'
}

@test "$(basename ${BATS_TEST_FILENAME}) check_oauth_authentication_environment fails if required environment is not present" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }

  run check_oauth_authentication_environment
  assert_failure
  assert_output -p "check_required_environment OAUTH_HOST_URL OAUTH_USER OAUTH_PASSWORD"
}

@test "$(basename ${BATS_TEST_FILENAME}) check_oauth_authentication_environment succeeds if required environment is present" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_oauth_authentication_environment
  assert_success
  assert_output -p "check_required_environment OAUTH_HOST_URL OAUTH_USER OAUTH_PASSWORD"
}

@test "$(basename ${BATS_TEST_FILENAME}) oauth_authentication returns basic auth formatted string" {
  source ${profile_script}

  run oauth_authentication
  assert_output "${OAUTH_USER}:${OAUTH_PASSWORD}"
}

@test "$(basename ${BATS_TEST_FILENAME}) oauth_get uses curl and oauth_authentication to GET a path from the OAUTH_HOST_URL" {
  source ${profile_script}

  function oauth_authentication() { echo 'oauth_authentication'; }
  function curl() { echo "curl ${*}" >&2; }

  expected_path='/path'

  run oauth_get "${expected_path}"
  assert_output "curl --globoff --silent -H Content-Type: application/json -H Accept: application/json -u oauth_authentication ${BASE_HOST_URI}${expected_path}"
}

@test "$(basename ${BATS_TEST_FILENAME}) oauth_put takes data and path and uses oauth_authentication to PUT to OAUTH_HOST_URL path" {
  source ${profile_script}

  function oauth_authentication() { echo 'oauth_authentication'; }
  function curl() { echo "curl ${*}" >&2; }

  expected_data='expected_data'
  expected_path='/path'

  run oauth_put "${expected_data}" "${expected_path}"
  assert_output -p "curl --globoff --silent -X PUT -H Content-Type: application/json -H Accept: application/json -u oauth_authentication -d ${expected_data} ${BASE_HOST_URI}${expected_path}"
}

@test "$(basename ${BATS_TEST_FILENAME}) oauth_post takes data and path and uses oauth_authentication to post to OAUTH_HOST_URL path" {
  source ${profile_script}

  function oauth_authentication() { echo 'oauth_authentication'; }
  function curl() { echo "curl ${*}" >&2; }

  expected_data='expected_data'
  expected_path='/path'

  run oauth_post "${expected_data}" "${expected_path}"
  assert_output "curl --silent -X POST -H Content-Type: application/json -H Accept: application/json -u oauth_authentication -d ${expected_data} ${BASE_HOST_URI}${expected_path}"
}

@test "$(basename ${BATS_TEST_FILENAME}) oauth_delete takes path and DELETES OAUTH_HOST_URL path" {
  source ${profile_script}

  function oauth_authentication() { echo 'oauth_authentication'; }
  function curl() { echo "curl ${*}" >&2; }

  expected_path='/path'

  run oauth_delete "${expected_path}"
  assert_output "curl --globoff --silent -X DELETE -H Content-Type: application/json -H Accept: application/json -u oauth_authentication ${BASE_HOST_URI}${expected_path}"
}

@test "$(basename ${BATS_TEST_FILENAME}) oauth_delete takes path and data, and DELETES OAUTH_HOST_URL path with data" {
  source ${profile_script}

  function oauth_authentication() { echo 'oauth_authentication'; }
  function curl() { echo "curl ${*}" >&2; }

  expected_path='/path'
  expected_data='expected_data'

  run oauth_delete "${expected_path}" "${expected_data}"
  assert_output "curl --globoff --silent -X DELETE -H Content-Type: application/json -H Accept: application/json -u oauth_authentication -d ${expected_data} ${BASE_HOST_URI}${expected_path}"
}

@test "$(basename ${BATS_TEST_FILENAME}) has_error returns nonzero if stdin matches \"error\":true" {
  source ${profile_script}

  ! echo '"error":true' | has_error
}

@test "$(basename ${BATS_TEST_FILENAME}) has_error returns zero if stdin does not match \"error\":true" {
  source ${profile_script}

  echo '"error":false' | has_error
}

@test "$(basename ${BATS_TEST_FILENAME}) get_oauth_client fails if not provided a client_id" {
  source ${profile_script}

  function oauth_get() { echo "oauth_get ${*}" >&2; }

  run get_oauth_client
  assert_failure
  refute_output -e "oauth_get.*"
}

@test "$(basename ${BATS_TEST_FILENAME}) get_oauth_client uses oauth_get to get a client definition" {
  source ${profile_script}

  function oauth_get() { echo "oauth_get ${*}" >&2; }

  expected_client_id='client_id'
  expected_path="/oauthRegistration/findByClientId?clientId=${expected_client_id}"

  run get_oauth_client "${expected_client_id}"
  assert_success
  assert_output "oauth_get ${expected_path}"
}

@test "$(basename ${BATS_TEST_FILENAME}) add_oauth_redirect_uri fails if not provided a client_id" {
  source ${profile_script}

  function oauth_put() { echo "oauth_put ${*}" >&2; }
  run add_oauth_redirect_uri
  assert_failure
  refute_output -e "oauth_put.*"
}

@test "$(basename ${BATS_TEST_FILENAME}) add_oauth_redirect_uri fails if not given redirect_uris" {
  source ${profile_script}

  function oauth_put() { echo "oauth_put ${*}" >&2; }
  expected_client_id='client_id'

  run add_oauth_redirect_uri "${expected_client_id}"
  assert_failure
  refute_output -e "oauth_put.*"
}

@test "$(basename ${BATS_TEST_FILENAME}) add_oauth_redirect_uri fails if oauth_put has_error" {
  source ${profile_script}

  function oauth_put() { echo "oauth_put ${*}" >&2; }
  function has_error() { return 1; }

  expected_client_id='client_id'
  expected_uris="uri1"
  expected_data='{"redirectUris":["'${expected_uris}'"]}'

  run add_oauth_redirect_uri "${expected_client_id}" "${expected_uris}"
  assert_failure
  assert_output -p "oauth_put ${expected_data} /oauthRegistration/addRedirectURIsByClientId?clientId=${expected_client_id}"
}

@test "$(basename ${BATS_TEST_FILENAME}) add_oauth_redirect_uri uses oauth_put to store a redirect_uri" {
  source ${profile_script}

  function oauth_put() { echo "oauth_put ${*}" >&2; }
  function has_error() { return; }

  expected_client_id='client_id'
  expected_uris="uri1"
  expected_data='{"redirectUris":["'${expected_uris}'"]}'

  run add_oauth_redirect_uri "${expected_client_id}" "${expected_uris}"
  assert_success
  assert_output -p "oauth_put ${expected_data} /oauthRegistration/addRedirectURIsByClientId?clientId=${expected_client_id}"
}

@test "$(basename ${BATS_TEST_FILENAME}) add_oauth_redirect_uri uses oauth_put to store multiple comma separated redirect_uris" {
  source ${profile_script}

  function oauth_put() { echo "oauth_put ${*}" >&2; }
  function has_error() { return; }

  expected_client_id='client_id'
  expected_uris="uri1,uri2"
  expected_data='{"redirectUris":["uri1","uri2"]}'

  run add_oauth_redirect_uri "${expected_client_id}" "${expected_uris}"
  assert_success
  assert_output -p "oauth_put ${expected_data} /oauthRegistration/addRedirectURIsByClientId?clientId=${expected_client_id}"
}

@test "$(basename ${BATS_TEST_FILENAME}) remove_oauth_redirect_uri fails if client_id is not provided" {
  source ${profile_script}

  function has_error() { return; }
  function oauth_delete() { echo "oauth_delete ${*}" >&2; }

  run remove_oauth_redirect_uri
  assert_failure
  refute_output -e "oauth_delete.*"
}

@test "$(basename ${BATS_TEST_FILENAME}) remove_oauth_redirect_uri fails if delete_uri is not provided" {
  source ${profile_script}

  function has_error() { return; }
  function oauth_delete() { echo "oauth_delete ${*}" >&2; }
  expected_client_id='client_id'

  run remove_oauth_redirect_uri "${expected_client_id}"
  assert_failure
  refute_output -e "oauth_delete.*"
}

@test "$(basename ${BATS_TEST_FILENAME}) remove_oauth_redirect_uri fails if oauth_delete has_error" {
  source ${profile_script}

  function has_error() { return 1; }
  function oauth_delete() { echo "oauth_delete ${*}" >&2; }
  expected_client_id='client_id'
  expected_uris="uri1"
  expected_data='{"redirectUris":["'${expected_uris}'"]}'

  run remove_oauth_redirect_uri "${expected_client_id}" "${expected_uris}"
  assert_failure
  assert_output -p "oauth_delete /oauthRegistration/removeRedirectURIsByClientId?clientId=${expected_client_id} ${expected_data}"
}

@test "$(basename ${BATS_TEST_FILENAME}) remove_oauth_redirect_uri deletes the uri for the client" {
  source ${profile_script}

  function has_error() { return; }
  function oauth_delete() { echo "oauth_delete ${*}" >&2; }
  expected_client_id='client_id'
  expected_uris="uri1"
  expected_data='{"redirectUris":["'${expected_uris}'"]}'

  run remove_oauth_redirect_uri "${expected_client_id}" "${expected_uris}"
  assert_success
  assert_output -p "oauth_delete /oauthRegistration/removeRedirectURIsByClientId?clientId=${expected_client_id} ${expected_data}"
}

@test "$(basename ${BATS_TEST_FILENAME}) remove_oauth_redirect_uri uses oauth_put to store multiple comma separated redirect_uris" {
  source ${profile_script}

  function oauth_delete() { echo "oauth_delete ${*}" >&2; }
  function has_error() { return; }

  expected_client_id='client_id'
  expected_uris="uri1,uri2"
  expected_data='{"redirectUris":["uri1","uri2"]}'

  run remove_oauth_redirect_uri "${expected_client_id}" "${expected_uris}"
  assert_success
  assert_output -p "oauth_delete /oauthRegistration/removeRedirectURIsByClientId?clientId=${expected_client_id} ${expected_data}"
}

@test "$(basename ${BATS_TEST_FILENAME}) update_oauth_client fails if not provided a client_id" {
  source ${profile_script}

  function oauth_put() { echo "oauth_put ${*}" >&2; }

  run update_oauth_client
  assert_failure
  refute_output -e "oauth_put.*"
}

@test "$(basename ${BATS_TEST_FILENAME}) update_oauth_client fails if not provided data" {
  source ${profile_script}

  function oauth_put() { echo "oauth_put ${*}" >&2; }
  expected_client_id='client_id'

  run update_oauth_client "${expected_client_id}"
  assert_failure
  refute_output -e "oauth_put.*"
}

@test "$(basename ${BATS_TEST_FILENAME}) update_oauth_client fails if oauth_put has_error" {
  source ${profile_script}

  function oauth_put() { echo "oauth_put ${*}" >&2; }
  function has_error() { return 1; }

  expected_client_id='client_id'
  expected_data='data'

  run update_oauth_client "${expected_client_id}" "${expected_data}"
  assert_failure
  assert_output -p "oauth_put ${expected_data} /oauthRegistration/updateByClientId?clientId=${expected_client_id}"
}

@test "$(basename ${BATS_TEST_FILENAME}) update_oauth_client uses oauth_put to update the client with the data" {
  source ${profile_script}

  function oauth_put() { echo "oauth_put ${*}" >&2; }
  function has_error() { return; }

  expected_client_id='client_id'
  expected_data='data'

  run update_oauth_client "${expected_client_id}" "${expected_data}"
  assert_success
  assert_output -p "oauth_put ${expected_data} /oauthRegistration/updateByClientId?clientId=${expected_client_id}"
}