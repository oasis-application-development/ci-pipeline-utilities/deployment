#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/lib/helpers/project_api"

setup() {
  export LOG_LEVEL="info"
  export CI_API_V4_URL="CI_API_V4_URL"
  export CI_PROJECT_ID="CI_PROJECT_ID"
}

@test "$(basename ${BATS_TEST_FILENAME}) private_token returns value of PRIVATE_TOKEN" {
  source ${profile_script}

  export PRIVATE_TOKEN="PRIVATE_TOKEN"

  run private_token
  assert_output "${PRIVATE_TOKEN}"
}

@test "$(basename ${BATS_TEST_FILENAME}) private_token returns value of BOT_KEY" {
  source ${profile_script}

  export BOT_KEY="BOT_KEY"

  run private_token
  assert_output "${BOT_KEY}"
}

@test "$(basename ${BATS_TEST_FILENAME}) project_get takes path and makes private token authenticated gitlab project api get for path" {
  source ${profile_script}

  local expected_private_token='PT'
  local expected_path="/path"

  function private_token() { echo 'private_token called' >&2; echo "PT"; }
  function curl() { echo "curl ${*}" >&2; }

  run project_get ${expected_path}
  assert_output -p 'private_token called'
  assert_output -p "curl --globoff --silent -H Content-Type: application/json -H Accept: application/json -H Private-Token: ${expected_private_token} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}${expected_path}"
}

@test "$(basename ${BATS_TEST_FILENAME}) project_put takes data and path and makes private token authenticated gitlab project api put to path" {
  source ${profile_script}
  local expected_private_token='PT'
  local expected_data="{\"key\":\"value\"}"
  local expected_path="/path"

  function private_token() { echo 'private_token called' >&2; echo "PT"; }
  function curl() { echo "curl ${*}" >&2; }

  run project_put "${expected_data}" "${expected_path}"
  assert_output -p "curl --globoff --silent -X PUT -H Content-Type: application/json -H Accept: application/json -H Private-Token: ${expected_private_token} -d ${expected_data} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}${expected_path}"
}

@test "$(basename ${BATS_TEST_FILENAME}) project_post takes data and path and makes private token authenticated gitlab project api post to path" {
  source ${profile_script}
  local expected_private_token='PT'
  local expected_data="{\"key\":\"value\"}"
  local expected_path="/path"

  function private_token() { echo 'private_token called' >&2; echo "PT"; }
  function curl() { echo "curl ${*}" >&2; }

  run project_post "${expected_data}" "${expected_path}"
  assert_output -p "curl --silent -X POST -H Content-Type: application/json -H Accept: application/json -H Private-Token: ${expected_private_token} -d ${expected_data} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}${expected_path}"
}

@test "$(basename ${BATS_TEST_FILENAME}) project_delete takes path and makes private token authenticated gitlab project api delete of the path" {
  source ${profile_script}
  local expected_private_token='PT'
  local expected_path="/path"

  function private_token() { echo 'private_token called' >&2; echo "PT"; }
  function curl() { echo "curl ${*}" >&2; }

  run project_delete ${expected_path}
  assert_output -p "curl --globoff --silent -X DELETE -H Content-Type: application/json -H Accept: application/json -H Private-Token: ${expected_private_token} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}${expected_path}"
}

