#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

expected_required_environment="CI_COMMIT_SHA CHART_MUSEUM_URL"
expected_default_environment="HELM_CHART_NAME:CI_PROJECT_NAME"
profile_script="/usr/local/bin/publish_chart"

setup() {
  export LOG_LEVEL="info"
  export CI_PROJECT_NAME="CI_PROJECT_NAME"
  export CI_COMMIT_SHA="CI_COMMIT_SHA"
  export CHART_MUSEUM_URL="CHART_MUSEUM_URL"
}

# it is not possible to override the command function
@test "$(basename ${BATS_TEST_FILENAME})#requirements_installed tests for helm-push plugin support" {
  source ${profile_script}

  function helm() { echo "helm ${*} called" >&2; echo "push"; }

  run requirements_installed
  assert_success
  assert_output -p "helm plugin list called"
  refute_output -p "helm-push plugin not installed"
}

@test "$(basename ${BATS_TEST_FILENAME})#requirements_installed fails if helm-push plugin support is not found" {
  source ${profile_script}

  function helm() { echo "helm ${*} called" >&2; }

  run requirements_installed
  assert_failure
  assert_output -p "helm plugin list called"
  assert_output -p "helm-push plugin not installed"
}

@test "$(basename ${BATS_TEST_FILENAME})#add_chart_museum prints command and returns if dry_run is true" {
  source ${profile_script}

  function dry_run() { echo "dry_run called" >&2; }
  function helm() { echo "helm ${*} called" >&2; }

  assert dry_run
  run add_chart_museum
  assert_success
  assert_output -p "helm repo add chartmuseum ${CHART_MUSEUM_URL}"
  refute_output -p "helm repo add chartmuseum ${CHART_MUSEUM_URL} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#add_chart_museum adds the CHART_MUSEUM_URL to helm charts if dry_run is false" {
  source ${profile_script}

  function helm() { echo "helm ${*} called" >&2; }

  refute dry_run
  run add_chart_museum
  assert_success
  assert_output -p "helm repo add chartmuseum ${CHART_MUSEUM_URL} called"
}

@test "$(basename ${BATS_TEST_FILENAME})#add_chart_museum fails if CHART_MUSEUM_URL helm chart cannot be added" {
  source ${profile_script}

  function helm() { echo "helm ${*} called" >&2; return 1; }

  refute dry_run
  run add_chart_museum
  assert_failure
  assert_output -p "helm repo add chartmuseum ${CHART_MUSEUM_URL} called"
  assert_output -p "Could not add chart museum ${CHART_MUSEUM_URL}"
}

@test "$(basename ${BATS_TEST_FILENAME})#publish_chart prints commands if dry_run is true" {
  source ${profile_script}

  function dry_run() { echo "dry_run called" >&2; }
  function add_chart_museum() { echo "add_chart_museum called" >&2; }
  function helm_chart_name() { echo "helm_chart_name called" >&2; echo "${CI_PROJECT_NAME}"; }
  function helm() { echo "helm ${*} called" >&2; }

  assert dry_run
  run publish_chart
  assert_success
  assert_output -p "add_chart_museum called"
  assert_output -p "helm_chart_name called"
  assert_output -p "helm cm-push helm-chart/${CI_PROJECT_NAME} chartmuseum"
  refute_output -p "helm cm-push helm-chart/${CI_PROJECT_NAME} chartmuseum called"
}

@test "$(basename ${BATS_TEST_FILENAME})#publish_chart calls add_chart_museum and pushes the chart" {
  source ${profile_script}

  function add_chart_museum() { echo "add_chart_museum called" >&2; }
  function helm_chart_name() { echo "helm_chart_name called" >&2; echo "${CI_PROJECT_NAME}"; }
  function helm() { echo "helm ${*} called" >&2; }

  refute dry_run
  run publish_chart
  assert_success
  assert_output -p "add_chart_museum called"
  assert_output -p "helm_chart_name called"
  assert_output -p "helm cm-push helm-chart/${CI_PROJECT_NAME} chartmuseum called"
}

@test "$(basename ${BATS_TEST_FILENAME})#publish_chart fails if add_chart_museum fails" {
  source ${profile_script}

  function add_chart_museum() { echo "add_chart_museum called" >&2; return 1; }
  function helm_chart_name() { echo "helm_chart_name called" >&2; echo "${CI_PROJECT_NAME}"; }
  function helm() { echo "helm ${*} called" >&2; }

  refute dry_run
  run publish_chart
  assert_failure
  assert_output -p "add_chart_museum called"
  assert_output -p "helm_chart_name called"
  refute_output -p "helm cm-push helm-chart/${CI_PROJECT_NAME} chartmuseum called"
}

@test "$(basename ${BATS_TEST_FILENAME})#publish_chart fails if helm cm-push fails" {
  source ${profile_script}

  function add_chart_museum() { echo "add_chart_museum called" >&2; }
  function helm_chart_name() { echo "helm_chart_name called" >&2; echo "${CI_PROJECT_NAME}"; }
  function helm() { echo "helm ${*} called" >&2; return 1; }

  refute dry_run
  run publish_chart
  assert_failure
  assert_output -p "add_chart_museum called"
  assert_output -p "helm_chart_name called"
  assert_output -p "helm cm-push helm-chart/${CI_PROJECT_NAME} chartmuseum called"
  assert_output -p "Could not publish chart!"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main runs check_helm_chart_requirements, check_required_environment, requirements_installed and publish_chart" {
  source ${profile_script}

  function check_helm_chart_requirements() { echo "check_helm_chart_requirements called"; }
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  function requirements_installed() { echo "requirements_installed called" >&2; }
  function publish_chart() { echo "publish_chart called" >&2; }

  run run_main
  assert_success
  assert_output -p "check_helm_chart_requirements called"
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p "requirements_installed called"
  assert_output -p "publish_chart called"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if check_helm_chart_requirements fails" {
  source ${profile_script}

  function check_helm_chart_requirements() { echo "check_helm_chart_requirements called"; return 1; }
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  function requirements_installed() { echo "requirements_installed called" >&2; }
  function publish_chart() { echo "publish_chart called" >&2; }

  run run_main
  assert_failure
  assert_output -p "check_helm_chart_requirements called"
  refute_output -p "check_required_environment ${expected_required_environment} called"
  refute_output -p "requirements_installed called"
  refute_output -p "publish_chart called"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if check_required_environment fails" {
  source ${profile_script}

  function check_helm_chart_requirements() { echo "check_helm_chart_requirements called"; }
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; return 1; }
  function requirements_installed() { echo "requirements_installed called" >&2; }
  function publish_chart() { echo "publish_chart called" >&2; }

  run run_main
  assert_failure
  assert_output -p "check_helm_chart_requirements called"
  assert_output -p "check_required_environment ${expected_required_environment} called"
  refute_output -p "requirements_installed called"
  refute_output -p "publish_chart called"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if requirements_installed fails" {
  source ${profile_script}

  function check_helm_chart_requirements() { echo "check_helm_chart_requirements called"; }
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  function requirements_installed() { echo "requirements_installed called" >&2; return 1; }
  function publish_chart() { echo "publish_chart called" >&2; }

  run run_main
  assert_failure
  assert_output -p "check_helm_chart_requirements called"
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p "requirements_installed called"
  refute_output -p "publish_chart called"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if publish_chart fails" {
  source ${profile_script}

  function check_helm_chart_requirements() { echo "check_helm_chart_requirements called"; }
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  function requirements_installed() { echo "requirements_installed called" >&2; }
  function publish_chart() { echo "publish_chart called" >&2; return 1; }

  run run_main
  assert_failure
  assert_output -p "check_helm_chart_requirements called"
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p "requirements_installed called"
  assert_output -p "publish_chart called"
}
