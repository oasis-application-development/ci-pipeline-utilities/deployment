#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/bin/shibboleth_service_proxy"

setup() {
    export CI_PROJECT_DIR='/tmp'
    export CHART_MUSEUM_URL="http://chartmuseum.local"
    export ORG="ORG"
    export ORGUNIT="ORGUNIT"
    export EMAIL="EMAIL"
}

teardown() {
    if [ -f ${CI_PROJECT_DIR}/proxy.conf.tmpl ]
    then
        rm ${CI_PROJECT_DIR}/proxy.conf.tmpl
    fi

    if [ -f ${CI_PROJECT_DIR}/attribute-map.xml ]
    then
      rm ${CI_PROJECT_DIR}/attribute-map.xml
    fi
}

@test "$(basename ${BATS_TEST_FILENAME}) install_chartmuseum installs ${CHART_MUSEUM_URL} to the local helm" {
    source ${profile_script}
    local expected_chart_name='chartmuseum'
    function helm() { echo "helm ${*} called" >&2; }
    run install_chartmuseum
    assert_success
    assert_output -p "helm repo add ${expected_chart_name} ${CHART_MUSEUM_URL} called"
}

@test "$(basename ${BATS_TEST_FILENAME}) protected_services without argument queries for all services with the expected annotations and returns a jq list" {
    source ${profile_script}
    function kubectl() { echo "kubectl ${*} called" >&2; echo ""; }
    function jq() { echo "jq ${*} called" >&2; }
    run protected_services
    assert_success
    assert_output -p "kubectl get services -l duke.shibboleth.proxy.edu/service_should_be_protected=true -o json called"
    assert_output -p "jq -r .items | map(.metadata.name+\":\"+.metadata.labels[\"duke.shibboleth.proxy.edu/service_port\"]+\"@\"+.metadata.labels[\"duke.shibboleth.proxy.edu/service_entity_id\"])[] called"
}

@test "$(basename ${BATS_TEST_FILENAME}) protected_services with an argument queries for all services with the expected annotations and returns a jq list" {
    source ${profile_script}
    expected_entity_id='protected_entity_id'
    function kubectl() { echo "kubectl ${*} called" >&2; echo ""; }
    function jq() { echo "jq ${*} called" >&2; }
    run protected_services "${expected_entity_id}"
    assert_success
    assert_output -p "kubectl get services -l duke.shibboleth.proxy.edu/service_should_be_protected=true,duke.shibboleth.proxy.edu/service_entity_id=${expected_entity_id} -o json called"
    assert_output -p "jq -r .items | map(.metadata.name+\":\"+.metadata.labels[\"duke.shibboleth.proxy.edu/service_port\"]+\"@\"+.metadata.labels[\"duke.shibboleth.proxy.edu/service_entity_id\"])[] called"
}

@test "$(basename ${BATS_TEST_FILENAME}) is_openshift returns true if working against openshift" {
    source ${profile_script}
    function kubectl() { echo "kubectl ${*} called" >&2; echo "route.openshift.io"; }
    assert is_openshift
}

@test "$(basename ${BATS_TEST_FILENAME}) is_openshift returns false if not working against openshift" {
    source ${profile_script}
    function kubectl() { echo "kubectl ${*} called" >&2; echo "v1"; }
    refute is_openshift
}

@test "$(basename ${BATS_TEST_FILENAME}) protected_routes queries for routes with the expected annotations as json if working against openshift" {
    source ${profile_script}
    function is_openshift() { echo "is_openshift called" >&2; }
    function kubectl() { echo "kubectl ${*} called" >&2; echo ""; }
    run protected_routes
    assert_success
    assert_output -p "is_openshift called"
    assert_output -p "kubectl get route -l duke.shibboleth.proxy.edu/protected_proxy=true -o json called"
}

@test "$(basename ${BATS_TEST_FILENAME}) protected_routes queries for ingresses with the expected annotations as json if not working against openshift" {
    source ${profile_script}
    function is_openshift() { echo "is_openshift called" >&2 ; return 1; }
    function kubectl() { echo "kubectl ${*} called" >&2; echo ""; }
    run protected_routes
    assert_success
    assert_output -p "is_openshift called"
    assert_output -p "kubectl get ingress -l duke.shibboleth.proxy.edu/protected_proxy=true -o json called"
}

@test "$(basename ${BATS_TEST_FILENAME}) service_protected returns 4 if service_definition is not provided" {
    source ${profile_script}
    run service_protected
    assert [ "$status" -eq 4 ]
}

@test "$(basename ${BATS_TEST_FILENAME}) service_protected returns 1 if no protected route exists for the service" {
    source ${profile_script}
    local expected_service_name='service'
    local service_definition="${expected_service_name}:port@entity"
    function protected_routes() { echo "protected_routes called" >&2; }
    function jq() { echo "jq ${*} called" >&2; echo 0; }
    run service_protected "${service_definition}"
    assert_failure
    assert_output -p "protected_routes called"
    assert_output -p 'jq -r .items | map(select(.metadata.name == "'${expected_service_name}'-proxy-shib-proxy")) | length called'
}

@test "$(basename ${BATS_TEST_FILENAME}) service_protected returns 0 if a protected route already exists for the service" {
    source ${profile_script}
    local expected_service_name='service'
    local service_definition="${expected_service_name}:port@entity"
    function protected_routes() { echo "protected_routes called" >&2; }
    function jq() { echo "jq ${*} called" >&2; echo 1; }
    run service_protected "${service_definition}"
    assert_success
    assert_output -p "protected_routes called"
    assert_output -p 'jq -r .items | map(select(.metadata.name == "'${expected_service_name}'-proxy-shib-proxy")) | length called'
}

@test "$(basename ${BATS_TEST_FILENAME}) create_shib_certificate returns 4 if service_definition is not provided" {
    source ${profile_script}
    run create_shib_certificate
    assert [ "$status" -eq 4 ]
}

@test "$(basename ${BATS_TEST_FILENAME}) create_shib_certificate returns 1 if it cannot make the shib_certificate directory" {
    source ${profile_script}
    local service_definition="service:port@entity"
    function mkdir() { echo "mkdir ${*} called" >&2; return 1; }
    run create_shib_certificate "${service_definition}"
    assert_failure
    assert_output -p "mkdir -p ${CI_PROJECT_DIR}/shib_certificate called"    
}

@test "$(basename ${BATS_TEST_FILENAME}) deploy_shibboleth_proxy returns 4 if service_definition is not provided" {
    source ${profile_script}
    run deploy_shibboleth_proxy
    assert [ "$status" -eq 4 ]
}

@test "$(basename ${BATS_TEST_FILENAME}) deploy_shibboleth_proxy returns 1 if proxy.conf.tmpl is not present in CI_PROJECT_DIR" {
    source ${profile_script}
    local service_definition="service:port@entity"
    run deploy_shibboleth_proxy "${service_definition}"
    assert_failure
    assert_output -p "proxy.conf.tmpl required in project root"
}

@test "$(basename ${BATS_TEST_FILENAME}) deploy_shibboleth_proxy fails if helm upgrade fails" {
    source ${profile_script}
    local expected_service_name='service'
    local expected_service_port='port'
    local expected_entity_id='entity'
    local service_definition="${expected_service_name}:${expected_service_port}@${expected_entity_id}"
    function helm() { echo "helm ${*} called" >&2; return 1; }
    echo '{% service_host %}' > ${CI_PROJECT_DIR}/proxy.conf.tmpl
    echo 'attributes' > ${CI_PROJECT_DIR}/attribute-map.xml
    run deploy_shibboleth_proxy "${service_definition}"
    assert_failure
    refute_output -p "proxy.conf.tmpl required in project root"
    assert_output -p "helm upgrade ${expected_service_name}-proxy chartmuseum/duke-shibboleth --atomic --reset-values --debug --wait --install --set-string entityID=${expected_entity_id} --set-file tls.crt=${CI_PROJECT_DIR}/shib_certificate/${expected_entity_id}.crt --set-file tls.key=${CI_PROJECT_DIR}/shib_certificate/${expected_entity_id}.key --set-file applicationConfiguration=proxy.conf --set-file shibboleth.attributeMap=attribute-map.xml"
}

@test "$(basename ${BATS_TEST_FILENAME}) deploy_shibboleth_proxy creates proxy.conf from proxy.conf.tmpl and passes it with cert and key to helm upgrade of duke-shibboleth chart" {
    source ${profile_script}
    local expected_service_name='service'
    local expected_service_port='port'
    local expected_entity_id='entity'
    local service_definition="${expected_service_name}:${expected_service_port}@${expected_entity_id}"
    function helm() { echo "helm ${*} called" >&2; }
    echo '{% service_host %}' > ${CI_PROJECT_DIR}/proxy.conf.tmpl
    run deploy_shibboleth_proxy "${service_definition}"
    assert_success
    refute_output -p "proxy.conf.tmpl required in project root"
    assert_output -p "helm upgrade ${expected_service_name}-proxy chartmuseum/duke-shibboleth --atomic --reset-values --debug --wait --install --set-string entityID=${expected_entity_id} --set-file tls.crt=${CI_PROJECT_DIR}/shib_certificate/${expected_entity_id}.crt --set-file tls.key=${CI_PROJECT_DIR}/shib_certificate/${expected_entity_id}.key --set-file applicationConfiguration=proxy.conf"
    assert [ -f ${CI_PROJECT_DIR}/proxy.conf ]
    proxy_conf=$(cat ${CI_PROJECT_DIR}/proxy.conf)
    assert_equal "${proxy_conf}" "${expected_service_name}:${expected_service_port}"
}

@test "$(basename ${BATS_TEST_FILENAME}) notify returns 4 if service_definition is not provided" {
    source ${profile_script}
    run notify
    assert [ "$status" -eq 4 ]
}

@test "$(basename ${BATS_TEST_FILENAME}) notify prints helpful info on service registration and generated cert,key" {
    source ${profile_script}
    local expected_service_name='service'
    local expected_service_port='port'
    local expected_entity_id='entity'
    local service_definition="${expected_service_name}:${expected_service_port}@${expected_entity_id}"
    run notify "${service_definition}"
    assert_output "Service ${expected_service_name}:${expected_service_port} is now protected. Create an entity at https://authentication.oit.duke.edu/manager/register/sp with the following:
       entity_id: https://${expected_entity_id}
       certificate: ${expected_entity_id}.crt
    "
}

@test "$(basename ${BATS_TEST_FILENAME}) setup runs check_required_cluster_login_environment, cluster_login, init_helm_with_tiller, and install_chartmuseum" {
    source ${profile_script}

    function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }
    function cluster_login() { echo "cluster_login called" >&2; }

    run setup
    assert_success
    assert_output -p "check_required_cluster_login_environment called"
    assert_output -p "cluster_login called"
}

@test "$(basename ${BATS_TEST_FILENAME}) setup fails if check_required_cluster_login_environment fails" {
    source ${profile_script}

    function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; return 1; }
    function cluster_login() { echo "cluster_login called" >&2; }

    run setup
    assert_failure
    assert_output -p "check_required_cluster_login_environment called"
    refute_output -p "cluster_login called"
}

@test "$(basename ${BATS_TEST_FILENAME}) setup fails if cluster_login fails" {
    source ${profile_script}

    function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }
    function cluster_login() { echo "cluster_login called" >&2; return 1; }

    run setup
    assert_failure
    assert_output -p "check_required_cluster_login_environment called"
    assert_output -p "cluster_login called"
}

@test "$(basename ${BATS_TEST_FILENAME}) main runs check_required_environment, setup, installs chart_museum, gets the protected_services, and with each protected_service verifies that it needs to be protected, generates a certificate, deploys the proxy, and notifies" {
    source ${profile_script}

    local expected_service1="foo"
    local expected_service1_port='1000'
    local expected_service1_entity_id='foo.com'
    local expected_service2="bar"
    local expected_service2_port='1000'
    local expected_service2_entity_id='bar.com'
    local expected_protected_services="${expected_service1}:${expected_service1_port}@${expected_service1_entity_id} ${expected_service2}:${expected_service2_port}@${expected_service2_entity_id}"
    local expected_environment_variables="CI_PROJECT_DIR ORG ORGUNIT EMAIL CHART_MUSEUM_URL"

    function deployment_image_version_info() { echo "deployment_image_version_info called" >&2; }
    function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
    function setup() { echo "setup called" >&2; }
    function install_chartmuseum() { echo "install_chartmuseum called" >&2; }
    function protected_services() { echo "protected_services called" >&2; echo "foo:1000@foo.com bar:1000@bar.com"; }
    function service_protected() { echo "service_protected ${*} called" >&2; return 1; }
    function create_shib_certificate() { echo "create_shib_certificate ${*} called" >&2; }
    function deploy_shibboleth_proxy() { echo "deploy_shibboleth_proxy ${*} called" >&2; }
    function notify() { echo "notify ${*} called" >&2; }

    run main
    assert_success
    assert_output -p "deployment_image_version_info called"
    assert_output -p "check_required_environment ${expected_environment_variables} called"
    assert_output -p "setup called"
    assert_output -p "install_chartmuseum called"
    assert_output -p "protected_services called"
    
    for expected_protected_service in ${expected_protected_services}
    do
        assert_output -p "service_protected ${expected_protected_service} called" 
        assert_output -p "create_shib_certificate ${expected_protected_service} called" 
        assert_output -p "deploy_shibboleth_proxy ${expected_protected_service} called" 
        assert_output -p "notify ${expected_protected_service} called" 
    done
}

@test "$(basename ${BATS_TEST_FILENAME}) main fails if check_required_environment fails" {
    source ${profile_script}

    local expected_service1="foo"
    local expected_service1_port='1000'
    local expected_service1_entity_id='foo.com'
    local expected_service2="bar"
    local expected_service2_port='1000'
    local expected_service2_entity_id='bar.com'
    local expected_protected_services="${expected_service1}:${expected_service1_port}@${expected_service1_entity_id} ${expected_service2}:${expected_service2_port}@${expected_service2_entity_id}"
    local expected_environment_variables="CI_PROJECT_DIR ORG ORGUNIT EMAIL CHART_MUSEUM_URL"

    function check_required_environment() { echo "check_required_environment ${*} called" >&2; return 1; }
    function setup() { echo "setup called" >&2; }
    function install_chartmuseum() { echo "install_chartmuseum called" >&2; }
    function protected_services() { echo "protected_services called" >&2; echo "foo:1000@foo.com bar:1000@bar.com"; }
    function service_protected() { echo "service_protected ${*} called" >&2; return 1; }
    function create_shib_certificate() { echo "create_shib_certificate ${*} called" >&2; }
    function deploy_shibboleth_proxy() { echo "deploy_shibboleth_proxy ${*} called" >&2; }
    function notify() { echo "notify ${*} called" >&2; }

    run main
    assert_failure
    assert_output -p "check_required_environment ${expected_environment_variables} called"
    refute_output -p "setup called"
    refute_output -p "install_chartmuseum called"
    refute_output -p "protected_services called"
    
    for expected_protected_service in ${expected_protected_services}
    do
        refute_output -p "service_protected ${expected_protected_service} called" 
        refute_output -p "create_shib_certificate ${expected_protected_service} called" 
        refute_output -p "deploy_shibboleth_proxy ${expected_protected_service} called" 
        refute_output -p "notify ${expected_protected_service} called" 
    done
}

@test "$(basename ${BATS_TEST_FILENAME}) main fails if setup fails" {
    source ${profile_script}

    local expected_service1="foo"
    local expected_service1_port='1000'
    local expected_service1_entity_id='foo.com'
    local expected_service2="bar"
    local expected_service2_port='1000'
    local expected_service2_entity_id='bar.com'
    local expected_protected_services="${expected_service1}:${expected_service1_port}@${expected_service1_entity_id} ${expected_service2}:${expected_service2_port}@${expected_service2_entity_id}"
    local expected_environment_variables="CI_PROJECT_DIR ORG ORGUNIT EMAIL CHART_MUSEUM_URL"

    function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
    function setup() { echo "setup called" >&2; return 1; }
    function install_chartmuseum() { echo "install_chartmuseum called" >&2; }   
    function protected_services() { echo "protected_services called" >&2; echo "foo:1000@foo.com bar:1000@bar.com"; }
    function service_protected() { echo "service_protected ${*} called" >&2; return 1; }
    function create_shib_certificate() { echo "create_shib_certificate ${*} called" >&2; }
    function deploy_shibboleth_proxy() { echo "deploy_shibboleth_proxy ${*} called" >&2; }
    function notify() { echo "notify ${*} called" >&2; }

    run main
    assert_failure
    assert_output -p "check_required_environment ${expected_environment_variables} called"
    assert_output -p "setup called"
    refute_output -p "install_chartmuseum called"
    refute_output -p "protected_services called"
    
    for expected_protected_service in ${expected_protected_services}
    do
        refute_output -p "service_protected ${expected_protected_service} called" 
        refute_output -p "create_shib_certificate ${expected_protected_service} called" 
        refute_output -p "deploy_shibboleth_proxy ${expected_protected_service} called" 
        refute_output -p "notify ${expected_protected_service} called" 
    done
}

@test "$(basename ${BATS_TEST_FILENAME}) main fails if install_chartmuseum fails" {
    source ${profile_script}

    local expected_service1="foo"
    local expected_service1_port='1000'
    local expected_service1_entity_id='foo.com'
    local expected_service2="bar"
    local expected_service2_port='1000'
    local expected_service2_entity_id='bar.com'
    local expected_protected_services="${expected_service1}:${expected_service1_port}@${expected_service1_entity_id} ${expected_service2}:${expected_service2_port}@${expected_service2_entity_id}"
    local expected_environment_variables="CI_PROJECT_DIR ORG ORGUNIT EMAIL CHART_MUSEUM_URL"

    function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
    function setup() { echo "setup called" >&2; }
    function install_chartmuseum() { echo "install_chartmuseum called" >&2; return 1; }
    function protected_services() { echo "protected_services called" >&2; echo "foo:1000@foo.com bar:1000@bar.com"; }
    function service_protected() { echo "service_protected ${*} called" >&2; return 1; }
    function create_shib_certificate() { echo "create_shib_certificate ${*} called" >&2; }
    function deploy_shibboleth_proxy() { echo "deploy_shibboleth_proxy ${*} called" >&2; }
    function notify() { echo "notify ${*} called" >&2; }

    run main
    assert_failure
    assert_output -p "check_required_environment ${expected_environment_variables} called"
    assert_output -p "setup called"
    assert_output -p "install_chartmuseum called"
    refute_output -p "protected_services called"
    
    for expected_protected_service in ${expected_protected_services}
    do
        refute_output -p "service_protected ${expected_protected_service} called" 
        refute_output -p "create_shib_certificate ${expected_protected_service} called" 
        refute_output -p "deploy_shibboleth_proxy ${expected_protected_service} called" 
        refute_output -p "notify ${expected_protected_service} called" 
    done
}

@test "$(basename ${BATS_TEST_FILENAME}) main fails if service_protected fails 4" {
    source ${profile_script}
    
    local expected_service1="foo"
    local expected_service1_port='1000'
    local expected_service1_entity_id='foo.com'
    local expected_service2="bar"
    local expected_service2_port='1000'
    local expected_service2_entity_id='bar.com'
    local first_service="${expected_service1}:${expected_service1_port}@${expected_service1_entity_id}"
    local second_service="${expected_service2}:${expected_service2_port}@${expected_service2_entity_id}"
    local expected_protected_services="${first_service} ${second_service}"
    local expected_environment_variables="CI_PROJECT_DIR ORG ORGUNIT EMAIL CHART_MUSEUM_URL"

    function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
    function setup() { echo "setup called" >&2; }
    function install_chartmuseum() { echo "install_chartmuseum called" >&2; }
    function protected_services() { echo "protected_services called" >&2; echo "foo:1000@foo.com bar:1000@bar.com"; }
    function service_protected() { echo "service_protected ${*} called" >&2; return 4; }
    function create_shib_certificate() { echo "create_shib_certificate ${*} called" >&2; }
    function deploy_shibboleth_proxy() { echo "deploy_shibboleth_proxy ${*} called" >&2; }
    function notify() { echo "notify ${*} called" >&2; }

    run main
    assert_failure
    assert_output -p "check_required_environment ${expected_environment_variables} called"
    assert_output -p "setup called"
    assert_output -p "install_chartmuseum called"
    assert_output -p "protected_services called"
    
    assert_output -p "service_protected ${first_service} called"
    refute_output -p "service_protected ${second_service} called"

    for expected_protected_service in ${expected_protected_services}
    do 
        refute_output -p "create_shib_certificate ${expected_protected_service} called" 
        refute_output -p "deploy_shibboleth_proxy ${expected_protected_service} called" 
        refute_output -p "notify ${expected_protected_service} called" 
    done
}

@test "$(basename ${BATS_TEST_FILENAME}) main skips service and does not deploy if service_protected is true" {
    source ${profile_script}

    local expected_service1="foo"
    local expected_service1_port='1000'
    local expected_service1_entity_id='foo.com'
    local expected_service2="bar"
    local expected_service2_port='1000'
    local expected_service2_entity_id='bar.com'
    local protected_service="${expected_service1}:${expected_service1_port}@${expected_service1_entity_id}"
    local unprotected_service="${expected_service2}:${expected_service2_port}@${expected_service2_entity_id}"
    local expected_protected_services="${protected_service} ${unprotected_service}"
    local expected_environment_variables="CI_PROJECT_DIR ORG ORGUNIT EMAIL CHART_MUSEUM_URL"

    function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
    function setup() { echo "setup called" >&2; }
    function install_chartmuseum() { echo "install_chartmuseum called" >&2; }
    function protected_services() { echo "protected_services called" >&2; echo "foo:1000@foo.com bar:1000@bar.com"; }
    function service_protected() { echo "service_protected ${*} called" >&2; [ "${*}" == "foo:1000@foo.com" ]; }
    function create_shib_certificate() { echo "create_shib_certificate ${*} called" >&2; }
    function deploy_shibboleth_proxy() { echo "deploy_shibboleth_proxy ${*} called" >&2; }
    function notify() { echo "notify ${*} called" >&2; }

    run main
    assert_success
    assert_output -p "check_required_environment ${expected_environment_variables} called"
    assert_output -p "setup called"
    assert_output -p "install_chartmuseum called"
    assert_output -p "protected_services called"
    
    assert_output -p "service_protected ${protected_service} called"
    refute_output -p "create_shib_certificate ${protected_service} called" 
    refute_output -p "deploy_shibboleth_proxy ${protected_service} called" 
    refute_output -p "notify ${protected_service} called" 

    assert_output -p "service_protected ${unprotected_service} called"
    assert_output -p "create_shib_certificate ${unprotected_service} called" 
    assert_output -p "deploy_shibboleth_proxy ${unprotected_service} called" 
    assert_output -p "notify ${unprotected_service} called" 
}

@test "$(basename ${BATS_TEST_FILENAME}) main fails if create_shib_certificate fails" {
    source ${profile_script}

    local expected_service1="foo"
    local expected_service1_port='1000'
    local expected_service1_entity_id='foo.com'
    local expected_service2="bar"
    local expected_service2_port='1000'
    local expected_service2_entity_id='bar.com'
    local first_service="${expected_service1}:${expected_service1_port}@${expected_service1_entity_id}"
    local second_service="${expected_service2}:${expected_service2_port}@${expected_service2_entity_id}"
    local expected_protected_services="${first_service} ${second_service}"
    local expected_environment_variables="CI_PROJECT_DIR ORG ORGUNIT EMAIL CHART_MUSEUM_URL"

    function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
    function setup() { echo "setup called" >&2; }
    function install_chartmuseum() { echo "install_chartmuseum called" >&2; }
    function protected_services() { echo "protected_services called" >&2; echo "foo:1000@foo.com bar:1000@bar.com"; }
    function service_protected() { echo "service_protected ${*} called" >&2; return 1; }
    function create_shib_certificate() { echo "create_shib_certificate ${*} called" >&2; return 1; }
    function deploy_shibboleth_proxy() { echo "deploy_shibboleth_proxy ${*} called" >&2; }
    function notify() { echo "notify ${*} called" >&2; }

    run main
    assert_failure
    assert_output -p "check_required_environment ${expected_environment_variables} called"
    assert_output -p "setup called"
    assert_output -p "install_chartmuseum called"
    assert_output -p "protected_services called"
    
    assert_output -p "service_protected ${first_service} called"
    assert_output -p "create_shib_certificate ${first_service} called" 
    refute_output -p "service_protected ${second_service} called"
    refute_output -p "create_shib_certificate ${second_service} called" 

    for expected_protected_service in ${expected_protected_services}
    do 
        refute_output -p "deploy_shibboleth_proxy ${expected_protected_service} called" 
        refute_output -p "notify ${expected_protected_service} called" 
    done
}

@test "$(basename ${BATS_TEST_FILENAME}) main fails if deploy_shibboleth_proxy fails" {
    source ${profile_script}
    
    local expected_service1="foo"
    local expected_service1_port='1000'
    local expected_service1_entity_id='foo.com'
    local expected_service2="bar"
    local expected_service2_port='1000'
    local expected_service2_entity_id='bar.com'
    local first_service="${expected_service1}:${expected_service1_port}@${expected_service1_entity_id}"
    local second_service="${expected_service2}:${expected_service2_port}@${expected_service2_entity_id}"
    local expected_protected_services="${first_service} ${second_service}"
    local expected_environment_variables="CI_PROJECT_DIR ORG ORGUNIT EMAIL CHART_MUSEUM_URL"

    function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
    function setup() { echo "setup called" >&2; }
    function install_chartmuseum() { echo "install_chartmuseum called" >&2; }
    function protected_services() { echo "protected_services called" >&2; echo "foo:1000@foo.com bar:1000@bar.com"; }
    function service_protected() { echo "service_protected ${*} called" >&2; return 1; }
    function create_shib_certificate() { echo "create_shib_certificate ${*} called" >&2; }
    function deploy_shibboleth_proxy() { echo "deploy_shibboleth_proxy ${*} called" >&2; return 1; }
    function notify() { echo "notify ${*} called" >&2; }

    run main
    assert_failure
    assert_output -p "check_required_environment ${expected_environment_variables} called"
    assert_output -p "setup called"
    assert_output -p "install_chartmuseum called"
    assert_output -p "protected_services called"
    
    assert_output -p "service_protected ${first_service} called"
    assert_output -p "create_shib_certificate ${first_service} called" 
    assert_output -p "deploy_shibboleth_proxy ${first_service} called" 
    refute_output -p "service_protected ${second_service} called"
    refute_output -p "create_shib_certificate ${second_service} called" 
    refute_output -p "deploy_shibboleth_proxy ${second_service} called" 

    for expected_protected_service in ${expected_protected_services}
    do 
        refute_output -p "notify ${expected_protected_service} called" 
    done
}

@test "$(basename ${BATS_TEST_FILENAME}) decommission_protected_service runs setup, gets the protected_services, and with each protected_service verifies that it isnt already protected, and deletes the helm deployment" {
    source ${profile_script}

    export PROTECTED_ENTITY_ID='foo.com'
    local expected_service="foo"
    local expected_service_port='1000'
    local expected_service_entity_id='foo.com'
    local expected_service_definition="${expected_service}:${expected_service_port}@${expected_service_entity_id}"

    function helm() { 
        echo "helm ${*} called" >&2;
        if [ "${1}" == "list" ]
        then
          echo 'foo-proxy'
        fi
    }
    function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
    function setup() { echo "setup called" >&2; }
    function protected_services() { echo "protected_services ${*} called" >&2; echo "foo:1000@foo.com"; }
    function notify_decommission() { echo "notify_decommission ${*} called" >&2; }

    run decommission_protected_service
    assert_success
    assert_output -p "check_required_environment PROTECTED_ENTITY_ID called"
    assert_output -p "setup called"
    assert_output -p "protected_services ${PROTECTED_ENTITY_ID} called"
    assert_output -p "helm list -q called"
    assert_output -p "helm delete ${expected_service}-proxy"
    assert_output -p "notify_decommission ${expected_service_definition}"
}

@test "$(basename ${BATS_TEST_FILENAME}) decommission_protected_service fails if required_environment is missing" {
    source ${profile_script}

    local expected_service="foo"
    local expected_service_port='1000'
    local expected_service_entity_id='foo.com'
    local expected_service_definition="${expected_service}:${expected_service_port}@${expected_service_entity_id}"

    function helm() { 
        echo "helm ${*} called" >&2;
        if [ "${1}" == "list" ]
        then
          echo 'foo-proxy'
        fi
    }
    function check_required_environment() { echo "check_required_environment ${*} called" >&2; return 1; }
    function setup() { echo "setup called" >&2; }
    function protected_services() { echo "protected_services ${*} called" >&2; echo "foo:1000@foo.com"; }
    function notify_decommission() { echo "notify_decommission ${*} called" >&2; }

    run decommission_protected_service
    assert_failure
    assert_output -p "check_required_environment PROTECTED_ENTITY_ID called"
    refute_output -p "setup called"
    refute_output -p "protected_services ${PROTECTED_ENTITY_ID} called"
    refute_output -p "helm list -q called"
    refute_output -p "helm delete ${expected_service}-proxy"
    refute_output -p "notify_decommission ${expected_service_definition}"
}

@test "$(basename ${BATS_TEST_FILENAME}) decommission_protected_service fails if setup fails" {
    source ${profile_script}

    export PROTECTED_ENTITY_ID='foo.com'
    local expected_service="foo"
    local expected_service_port='1000'
    local expected_service_entity_id='foo.com'
    local expected_service_definition="${expected_service}:${expected_service_port}@${expected_service_entity_id}"

    function helm() { 
        echo "helm ${*} called" >&2;
        if [ "${1}" == "list" ]
        then
          echo 'foo-proxy'
        fi
    }
    function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
    function setup() { echo "setup called" >&2; return 1; }
    function protected_services() { echo "protected_services ${*} called" >&2; echo "foo:1000@foo.com"; }
    function notify_decommission() { echo "notify_decommission ${*} called" >&2; }

    run decommission_protected_service
    assert_failure
    assert_output -p "check_required_environment PROTECTED_ENTITY_ID called"
    assert_output -p "setup called"
    refute_output -p "protected_services ${PROTECTED_ENTITY_ID} called"
    refute_output -p "helm list -q called"
    refute_output -p "helm delete ${expected_service}-proxy"
    refute_output -p "notify_decommission ${expected_service_definition}"
}

@test "$(basename ${BATS_TEST_FILENAME}) decommission_protected_service fails if helm list fails" {
    source ${profile_script}

    export PROTECTED_ENTITY_ID='foo.com'
    local expected_service="foo"
    local expected_service_port='1000'
    local expected_service_entity_id='foo.com'
    local expected_service_definition="${expected_service}:${expected_service_port}@${expected_service_entity_id}"

    function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
    function setup() { echo "setup called" >&2; }
    function protected_services() { echo "protected_services ${*} called" >&2; echo "foo:1000@foo.com"; }
    function helm() { echo "helm ${*} called" >&2; [ "${1}" != "list" ]; }
    function create_shib_certificate() { echo "create_shib_certificate ${*} called" >&2; }
    function deploy_shibboleth_proxy() { echo "deploy_shibboleth_proxy ${*} called" >&2; }
    function notify() { echo "notify ${*} called" >&2; }

    run decommission_protected_service
    assert_failure
    assert_output -p "check_required_environment PROTECTED_ENTITY_ID called"
    assert_output -p "setup called"
    assert_output -p "protected_services ${PROTECTED_ENTITY_ID} called"
    assert_output -p "helm list -q called"
    refute_output -p "helm delete ${expected_service}-proxy"
    refute_output -p "notify_decommission ${expected_service_definition}"
}

@test "$(basename ${BATS_TEST_FILENAME}) decommission_protected_service fails if helm delete fails" {
    source ${profile_script}

    export PROTECTED_ENTITY_ID='foo.com'
    local expected_service="foo"
    local expected_service_port='1000'
    local expected_service_entity_id='foo.com'
    local expected_service_definition="${expected_service}:${expected_service_port}@${expected_service_entity_id}"

    function helm() { 
        echo "helm ${*} called" >&2;
        if [ "${1}" == "list" ]
        then
          echo 'foo-proxy'
        else
            return 1
        fi
    }
    function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
    function setup() { echo "setup called" >&2; }
    function protected_services() { echo "protected_services ${*} called" >&2; echo "foo:1000@foo.com"; }
    function create_shib_certificate() { echo "create_shib_certificate ${*} called" >&2; }
    function deploy_shibboleth_proxy() { echo "deploy_shibboleth_proxy ${*} called" >&2; }
    function notify() { echo "notify ${*} called" >&2; }

    run decommission_protected_service
    assert_failure
    assert_output -p "check_required_environment PROTECTED_ENTITY_ID called"
    assert_output -p "setup called"
    assert_output -p "protected_services ${PROTECTED_ENTITY_ID} called"
    assert_output -p "helm list -q called"
    assert_output -p "helm delete ${expected_service}-proxy"
    refute_output -p "notify_decommission ${expected_service_definition}"
}

@test "$(basename ${BATS_TEST_FILENAME}) decommission_protected_service skips a service if it is already decommissioned" {
    source ${profile_script}

    export PROTECTED_ENTITY_ID='foo.com'
    local expected_service="foo"
    local expected_service_port='1000'
    local expected_service_entity_id='foo.com'
    local expected_service_definition="${expected_service}:${expected_service_port}@${expected_service_entity_id}"

    function helm() { 
        echo "helm ${*} called" >&2;
        if [ "${1}" == "list" ]
        then
          echo 'bar-proxy'
        fi
    }
    function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
    function setup() { echo "setup called" >&2; }
    function protected_services() { echo "protected_services ${*} called" >&2; echo "foo:1000@foo.com"; }
    function create_shib_certificate() { echo "create_shib_certificate ${*} called" >&2; }
    function deploy_shibboleth_proxy() { echo "deploy_shibboleth_proxy ${*} called" >&2; }
    function notify() { echo "notify ${*} called" >&2; }

    run decommission_protected_service
    assert_success
    assert_output -p "check_required_environment PROTECTED_ENTITY_ID called"
    assert_output -p "setup called"
    assert_output -p "protected_services ${PROTECTED_ENTITY_ID} called"
    assert_output -p "helm list -q called"
    refute_output -p "helm delete ${expected_service}-proxy"
    refute_output -p "notify_decommission ${expected_service_definition}"
    assert_output -p "${expected_service1}-proxy is already decommissioned"
}