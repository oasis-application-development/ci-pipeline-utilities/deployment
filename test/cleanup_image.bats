load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/bin/cleanup_image"

setup() {
  export LOG_LEVEL="info"
  export BOT_KEY='BOT_KEY'
  export CI_API_V4_URL='CI_API_V4_URL'
  export CI_PROJECT_ID='CI_PROJECT_ID'
  export DELETE_FROM_REPO='DELETE_FROM_REPO'
  export DELETE_IMAGE_TAG='DELETE_IMAGE_TAG'
  export expected_repo_name="theRepo"
  export expected_image_tag="TheImageTag"
  export expected_repo_id="1"
  export expected_error_message='{"message":"error message"}'
}

@test "$(basename ${BATS_TEST_FILENAME}) repo_id takes repo_name and returns id for the image registry in project with that name" {
  source ${profile_script}

  function curl() { echo "curl ${*}" >&2; echo "[{\"name\":\"${expected_repo_name}\",\"id\":\"${expected_repo_id}\"}]"; }
  run repo_id "${expected_repo_name}"
  assert_success
  assert_output -p "curl -H "Private-Token: ${BOT_KEY}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories"
  assert_output -p "${expected_repo_id}"
}

@test "$(basename ${BATS_TEST_FILENAME}) repo_id fails if GET for project registry returns an error message" {
  source ${profile_script}

  function curl() { echo "curl ${*}" >&2; echo "${expected_error_message}"; }
  run repo_id "${expected_repo_name}"
  assert_failure
  assert_output -p "curl -H "Private-Token: ${BOT_KEY}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories"
  assert_output -p "COULD NOT GET repo_id ${expected_error_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) repo_id fails if -ci registry is not found" {
  source ${profile_script}
  export expected_repo_resp='[{"name":"otherImage","id":"1"}]'
  local expected_error="${expected_repo_name} not found in ${expected_repo_resp}"
  function curl() { echo "curl ${*}" >&2; echo "${expected_repo_resp}"; }
  run repo_id "${expected_repo_name}"
  assert_failure
  assert_output -p "curl -H "Private-Token: ${BOT_KEY}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories"
  assert_output -p "${expected_error}"
}

@test "$(basename ${BATS_TEST_FILENAME}) delete_repo_image takes repo_name and image_tag, gets repo_id and deletes image_tag" {
  source ${profile_script}

  function repo_id() { echo "repo_id ${*} called" >&2; echo "${expected_repo_id}"; }
  function curl() { echo "curl ${*}" >&2; echo "200"; }

  run delete_repo_image "${expected_repo_name}" "${expected_image_tag}"
  assert_success
  assert_output -p "repo_id ${expected_repo_name} called"
  assert_output -p "DELETING image tag ${expected_image_tag} from repo ${expected_repo_name}"
  assert_output -p "curl -X DELETE -H "Private-Token: ${BOT_KEY}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/${expected_repo_id}/tags/${expected_image_tag}"
}

@test "$(basename ${BATS_TEST_FILENAME}) delete_repo_image fails if repo_id fails" {
  source ${profile_script}
  function repo_id() { echo "repo_id ${*} called" >&2; return 1; }
  function curl() { echo "curl ${*}" >&2; echo "200"; }

  run delete_repo_image "${expected_repo_name}" "${expected_image_tag}"
  assert_failure
  assert_output -p "repo_id ${expected_repo_name} called"
  refute_output -p "DELETING image tag ${expected_image_tag} from repo ${expected_repo_name}"
  refute_output -p "curl -X DELETE"
}

@test "$(basename ${BATS_TEST_FILENAME}) delete_repo_image fails if delete fails with anything except 404 tag not found" {
  source ${profile_script}

  function repo_id() { echo "repo_id ${*} called" >&2; echo "${expected_repo_id}"; }
  function curl() { echo "curl ${*}" >&2; echo "${expected_error_message}"; }

  run delete_repo_image "${expected_repo_name}" "${expected_image_tag}"
  assert_failure
  assert_output -p "repo_id ${expected_repo_name} called"
  assert_output -p "DELETING image tag ${expected_image_tag} from repo ${expected_repo_name}"
  assert_output -p "curl -X DELETE -H "Private-Token: ${BOT_KEY}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/${expected_repo_id}/tags/${expected_image_tag}"
  assert_output -p "COULD NOT DELETE: ${expected_error_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) delete_repo_image succeeds if delete fails with 404 tag not found" {
  source ${profile_script}

  function repo_id() { echo "repo_id ${*} called" >&2; echo "${expected_repo_id}"; }
  function curl() { echo "curl ${*}" >&2; echo '{"message":"404 Tag Not Found"}'; }

  run delete_repo_image "${expected_repo_name}" "${expected_image_tag}"
  assert_success
  assert_output -p "repo_id ${expected_repo_name} called"
  assert_output -p "DELETING image tag ${expected_image_tag} from repo ${expected_repo_name}"
  assert_output -p "curl -X DELETE -H "Private-Token: ${BOT_KEY}" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/${expected_repo_id}/tags/${expected_image_tag}"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main calls check_required_environment and delete_repo_image" {
  source ${profile_script}
  local expected_required_environment="BOT_KEY CI_API_V4_URL CI_PROJECT_ID DELETE_FROM_REPO DELETE_IMAGE_TAG"
  function check_required_environment() { echo "check_required_environment ${*} called"; }
  function delete_repo_image() { echo "delete_repo_image ${*} called" >&2; }

  run run_main
  assert_success
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p "delete_repo_image ${DELETE_FROM_REPO} ${DELETE_IMAGE_TAG} called"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if check_required_environment fails" {
  source ${profile_script}
  local expected_required_environment="BOT_KEY CI_API_V4_URL CI_PROJECT_ID DELETE_FROM_REPO DELETE_IMAGE_TAG"
  function check_required_environment() { echo "check_required_environment ${*} called"; return 1; }
  function delete_repo_image() { echo "delete_repo_image ${*} called" >&2; }

  run run_main
  assert_failure
  assert_output -p "check_required_environment ${expected_required_environment} called"
  refute_output -p "delete_repo_image ${DELETE_FROM_REPO} ${DELETE_IMAGE_TAG} called"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if delete_repo_image fails" {
  source ${profile_script}
  local expected_required_environment="BOT_KEY CI_API_V4_URL CI_PROJECT_ID DELETE_FROM_REPO DELETE_IMAGE_TAG"
  function check_required_environment() { echo "check_required_environment ${*} called"; }
  function delete_repo_image() { echo "delete_repo_image ${*} called" >&2; return 1; }

  run run_main
  assert_failure
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p "delete_repo_image ${DELETE_FROM_REPO} ${DELETE_IMAGE_TAG} called"
}
