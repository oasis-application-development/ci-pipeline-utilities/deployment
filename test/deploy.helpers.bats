#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/lib/helpers/deploy"

setup() {
  export LOG_LEVEL="info"
  export HELM_CHART_NAME='HELM_CHART_NAME'
  export CI_PROJECT_NAME="CI_PROJECT_NAME"
  export CI_PROJECT_DIR="CI_PROJECT_DIR"
  export CI_ENVIRONMENT_NAME="CI_ENVIRONMENT_NAME"
}

@test "$(basename ${BATS_TEST_FILENAME})#helm_chart_dir" {
  source ${profile_script}

  run helm_chart_dir
  assert_output "${CI_PROJECT_DIR}/helm-chart/${HELM_CHART_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_project_environment fails if check_required_environment fails" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }

  run check_required_project_environment
  assert_failure
  assert_output -p "check_required_environment HELM_CHART_NAME CI_PROJECT_NAME CI_PROJECT_DIR"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_project_environment fails if HELM_CHART_NAME contains underscores" {
  source ${profile_script}
  export HELM_CHART_NAME='bad_name'
  function check_required_environment() { echo "check_required_environment ${*}">&2; }

  run check_required_project_environment
  assert_failure
  assert_output -p "check_required_environment HELM_CHART_NAME CI_PROJECT_NAME CI_PROJECT_DIR"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_project_helm_chart_dir fails if expected helm_chart_dir does not exist" {
  source ${profile_script}
  export HELM_CHART_NAME='good-name'

  refute [ -d "${CI_PROJECT_DIR}/helm-chart/${HELM_CHART_NAME}" ]
  run check_project_helm_chart_dir
  assert_failure
}

@test "$(basename ${BATS_TEST_FILENAME})#check_project_helm_chart_dir succeeds if check_required_environment passes, HELM_CHART_NAME is ok, and expected helm_chart_dir exists" {
  source ${profile_script}
  export HELM_CHART_NAME='good-name'
  export CI_PROJECT_DIR=$(mktemp -d -t deploy-bats-XXXXXX)
  mkdir -p ${CI_PROJECT_DIR}/helm-chart/${HELM_CHART_NAME}

  assert [ -d "${CI_PROJECT_DIR}/helm-chart/${HELM_CHART_NAME}" ]
  run check_project_helm_chart_dir
  assert_success
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_deployment_environment fails if expected environment is not present" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }
  function check_default_environment() { echo "check_default_environment ${*}" >&2; }
  function check_required_ingress_environment() { echo "check_required_ingress_environment" >&2; }

  run check_required_deployment_environment
  assert_failure
  assert_output -p "check_required_environment CI_ENVIRONMENT_SLUG CI_ENVIRONMENT_NAME"
  refute_output -p "check_default_environment DEPLOYMENT_NAME:HELM_CHART_NAME"
  refute_output -p "check_required_ingress_environment"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_deployment_environment fails if check_default_environment DEPLOYMENT_NAME:HELM_CHART_NAME fails" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*}" >&2; }
  function check_default_environment() { echo "check_default_environment ${*}" >&2; return 1; }
  function check_required_ingress_environment() { echo "check_required_ingress_environment" >&2; }

  run check_required_deployment_environment
  assert_failure
  assert_output -p "check_required_environment CI_ENVIRONMENT_SLUG CI_ENVIRONMENT_NAME"
  assert_output -p "check_default_environment DEPLOYMENT_NAME:HELM_CHART_NAME"
  refute_output -p "check_required_ingress_environment"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_deployment_environment fails if check_required_ingress_environment fails" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*}" >&2; }
  function check_default_environment() { echo "check_default_environment ${*}" >&2; }
  function check_required_ingress_environment() { echo "check_required_ingress_environment" >&2; return 1; }

  run check_required_deployment_environment
  assert_failure
  assert_output -p "check_required_environment CI_ENVIRONMENT_SLUG CI_ENVIRONMENT_NAME"
  assert_output -p "check_default_environment DEPLOYMENT_NAME:HELM_CHART_NAME"
  assert_output -p "check_required_ingress_environment"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_deployment_environment passes with a warning if expected environment is present, DEPLOYMENT_NAME is not present, and CI_ENVIRONMENT_NAME-HELM_CHART_NAME is too large" {
  source ${profile_script}

  export HELM_CHART_NAME="helm-chart-name"
  max_length=53
  max_env_name_length=$((${max_length} - ${#HELM_CHART_NAME}))
  this_length=$((${max_env_name_length} + 10))
  export CI_ENVIRONMENT_NAME=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${this_length} | head -n 1)
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }
  function check_default_environment() { echo "check_default_environment ${*}" >&2; }
  function check_required_ingress_environment() { echo "check_required_ingress_environment" >&2; }
  function warn() { echo "warn ${*}" >&2; }

  run check_required_deployment_environment
  assert_success
  assert_output -p "check_required_environment CI_ENVIRONMENT_SLUG CI_ENVIRONMENT_NAME"
  assert_output -p "check_default_environment DEPLOYMENT_NAME:HELM_CHART_NAME"
  assert_output -p "check_required_ingress_environment"
  assert_output -p "warn WARNING: ${CI_ENVIRONMENT_NAME}-${HELM_CHART_NAME} will be truncated!"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_deployment_environment passes if expected environment is present, DEPLOYMENT_NAME is not present, and CI_ENVIRONMENT_NAME-HELM_CHART_NAME is not too large" {
  source ${profile_script}

  export HELM_CHART_NAME="helm-chart-name"
  max_length=53
  max_env_name_length=$((${max_length} - ${#HELM_CHART_NAME}))  
  this_length=$((${max_env_name_length} - 10))
  export CI_ENVIRONMENT_NAME=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${this_length} | head -n 1)
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }
  function check_default_environment() { echo "check_default_environment ${*}" >&2; }
  function check_required_ingress_environment() { echo "check_required_ingress_environment" >&2; }
  function warn() { echo "warn ${*}" >&2; }

  run check_required_deployment_environment
  assert_success
  assert_output -p "check_required_environment CI_ENVIRONMENT_SLUG CI_ENVIRONMENT_NAME"
  assert_output -p "check_default_environment DEPLOYMENT_NAME:HELM_CHART_NAME"
  assert_output -p "check_required_ingress_environment"
  refute_output -p "warn WARNING: ${CI_ENVIRONMENT_NAME}-${HELM_CHART_NAME} will be truncated!"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_deployment_environment fails if expected environment is present, DEPLOYMENT_NAME is present, and DEPLOYMENT_NAME is too large" {
  source ${profile_script}

  this_length=54
  export DEPLOYMENT_NAME=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${this_length} | head -n 1)
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }
  function check_default_environment() { echo "check_default_environment ${*}" >&2; }
  function check_required_ingress_environment() { echo "check_required_ingress_environment" >&2; }
  function error() { echo "error ${*}" >&2; }

  run check_required_deployment_environment
  assert_failure
  assert_output -p "check_required_environment CI_ENVIRONMENT_SLUG CI_ENVIRONMENT_NAME"
  assert_output -p "check_default_environment DEPLOYMENT_NAME:HELM_CHART_NAME"
  assert_output -p "check_required_ingress_environment"
  assert_output -p "error ERROR: ${DEPLOYMENT_NAME} is too long!"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_deployment_environment passes if expected environment is present, DEPLOYMENT_NAME is present, and DEPLOYMENT_NAME is not too large" {
  source ${profile_script}

  this_length=53
  export DEPLOYMENT_NAME=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${this_length} | head -n 1)
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }
  function check_default_environment() { echo "check_default_environment ${*}" >&2; }
  function check_required_ingress_environment() { echo "check_required_ingress_environment" >&2; }
  function warn() { echo "warn ${*}" >&2; }

  run check_required_deployment_environment
  assert_success
  assert_output -p "check_required_environment CI_ENVIRONMENT_SLUG CI_ENVIRONMENT_NAME"
  assert_output -p "check_default_environment DEPLOYMENT_NAME:HELM_CHART_NAME"
  assert_output -p "check_required_ingress_environment"
  refute_output -p "warn WARNING: ${DEPLOYMENT_NAME} will be truncated!"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_ingress_environment does not check for CI_ENVIRONMENT_URL if SKIP_INGRESS is set" {
  source ${profile_script}

  export SKIP_INGRESS=1
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_required_ingress_environment
  assert_success
  refute_output -p "check_required_environment CI_ENVIRONMENT_URL"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_ingress_environment fails if CI_ENVIRONMENT_URL is not set and SKIP_INGRESS is not set" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }

  run check_required_ingress_environment
  assert_failure
  assert_output -p 'check_required_environment CI_ENVIRONMENT_URL'
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_ingress_environment passes if CI_ENVIRONMENT_URL is set and SKIP_INGRESS is not set" {
  source ${profile_script}

  export CI_ENVIRONMENT_URL='CI_ENVIRONMENT_URL'
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_required_ingress_environment
  assert_success
  assert_output -p 'check_required_environment CI_ENVIRONMENT_URL'
}

@test "$(basename ${BATS_TEST_FILENAME})#deployment_name prints DEPLOYMENT_NAME if present" {
  source ${profile_script}
  export DEPLOYMENT_NAME="DEPLOYMENT_NAME"

  local expected_output="${DEPLOYMENT_NAME}"

  run deployment_name
  assert_success
  assert_output "${expected_output}"
}

@test "$(basename ${BATS_TEST_FILENAME})#deployment_name prints CI_ENVIRONMENT_NAME-HELM_CHART_NAME if DEPLOYMENT_NAME is not present and CI_ENVIRONMENT_NAME is not too long" {
  source ${profile_script}
  export HELM_CHART_NAME="helm-chart-name"
  max_length=53
  max_env_name_length=$((${max_length} - ${#HELM_CHART_NAME}))
  this_length=$((${max_env_name_length} - 10))
  export CI_ENVIRONMENT_NAME=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${this_length} | head -n 1)
  local expected_output="${CI_ENVIRONMENT_NAME}-${HELM_CHART_NAME}"

  run deployment_name
  assert_success
  assert_output "${expected_output}"
}

@test "$(basename ${BATS_TEST_FILENAME})#deployment_name prints truncated CI_ENVIRONMENT_NAME-HELM_CHART_NAME if DEPLOYMENT_NAME is not present and CI_ENVIRONMENT_NAME is too long" {
  source ${profile_script}
  export HELM_CHART_NAME="helm-chart-name"
  max_length=53
  max_env_name_length=$((${max_length} - ${#HELM_CHART_NAME}))
  this_length=$((${max_env_name_length} + 10))
  export CI_ENVIRONMENT_NAME=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${this_length} | head -n 1)
  local expected_truncated_environment_name=$(expr substr $CI_ENVIRONMENT_NAME 1 $((45 - ${#HELM_CHART_NAME})))

  run deployment_name
  assert_success
  assert [ ${#output} -le ${max_length} ]
  assert_output -e "${HELM_CHART_NAME}"
  assert_output -e "${expected_truncated_environment_name}"
}
