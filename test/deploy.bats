#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/bin/deploy"

setup() {
  export LOG_LEVEL="info"
  export CI_PROJECT_NAME="CI_PROJECT_NAME"
  export CI_ENVIRONMENT_NAME="CI_ENVIRONMENT_NAME"
  export CI_ENVIRONMENT_SLUG="CI_ENVIRONMENT_SLUG"
  export CI_PROJECT_DIR="CI_PROJECT_DIR"
  export CI_COMMIT_REF_SLUG="CI_COMMIT_REF_SLUG"
  export CI_REGISTRY="CI_REGISTRY"
  export CI_REGISTRY_IMAGE="CI_REGISTRY_IMAGE"
  export CI_COMMIT_SHORT_SHA="CI_COMMIT_SHORT_SHA"
  export CI_DEPLOY_USER="CI_DEPLOY_USER"
  export CI_DEPLOY_PASSWORD="CI_DEPLOY_PASSWORD"
}

teardown() {
  if [ -d "${CI_PROJECT_DIR}" ]
  then
    rm -rf "${CI_PROJECT_DIR}"
  fi
  if [ -f "${tls__key}" ]
  then
    rm "${tls__key}"
  fi
  if [ -f "${tls__certificate}" ]
  then
    rm "${tls__certificate}"
  fi
  if [ -f "${tls__cacertificate}" ]
  then
    rm "${tls__cacertificate}"
  fi
}

@test "$(basename ${BATS_TEST_FILENAME})#wants_database returns false if WANTS_DATABASE environment is not set" {
  source ${profile_script}

  refute wants_database
}

@test "$(basename ${BATS_TEST_FILENAME})#wants_database returns true if WANTS_DATABASE environment is set" {
  source ${profile_script}
  export WANTS_DATABASE=1

  assert wants_database
}

@test "$(basename ${BATS_TEST_FILENAME})#check_tls_requirements returns successfully if environment variables tls__key, tls__certificate, and tls__cacertificate are not present" {
  source ${profile_script}

  run check_tls_requirements
  assert_success
}

@test "$(basename ${BATS_TEST_FILENAME})#check_tls_requirements fails if environment variables tls__key, tls__certificate, and tls__cacertificate are present and tls__key is not a file" {
  source ${profile_script}

  export tls__key="tls__key"
  export tls__certificate=$(mktemp)
  export tls__cacertificate=$(mktemp)

  run check_tls_requirements
  assert_failure
  assert_output -p "tls__key must be a file"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_tls_requirements fails if environment variables tls__key, tls__certificate, and tls__cacertificate are present and tls__certificate is not a__file" {
  source ${profile_script}

  export tls__key=$(mktemp)
  export tls__certificate="tls__certificate"
  export tls__cacertificate=$(mktemp)

  run check_tls_requirements
  assert_failure
  assert_output -p "tls__certificate must be a file"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_tls_requirements fails if environment variables tls__key, tls__certificate, and tls__cacertificate are present and tls.cacertificate is not a file" {
  source ${profile_script}

  export tls__key=$(mktemp)
  export tls__certificate=$(mktemp)
  export tls__cacertificate="tls__cacertificate"

  run check_tls_requirements
  assert_failure
  assert_output -p "tls__cacertificate must be a file"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_tls_requirements succeeds if environment variables tls__key and tls__certificate are present and all are files" {
  source ${profile_script}

  export tls__key=$(mktemp)
  export tls__certificate=$(mktemp)

  run check_tls_requirements
  assert_success
}

@test "$(basename ${BATS_TEST_FILENAME})#check_tls_requirements succeeds if environment variables tls__key, tls__certificate, and tls__cacertificate are present and all are files" {
  source ${profile_script}

  export tls__key=$(mktemp)
  export tls__certificate=$(mktemp)
  export tls__cacertificate=$(mktemp)

  run check_tls_requirements
  assert_success
}

@test "$(basename ${BATS_TEST_FILENAME})#tls_settings does not add --set-file to helm_args if environment variables tls__key, tls__certificate, and tls__cacertificate are not present" {
  source ${profile_script}

  export helm_args=()
  local expected_settings=()
  tls_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#tls_settings adds --set-file to helm_args if environment variables tls__key, tls__certificate, and tls__cacertificate are present" {
  source ${profile_script}

  export tls__key=$(mktemp)
  export tls__certificate=$(mktemp)
  export tls__cacertificate=$(mktemp)

  export helm_args=()
  local expected_settings=(--set-file tls.key=${tls__key} --set-file tls.certificate=${tls__certificate} --set-file tls.cacertificate=${tls__cacertificate})
  tls_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#tls_settings adds --set-file to helm_args if environment variables tls__key and tls__certificate are present" {
  source ${profile_script}

  export tls__key=$(mktemp)
  export tls__certificate=$(mktemp)

  export helm_args=()
  local expected_settings=(--set-file tls.key=${tls__key} --set-file tls.certificate=${tls__certificate})
  tls_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_deployment_requirements fails if check_required_project_environment fails" {
  source ${profile_script}
  
  function check_required_project_environment() { echo "check_required_project_environment called" >&2; return 1; }
  function check_project_helm_chart_dir() { echo "check_project_helm_chart_dir called" >&2; }
  function check_required_deployment_environment() { echo "check_required_deployment_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }
  function check_required_image_pull_environment() { echo "check_required_image_pull_environment called" >&2; }
  function check_required_image_environment() { echo "check_required_image_environment called" >&2; }
  function check_required_repository_metadata() { echo "check_required_repository_metadata called" >&2; }
  function check_required_database_environment() { echo "check_required_database_environment called" >&2; }
  function check_required_container_env() { echo "check_required_container_env called" >&2; }
  function check_application_specific_environment() { echo "check_application_specific_environment called" >&2; }
  function check_tls_requirements() { echo "check_tls_requirements called" >&2; }

  run check_helm_deployment_requirements
  assert_failure
  assert_output -p "check_required_project_environment called"
  refute_output -p "check_project_helm_chart_dir called"
  refute_output -p "check_required_deployment_environment called"
  refute_output -p "check_required_cluster_login_environment called"
  refute_output -p "check_required_image_pull_environment called"
  refute_output -p "check_required_image_environment called"
  refute_output -p "check_required_repository_metadata called"
  refute_output -p "check_required_database_environment called"
  refute_output -p "check_required_container_env called"
  refute_output -p "check_application_specific_environment called"
  refute_output -p "check_tls_requirements called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_deployment_requirements fails if check_project_helm_chart_dir fails" {
  source ${profile_script}
  
  function check_required_project_environment() { echo "check_required_project_environment called" >&2; }
  function check_project_helm_chart_dir() { echo "check_project_helm_chart_dir called" >&2; return 1; }
  function check_required_deployment_environment() { echo "check_required_deployment_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }
  function check_required_image_pull_environment() { echo "check_required_image_pull_environment called" >&2; }
  function check_required_image_environment() { echo "check_required_image_environment called" >&2; }
  function check_required_repository_metadata() { echo "check_required_repository_metadata called" >&2; }
  function check_required_database_environment() { echo "check_required_database_environment called" >&2; }
  function check_required_container_env() { echo "check_required_container_env called" >&2; }
  function check_application_specific_environment() { echo "check_application_specific_environment called" >&2; }
  function check_tls_requirements() { echo "check_tls_requirements called" >&2; }

  run check_helm_deployment_requirements
  assert_failure
  assert_output -p "check_required_project_environment called"
  assert_output -p "check_project_helm_chart_dir called"
  assert_output -p "check_project_helm_chart_dir called"
  refute_output -p "check_required_deployment_environment called"
  refute_output -p "check_required_cluster_login_environment called"
  refute_output -p "check_required_image_pull_environment called"
  refute_output -p "check_required_image_environment called"
  refute_output -p "check_required_repository_metadata called"
  refute_output -p "check_required_database_environment called"
  refute_output -p "check_required_container_env called"
  refute_output -p "check_application_specific_environment called"
  refute_output -p "check_tls_requirements called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_deployment_requirements fails if check_required_deployment_environment fails" {
  source ${profile_script}

  function check_required_project_environment() { echo "check_required_project_environment called" >&2; }
  function check_project_helm_chart_dir() { echo "check_project_helm_chart_dir called" >&2; }
  function check_required_deployment_environment() { echo "check_required_deployment_environment called" >&2; return 1; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }
  function check_required_image_pull_environment() { echo "check_required_image_pull_environment called" >&2; }
  function check_required_image_environment() { echo "check_required_image_environment called" >&2; }
  function check_required_repository_metadata() { echo "check_required_repository_metadata called" >&2; }
  function check_required_database_environment() { echo "check_required_database_environment called" >&2; }
  function check_required_container_env() { echo "check_required_container_env called" >&2; }
  function check_application_specific_environment() { echo "check_application_specific_environment called" >&2; }
  function check_tls_requirements() { echo "check_tls_requirements called" >&2; }

  run check_helm_deployment_requirements
  assert_failure
  assert_output -p "check_required_project_environment called"
  assert_output -p "check_project_helm_chart_dir called"
  assert_output -p "check_required_deployment_environment called"
  refute_output -p "check_required_cluster_login_environment called"
  refute_output -p "check_required_image_pull_environment called"
  refute_output -p "check_required_image_environment called"
  refute_output -p "check_required_repository_metadata called"
  refute_output -p "check_required_database_environment called"
  refute_output -p "check_required_container_env called"
  refute_output -p "check_application_specific_environment called"
  refute_output -p "check_tls_requirements called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_deployment_requirements fails if check_required_cluster_login_environment fails" {
  source ${profile_script}

  function check_required_project_environment() { echo "check_required_project_environment called" >&2; }
  function check_project_helm_chart_dir() { echo "check_project_helm_chart_dir called" >&2; }
  function check_required_deployment_environment() { echo "check_required_deployment_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; return 1; }
  function check_required_image_pull_environment() { echo "check_required_image_pull_environment called" >&2; }
  function check_required_image_environment() { echo "check_required_image_environment called" >&2; }
  function check_required_repository_metadata() { echo "check_required_repository_metadata called" >&2; }
  function check_required_database_environment() { echo "check_required_database_environment called" >&2; }
  function check_required_container_env() { echo "check_required_container_env called" >&2; }
  function check_application_specific_environment() { echo "check_application_specific_environment called" >&2; }
  function check_tls_requirements() { echo "check_tls_requirements called" >&2; }

  run check_helm_deployment_requirements
  assert_failure
  assert_output -p "check_required_project_environment called"
  assert_output -p "check_project_helm_chart_dir called"
  assert_output -p "check_required_deployment_environment called"
  assert_output -p "check_required_cluster_login_environment called"
  refute_output -p "check_required_image_pull_environment called"
  refute_output -p "check_required_image_environment called"
  refute_output -p "check_required_repository_metadata called"
  refute_output -p "check_required_database_environment called"
  refute_output -p "check_required_container_env called"
  refute_output -p "check_application_specific_environment called"
  refute_output -p "check_tls_requirements called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_deployment_requirements fails if check_required_image_pull_environment fails" {
  source ${profile_script}

  function check_required_project_environment() { echo "check_required_project_environment called" >&2; }
  function check_project_helm_chart_dir() { echo "check_project_helm_chart_dir called" >&2; }
  function check_required_deployment_environment() { echo "check_required_deployment_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }
  function check_required_image_pull_environment() { echo "check_required_image_pull_environment called" >&2; return 1; }
  function check_required_image_environment() { echo "check_required_image_environment called" >&2; }
  function check_required_repository_metadata() { echo "check_required_repository_metadata called" >&2; }
  function check_required_database_environment() { echo "check_required_database_environment called" >&2; }
  function check_required_container_env() { echo "check_required_container_env called" >&2; }
  function check_application_specific_environment() { echo "check_application_specific_environment called" >&2; }
  function check_tls_requirements() { echo "check_tls_requirements called" >&2; }

  run check_helm_deployment_requirements
  assert_failure
  assert_output -p "check_required_project_environment called"
  assert_output -p "check_project_helm_chart_dir called"
  assert_output -p "check_required_deployment_environment called"
  assert_output -p "check_required_cluster_login_environment called"
  assert_output -p "check_required_image_pull_environment called"
  refute_output -p "check_required_image_environment called"
  refute_output -p "check_required_repository_metadata called"
  refute_output -p "check_required_database_environment called"
  refute_output -p "check_required_container_env called"
  refute_output -p "check_application_specific_environment called"
  refute_output -p "check_tls_requirements called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_deployment_requirements fails if check_required_image_environment fails" {
  source ${profile_script}

  function check_required_project_environment() { echo "check_required_project_environment called" >&2; }
  function check_project_helm_chart_dir() { echo "check_project_helm_chart_dir called" >&2; }
  function check_required_deployment_environment() { echo "check_required_deployment_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }
  function check_required_image_pull_environment() { echo "check_required_image_pull_environment called" >&2; }
  function check_required_image_environment() { echo "check_required_image_environment called" >&2; return 1; }
  function check_required_repository_metadata() { echo "check_required_repository_metadata called" >&2; }
  function check_required_database_environment() { echo "check_required_database_environment called" >&2; }
  function check_required_container_env() { echo "check_required_container_env called" >&2; }
  function check_application_specific_environment() { echo "check_application_specific_environment called" >&2; }
  function check_tls_requirements() { echo "check_tls_requirements called" >&2; }

  run check_helm_deployment_requirements
  assert_failure
  assert_output -p "check_required_project_environment called"
  assert_output -p "check_project_helm_chart_dir called"
  assert_output -p "check_required_deployment_environment called"
  assert_output -p "check_required_cluster_login_environment called"
  assert_output -p "check_required_image_pull_environment called"
  assert_output -p "check_required_image_environment called"
  refute_output -p "check_required_repository_metadata called"
  refute_output -p "check_required_database_environment called"
  refute_output -p "check_required_container_env called"
  refute_output -p "check_application_specific_environment called"
  refute_output -p "check_tls_requirements called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_deployment_requirements fails if check_required_repository_metadata fails" {
  source ${profile_script}

  function check_required_project_environment() { echo "check_required_project_environment called" >&2; }
  function check_project_helm_chart_dir() { echo "check_project_helm_chart_dir called" >&2; }
  function check_required_deployment_environment() { echo "check_required_deployment_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }
  function check_required_image_pull_environment() { echo "check_required_image_pull_environment called" >&2; }
  function check_required_image_environment() { echo "check_required_image_environment called" >&2; }
  function check_required_repository_metadata() { echo "check_required_repository_metadata called" >&2; return 1; }
  function check_required_database_environment() { echo "check_required_database_environment called" >&2; }
  function check_required_container_env() { echo "check_required_container_env called" >&2; }
  function check_application_specific_environment() { echo "check_application_specific_environment called" >&2; }
  function check_tls_requirements() { echo "check_tls_requirements called" >&2; }

  run check_helm_deployment_requirements
  assert_failure
  assert_output -p "check_required_project_environment called"
  assert_output -p "check_project_helm_chart_dir called"
  assert_output -p "check_required_deployment_environment called"
  assert_output -p "check_required_cluster_login_environment called"
  assert_output -p "check_required_image_pull_environment called"
  assert_output -p "check_required_image_environment called"
  assert_output -p "check_required_repository_metadata called"
  refute_output -p "check_required_database_environment called"
  refute_output -p "check_required_container_env called"
  refute_output -p "check_application_specific_environment called"
  refute_output -p "check_tls_requirements called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_deployment_requirements fails if check_required_database_environment fails" {
  source ${profile_script}

  function check_required_project_environment() { echo "check_required_project_environment called" >&2; }
  function check_project_helm_chart_dir() { echo "check_project_helm_chart_dir called" >&2; }
  function check_required_deployment_environment() { echo "check_required_deployment_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }
  function check_required_image_pull_environment() { echo "check_required_image_pull_environment called" >&2; }
  function check_required_image_environment() { echo "check_required_image_environment called" >&2; }
  function check_required_repository_metadata() { echo "check_required_repository_metadata called" >&2; }
  function check_required_database_environment() { echo "check_required_database_environment called" >&2; return 1; }
  function check_required_container_env() { echo "check_required_container_env called" >&2; }
  function check_application_specific_environment() { echo "check_application_specific_environment called" >&2; }
  function check_tls_requirements() { echo "check_tls_requirements called" >&2; }

  run check_helm_deployment_requirements
  assert_failure
  assert_output -p "check_required_project_environment called"
  assert_output -p "check_project_helm_chart_dir called"
  assert_output -p "check_required_deployment_environment called"
  assert_output -p "check_required_cluster_login_environment called"
  assert_output -p "check_required_image_pull_environment called"
  assert_output -p "check_required_image_environment called"
  assert_output -p "check_required_repository_metadata called"
  assert_output -p "check_required_database_environment called"
  refute_output -p "check_required_container_env called"
  refute_output -p "check_application_specific_environment called"
  refute_output -p "check_tls_requirements called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_deployment_requirements fails if check_required_container_env fails" {
  source ${profile_script}

  function check_required_project_environment() { echo "check_required_project_environment called" >&2; }
  function check_project_helm_chart_dir() { echo "check_project_helm_chart_dir called" >&2; }
  function check_required_deployment_environment() { echo "check_required_deployment_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }
  function check_required_image_pull_environment() { echo "check_required_image_pull_environment called" >&2; }
  function check_required_image_environment() { echo "check_required_image_environment called" >&2; }
  function check_required_repository_metadata() { echo "check_required_repository_metadata called" >&2; }
  function check_required_database_environment() { echo "check_required_database_environment called" >&2; }
  function check_required_container_env() { echo "check_required_container_env called" >&2; return 1; }
  function check_application_specific_environment() { echo "check_application_specific_environment called" >&2; }
  function check_tls_requirements() { echo "check_tls_requirements called" >&2; }

  run check_helm_deployment_requirements
  assert_failure
  assert_output -p "check_required_project_environment called"
  assert_output -p "check_project_helm_chart_dir called"
  assert_output -p "check_required_deployment_environment called"
  assert_output -p "check_required_cluster_login_environment called"
  assert_output -p "check_required_image_pull_environment called"
  assert_output -p "check_required_image_environment called"
  assert_output -p "check_required_repository_metadata called"
  assert_output -p "check_required_database_environment called"
  assert_output -p "check_required_container_env called"
  refute_output -p "check_application_specific_environment called"
  refute_output -p "check_tls_requirements called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_deployment_requirements fails if check_application_specific_environment fails" {
  source ${profile_script}

  function check_required_project_environment() { echo "check_required_project_environment called" >&2; }
  function check_project_helm_chart_dir() { echo "check_project_helm_chart_dir called" >&2; }
  function check_required_deployment_environment() { echo "check_required_deployment_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }
  function check_required_image_pull_environment() { echo "check_required_image_pull_environment called" >&2; }
  function check_required_image_environment() { echo "check_required_image_environment called" >&2; }
  function check_required_repository_metadata() { echo "check_required_repository_metadata called" >&2; }
  function check_required_database_environment() { echo "check_required_database_environment called" >&2; }
  function check_required_container_env() { echo "check_required_container_env called" >&2; }
  function check_application_specific_environment() { echo "check_application_specific_environment called" >&2; return 1; }
  function check_tls_requirements() { echo "check_tls_requirements called" >&2; }

  run check_helm_deployment_requirements
  assert_failure
  assert_output -p "check_required_project_environment called"
  assert_output -p "check_project_helm_chart_dir called"
  assert_output -p "check_required_deployment_environment called"
  assert_output -p "check_required_cluster_login_environment called"
  assert_output -p "check_required_image_pull_environment called"
  assert_output -p "check_required_image_environment called"
  assert_output -p "check_required_repository_metadata called"
  assert_output -p "check_required_database_environment called"
  assert_output -p "check_required_container_env called"
  assert_output -p "check_application_specific_environment called"
  refute_output -p "check_tls_requirements called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_deployment_requirements fails if check_tls_requirements fails" {
  source ${profile_script}

  function check_required_project_environment() { echo "check_required_project_environment called" >&2; }
  function check_project_helm_chart_dir() { echo "check_project_helm_chart_dir called" >&2; }
  function check_required_deployment_environment() { echo "check_required_deployment_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }
  function check_required_image_pull_environment() { echo "check_required_image_pull_environment called" >&2; }
  function check_required_image_environment() { echo "check_required_image_environment called" >&2; }
  function check_required_repository_metadata() { echo "check_required_repository_metadata called" >&2; }
  function check_required_database_environment() { echo "check_required_database_environment called" >&2; }
  function check_required_container_env() { echo "check_required_container_env called" >&2; }
  function check_application_specific_environment() { echo "check_application_specific_environment called" >&2; }
  function check_tls_requirements() { echo "check_tls_requirements called" >&2; return 1; }

  run check_helm_deployment_requirements
  assert_failure
  assert_output -p "check_required_project_environment called"
  assert_output -p "check_project_helm_chart_dir called"
  assert_output -p "check_required_deployment_environment called"
  assert_output -p "check_required_cluster_login_environment called"
  assert_output -p "check_required_image_pull_environment called"
  assert_output -p "check_required_image_environment called"
  assert_output -p "check_required_repository_metadata called"
  assert_output -p "check_required_database_environment called"
  assert_output -p "check_required_container_env called"
  assert_output -p "check_application_specific_environment called"
  assert_output -p "check_tls_requirements called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_helm_deployment_requirements succeeds if checks succeed" {
  source ${profile_script}

  function check_required_project_environment() { echo "check_required_project_environment called" >&2; }
  function check_project_helm_chart_dir() { echo "check_project_helm_chart_dir called" >&2; }
  function check_required_deployment_environment() { echo "check_required_deployment_environment called" >&2; }
  function check_required_cluster_login_environment() { echo "check_required_cluster_login_environment called" >&2; }
  function check_required_image_pull_environment() { echo "check_required_image_pull_environment called" >&2; }
  function check_required_image_environment() { echo "check_required_image_environment called" >&2; }
  function check_required_repository_metadata() { echo "check_required_repository_metadata called" >&2; }
  function check_required_database_environment() { echo "check_required_database_environment called" >&2; }
  function check_required_container_env() { echo "check_required_container_env called" >&2; }
  function check_application_specific_environment() { echo "check_application_specific_environment called" >&2; }
  function check_tls_requirements() { echo "check_tls_requirements called" >&2; }

  run check_helm_deployment_requirements
  assert_success
  assert_output -p "check_required_project_environment called"
  assert_output -p "check_project_helm_chart_dir called"
  assert_output -p "check_required_deployment_environment called"
  assert_output -p "check_required_cluster_login_environment called"
  assert_output -p "check_required_image_pull_environment called"
  assert_output -p "check_required_image_environment called"
  assert_output -p "check_required_repository_metadata called"
  assert_output -p "check_required_database_environment called"
  assert_output -p "check_required_container_env called"
  assert_output -p "check_application_specific_environment called"
}

@test "$(basename ${BATS_TEST_FILENAME})#announce_deployment uses info to print deployment information" {
  source ${profile_script}

  function info() { echo "info ${*}" >&2; }

  run announce_deployment
  assert_success
  assert_output -p "info Deploy app named ${CI_PROJECT_NAME} into environment ${CI_ENVIRONMENT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#announce_deployment prints route information if CI_ENVIRONMENT_URL is present" {
  source ${profile_script}
  export CI_ENVIRONMENT_URL='http://ci-environment-url'

  function info() { echo "info ${*}" >&2; }

  run announce_deployment
  assert_success
  assert_output -p "info Deploy app named ${CI_PROJECT_NAME} into environment ${CI_ENVIRONMENT_NAME} using route url ${CI_ENVIRONMENT_URL}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_image_environment fails if neither DEPLOYMENT_IMAGE or default environment are present" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }

  run check_required_image_environment
  assert_failure
  assert_output -p "check_required_environment CI_REGISTRY_IMAGE CI_COMMIT_SHORT_SHA"
  assert_output -p "check_required_environment DEPLOYMENT_IMAGE"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_image_environment passes if default environment is present" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_required_image_environment
  assert_success
  assert_output -p "check_required_environment CI_REGISTRY_IMAGE CI_COMMIT_SHORT_SHA"
  refute_output -p "check_required_environment DEPLOYMENT_IMAGE"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_image_environment passes if DEPLOYMENT_IMAGE is set" {
  source ${profile_script}

  export DEPLOYMENT_IMAGE="DEPLOYMENT_IMAGE"
  function check_required_environment() { echo "check_required_environment ${*}" >&2; echo "${*}" | grep "DEPLOYMENT_IMAGE"; }

  run check_required_image_environment
  assert_success
  assert_output -p "check_required_environment CI_REGISTRY_IMAGE CI_COMMIT_SHORT_SHA"
  assert_output -p "check_required_environment DEPLOYMENT_IMAGE"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_image_pull_environment returns successfully if CI_PROJECT_VISIBILITY is public" {
  source ${profile_script}
  export CI_PROJECT_VISIBILITY="public"

  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_required_image_pull_environment
  assert_success
  refute_output -p "check_required_environment"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_image_pull_environment fails if CI_PROJECT_VISIBILITY is not public and expected image_pull secret environment requirements are not present" {
  source ${profile_script}
  export CI_PROJECT_VISIBILITY="private"
  local expected_image_pull_secret_environment="CI_REGISTRY CI_DEPLOY_USER CI_DEPLOY_PASSWORD"

  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }

  run check_required_image_pull_environment
  assert_failure
  assert_output -p "check_required_environment ${expected_image_pull_secret_environment}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_image_pull_environment fails if CI_PROJECT_VISIBILITY is not public and expected image_pull secret environment is present" {
  source ${profile_script}
  export CI_PROJECT_VISIBILITY="private"
  local expected_image_pull_secret_environment="CI_REGISTRY CI_DEPLOY_USER CI_DEPLOY_PASSWORD"

  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_required_image_pull_environment
  assert_success
  assert_output -p "check_required_environment ${expected_image_pull_secret_environment}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_repository_metadata fails if expected environment is not present" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1;}

  run check_required_repository_metadata
  assert_failure
  assert_output -p "check_required_environment CI_COMMIT_SHORT_SHA CI_COMMIT_REF_SLUG"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_repository_metadata succeeds if expected environment is present" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*}" >&2;}
  run check_required_repository_metadata
  assert_success
  assert_output -p "check_required_environment CI_COMMIT_SHORT_SHA CI_COMMIT_REF_SLUG"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_container_env succeeds if CONTAINER_ENV is not set" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment" >&2; }
  run check_required_container_env
  assert_success
  refute_output -p "check_required_environment"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_container_env fails if CONTAINER_ENV is set and not present" {
  source ${profile_script}
  export CONTAINER_ENV="FOO BAR"
  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }

  run check_required_container_env
  assert_failure
  assert_output -p "check_required_environment ${CONTAINER_ENV}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_container_env succeeds if CONTAINER_ENV is set and present" {
  source ${profile_script}
  export CONTAINER_ENV="FOO BAR"
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_required_container_env
  assert_success
  assert_output -p "check_required_environment ${CONTAINER_ENV}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_application_specific_environment succeeds if APPLICATION_SPECIFIC_ENVIRONMENT is not set" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment" >&2; }
  run check_application_specific_environment
  assert_success
  refute_output -p "check_required_environment"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_application_specific_environment fails if APPLICATION_SPECIFIC_ENVIRONMENT is set and a value is not present for a key in APPLICATION_SPECIFIC_ENVIRONMENT" {
  source ${profile_script}
  export APPLICATION_SPECIFIC_ENVIRONMENT="FOO BAR"
  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }

  run check_application_specific_environment
  assert_failure
  assert_output -p "check_required_environment ${APPLICATION_SPECIFIC_ENVIRONMENT}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_application_specific_environment succeeds if APPLICATION_SPECIFIC_ENVIRONMENT is set and values are present for all keys in APPLICATION_SPECIFIC_ENVIRONMENT" {
  source ${profile_script}
  export APPLICATION_SPECIFIC_ENVIRONMENT="FOO BAR"
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_application_specific_environment
  assert_success
  assert_output -p "check_required_environment ${APPLICATION_SPECIFIC_ENVIRONMENT}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_pod_metadata_environment succeeds if POD_METADATA_ENVIRONMENT is not set" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment" >&2; }
  run check_pod_metadata_environment
  assert_success
  refute_output -p "check_required_environment"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_pod_metadata_environment fails if POD_METADATA_ENVIRONMENT is set and a value is not present for a key in POD_METADATA_ENVIRONMENT" {
  source ${profile_script}
  export POD_METADATA_ENVIRONMENT="FOO BAR"
  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }

  run check_pod_metadata_environment
  assert_failure
  assert_output -p "check_required_environment ${POD_METADATA_ENVIRONMENT}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_pod_metadata_environment succeeds if POD_METADATA_ENVIRONMENT is set and values are present for all keys in POD_METADATA_ENVIRONMENT" {
  source ${profile_script}
  export POD_METADATA_ENVIRONMENT="FOO BAR"
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_pod_metadata_environment
  assert_success
  assert_output -p "check_required_environment ${POD_METADATA_ENVIRONMENT}"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_database_environment succeeds if wants_database is false" {
  source ${profile_script}

  function wants_database() { echo "wants_database called" >&2; return 1; }

  run check_required_database_environment
  assert_success
  assert_output -p "wants_database called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_database_environment succeeds if wants_database is true and DB__USER DB__PASSWORD DB__NAME DB__SERVER are set" {
  export DB__USER="DBU"
  export DB__PASSWORD="DBP"
  export DB__NAME="DBN"
  export DB__SERVER="DBS"
  source ${profile_script}

  function wants_database() { echo "wants_database called" >&2; }

  run check_required_database_environment
  assert_success
  assert_output -p "wants_database called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_database_environment succeeds if wants_database is true and DATABASE_URL is set" {
  export DATABASE_URL="DATABASE_URL"
  source ${profile_script}

  function wants_database() { echo "wants_database called" >&2; }

  run check_required_database_environment
  assert_success
  assert_output -p "wants_database called"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_database_environment fails if wants_database is true and DATABASE_URL, or DB__USER, DB__PASSWORD, DB__NAME, and DB__SERVER are missing" {
  source ${profile_script}

  function wants_database() { echo "wants_database called" >&2; }

  run check_required_database_environment
  assert_failure
  assert_output -p "wants_database called"
  assert_output -p "one of DATABASE_URL or DB__USER DB__PASSWORD DB__NAME and DB__SERVER required if WANTS_DATABASE is set"
}

@test "$(basename ${BATS_TEST_FILENAME})#image_settings sets helm_args with settings for image.repository and image.tag when DEPLOYMENT_IMAGE is not set" {
  source ${profile_script}
  export helm_args=()
  local expected_settings=(--set-string image.repository=${CI_REGISTRY_IMAGE}/${CI_PROJECT_NAME} --set-string image.tag=${CI_COMMIT_SHORT_SHA})
  image_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#image_settings sets helm_args with settings for image.full when DEPLOYMENT_IMAGE is set" {
  source ${profile_script}
  export DEPLOYMENT_IMAGE="IMAGE:TAG"
  export helm_args=()
  local expected_settings=(--set-string image.full=${DEPLOYMENT_IMAGE})
  image_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#image_pull_secret_settings adds nothing to helm_args if CI_PROJECT_VISIBILITY is public" {
  source ${profile_script}
  export CI_PROJECT_VISIBILITY="public"
  export helm_args=()
  local expected_settings=()
  image_pull_secret_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#image_pull_secret_settings sets helm_args with settings for image pull secret if CI_PROJECT_VISIBILITY is not public" {
  source ${profile_script}
  export CI_PROJECT_VISIBILITY="internal"
  export CI_DEPLOY_USER="CI_DEPLOY_USER"
  export CI_DEPLOY_PASSWORD="CI_DEPLOY_PASSWORD"
  export helm_args=()
  local expected_settings=(--set-string registry.root=${CI_REGISTRY} --set-string registry.secret.username=${CI_DEPLOY_USER} --set-string registry.secret.password=${CI_DEPLOY_PASSWORD})
  image_pull_secret_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#deployment_environment_settings sets environment but not url if SKIP_INGRESS is set" {
  export CI_ENVIRONMENT_URL='http://ci-environment-url'
  export SKIP_INGRESS=1

  source ${profile_script}

  export helm_args=()
  local expected_settings=(--set-string environment=${CI_ENVIRONMENT_SLUG})
  deployment_environment_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#deployment_environment_settings sets environment and url if deployment_infrastructure is not set" {
  export CI_ENVIRONMENT_URL='http://ci-environment-url'
  export deployment__application=1
  source ${profile_script}
  export helm_args=()
  local expected_settings=(--set-string environment=${CI_ENVIRONMENT_SLUG} --set-string url=${CI_ENVIRONMENT_URL})
  deployment_environment_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#repository_metadata_settings sets expected helm_args" {
  source ${profile_script}
  export helm_args=()
  local expected_settings=(--set-string git_commit=${CI_COMMIT_SHORT_SHA} --set-string git_ref=${CI_COMMIT_REF_SLUG})
  repository_metadata_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#container_env_settings adds nothing to helm_args if CONTAINER_ENV and OPTIONAL_CONTAINER_ENV are not set" {
  source ${profile_script}
  export helm_args=()
  container_env_settings
  assert_empty "$(echo "${helm_args}")"
}

@test "$(basename ${BATS_TEST_FILENAME})#container_env_settings sets helm_args wiht settings in CONTAINER_ENV if set" {
  source ${profile_script}
  export CONTAINER_ENV="FOO fooBar"
  export FOO="FOO"
  export fooBar="fooBar"
  local expected_settings=(--set-string containerEnv.FOO="FOO" --set-string containerEnv.fooBar="fooBar")
  export helm_args=()
  container_env_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#container_env_settings sets helm_args settings with spaces and commas escaped if present" {
  source ${profile_script}
  export CONTAINER_ENV="foo fooComma fooSpace"
  export foo="foo"
  export fooComma="foo,comma"
  export fooSpace="foo space"
  export helm_args=()
  local expected_settings=(--set-string containerEnv.foo="foo" --set-string containerEnv.fooComma="foo\,comma" --set-string containerEnv.fooSpace="foo\ space")
  container_env_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#container_env_settings sets helm_args with settings in OPTIONAL_CONTAINER_ENV if set and CONTAINER_ENV is not set" {
  source ${profile_script}
  export OPTIONAL_CONTAINER_ENV="OFOO oBar"
  export OFOO="OFOO"
  export oBar="oBar"
  export helm_args=()
  local expected_settings=(--set-string containerEnv.OFOO="OFOO" --set-string containerEnv.oBar="oBar")
  container_env_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#container_env_settings sets helm_args with settings in OPTIONAL_CONTAINER_ENV with spaces and commas escaped" {
  source ${profile_script}
  export OPTIONAL_CONTAINER_ENV="ofoo ofooComma ofooSpace"
  export ofoo="ofoo"
  export ofooComma="ofoo,comma"
  export ofooSpace="ofoo space"
  export helm_args=()
  local expected_settings=(--set-string containerEnv.ofoo="ofoo" --set-string containerEnv.ofooComma="ofoo\,comma" --set-string containerEnv.ofooSpace="ofoo\ space")
  container_env_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#container_env_settings sets helm_args with settings only for defined environment in OPTIONAL_CONTAINER_ENV" {
  source ${profile_script}
  export OPTIONAL_CONTAINER_ENV="OFOO oBar"
  export OFOO="OFOO"
  export helm_args=()
  local expected_settings=(--set-string containerEnv.OFOO="OFOO")
  container_env_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#container_env_settings sets helm_args with settings in CONTAINER_ENV and OPTIONAL_CONTAINER_ENV if both are set" {
  source ${profile_script}
  export CONTAINER_ENV="FOO fooBar"
  export FOO="FOO"
  export fooBar="fooBar"
  export OPTIONAL_CONTAINER_ENV="OFOO oBar"
  export OFOO="OFOO"
  export oBar="oBar"
  local expected_settings=(--set-string containerEnv.FOO="FOO" --set-string containerEnv.fooBar="fooBar" --set-string containerEnv.OFOO="OFOO" --set-string containerEnv.oBar="oBar")
  export helm_args=()
  container_env_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#application_specific_settings does not add to helm_args if neither APPLICATION_SPECIFIC_ENVIRONMENT or OPTIONAL_APPLICATION_SPECIFIC_ENVIRONMENT are set" {
  source ${profile_script}
  export helm_args=()
  application_specific_settings
  assert_empty "$(echo "${helm_args[@]}")"
}

@test "$(basename ${BATS_TEST_FILENAME})#application_specific_settings set helm_args with settings in APPLICATION_SPECIFIC_ENVIRONMENT if set" {
  source ${profile_script}
  export APPLICATION_SPECIFIC_ENVIRONMENT="FOO fooBar"
  export FOO="FOO"
  export fooBar="fooBar"
  local expected_settings=(--set-string FOO="FOO" --set-string fooBar="fooBar")
  export helm_args=()
  application_specific_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#application_specific_settings escapes commas and strings in values" {
  source ${profile_script}
  export APPLICATION_SPECIFIC_ENVIRONMENT="FOO fooComma fooSpace"
  export FOO="FOO"
  export fooComma="foo,comma"
  export fooSpace="foo space"
  local expected_settings=(--set-string FOO="FOO" --set-string fooComma="foo\,comma" --set-string fooSpace="foo\ space")
  export helm_args=()
  application_specific_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#application_specific_settings converts keys in APPLICATION_SPECIFIC_ENVIRONMENT with __ to use dots in place of __" {
  source ${profile_script}
  export APPLICATION_SPECIFIC_ENVIRONMENT="FOO__BAR KEYC__VC__Xy keyD__vD__Ab__cD"
  export FOO__BAR="FOO__BAR"
  export KEYC__VC__Xy="KEYC__VC__Xy"
  export keyD__vD__Ab__cD="keyD__vD__Ab__cD"
  local expected_settings=(--set-string FOO.BAR="FOO__BAR" --set-string KEYC.VC.Xy="KEYC__VC__Xy" --set-string keyD.vD.Ab.cD="keyD__vD__Ab__cD")
  export helm_args=()
  application_specific_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#application_specific_settings converts keys in APPLICATION_SPECIFIC_ENVIRONMENT with __ to use dots in place of __ with escaped commas and strings in values" {
  source ${profile_script}
  export APPLICATION_SPECIFIC_ENVIRONMENT="FOO__BAR KEYC__VC__Xy keyD__vD__Ab__cD"
  export FOO__BAR="FOO.BAR"
  export KEYC__VC__Xy="KEYC.VC,value"
  export keyD__vD__Ab__cD="keyD.Vd value"
  local expected_settings=(--set-string FOO.BAR="FOO.BAR" --set-string KEYC.VC.Xy="KEYC.VC\,value" --set-string keyD.vD.Ab.cD="keyD.Vd\ value")
  export helm_args=()
  application_specific_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#application_specific_settings sets helm-args with settings from OPTIONAL_APPLICATION_SPECIFIC_ENVIRONMENT if set" {
  source ${profile_script}
  export OPTIONAL_APPLICATION_SPECIFIC_ENVIRONMENT="oFoo OBAR"
  export oFoo="oFoo"
  export OBAR="OBAR"
  local expected_settings=(--set-string oFoo="oFoo" --set-string OBAR="OBAR")
  export helm_args=()
  application_specific_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#application_specific_settings converts keys in OPTIONAL_APPLICATION_SPECIFIC_ENVIRONMENT with __ to use dots in place of __" {
  source ${profile_script}
  export OPTIONAL_APPLICATION_SPECIFIC_ENVIRONMENT="A__B A__B__C keyC__valueD__eF__gee"
  export A__B="A.Bvalue"
  export A__B__C="A.B.Cvalue"
  export keyC__valueD__eF__gee="keyC.valueD.eF.gee-value"
  local expected_settings=(--set-string A.B="A.Bvalue" --set-string A.B.C="A.B.Cvalue" --set-string keyC.valueD.eF.gee="keyC.valueD.eF.gee-value")
  export helm_args=()
  application_specific_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#application_specific_settings converts keys in OPTIONAL_APPLICATION_SPECIFIC_ENVIRONMENT with __ to use dots in place of with escaped spaces and commas in values" {
  source ${profile_script}
  export OPTIONAL_APPLICATION_SPECIFIC_ENVIRONMENT="A__B A__B__C keyC__valueD__eF__gee"
  export A__B="A Bvalue"
  export A__B__C="A.B,Cvalue"
  export keyC__valueD__eF__gee="keyC valueD.eF, gee-value"
  local expected_settings=(--set-string A.B="A\ Bvalue" --set-string A.B.C="A.B\,Cvalue" --set-string keyC.valueD.eF.gee="keyC\ valueD.eF\,\ gee-value")
  export helm_args=()
  application_specific_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#application_specific_settings skips a key in OPTIONAL_APPLICATION_SPECIFIC_ENVIRONMENT if it is not set" {
  source ${profile_script}
  export OPTIONAL_APPLICATION_SPECIFIC_ENVIRONMENT="OFOO OBAR"
  export OBAR="OBAR"
  local expected_settings=(--set-string OBAR="OBAR")
  export helm_args=()
  application_specific_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#application_specific_settings sets helm_args with settings from both APPLICATION_SPECIFIC_ENVIRONMENT and OPTIONAL_APPLICATION_SPECIFIC_ENVIRONMENT if set" {
  source ${profile_script}
  export APPLICATION_SPECIFIC_ENVIRONMENT="FOO fooBar"
  export FOO="FOO"
  export fooBar="fooBar"
  export OPTIONAL_APPLICATION_SPECIFIC_ENVIRONMENT="OFOO oFooBar"
  export OFOO="OFOO"
  export oFooBar="oFooBar"
  local expected_settings=(--set-string FOO="FOO" --set-string fooBar="fooBar" --set-string OFOO="OFOO" --set-string oFooBar="oFooBar")
  export helm_args=()
  application_specific_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#application_specific_settings sets helm_args with settings from both APPLICATION_SPECIFIC_ENVIRONMENT and OPTIONAL_APPLICATION_SPECIFIC_ENVIRONMENT with escaped spaces and commas if set" {
  source ${profile_script}
  export APPLICATION_SPECIFIC_ENVIRONMENT="FOO fooBar"
  export FOO="FOO,value"
  export fooBar="fooBar value"
  export OPTIONAL_APPLICATION_SPECIFIC_ENVIRONMENT="OFOO oFooBar"
  export OFOO="O,FOO"
  export oFooBar="oFoo Bar"
  local expected_settings=(--set-string FOO="FOO\,value" --set-string fooBar="fooBar\ value" --set-string OFOO="O\,FOO" --set-string oFooBar="oFoo\ Bar")
  export helm_args=()
  application_specific_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "#(basename ${BATS_TEST_FILENAME})#pod_metadata_settings does not add to helm_args if neither POD_METADATA_ENVIRONMENT or OPTIONAL_POD_METADATA_ENVIRONMENT are set" {
  source ${profile_script}
  export helm_args=()
  pod_metadata_settings
  assert_empty "$(echo "${helm_args[@]}")"
}

@test "#(basename ${BATS_TEST_FILENAME})#pod_metadata_settings set helm_args with settings in POD_METADATA_ENVIRONMENT if set" {
  source ${profile_script}
  export POD_METADATA_ENVIRONMENT="FOO fooBar"
  export FOO="FOO"
  export fooBar="fooBar"
  local expected_settings=(--set-string meta.FOO="FOO" --set-string meta.fooBar="fooBar")
  export helm_args=()
  pod_metadata_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#pod_metadata_settings escapes commas and strings in values" {
  source ${profile_script}
  export POD_METADATA_ENVIRONMENT="FOO fooComma fooSpace"
  export FOO="FOO"
  export fooComma="foo,comma"
  export fooSpace="foo space"
  local expected_settings=(--set-string meta.FOO="FOO" --set-string meta.fooComma="foo\,comma" --set-string meta.fooSpace="foo\ space")
  export helm_args=()
  pod_metadata_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#pod_metadata_settings converts keys in POD_METADATA_ENVIRONMENT with __ to use dots in place of __" {
  source ${profile_script}
  export POD_METADATA_ENVIRONMENT="FOO__BAR KEYC__VC__Xy keyD__vD__Ab__cD"
  export FOO__BAR="FOO__BAR"
  export KEYC__VC__Xy="KEYC__VC__Xy"
  export keyD__vD__Ab__cD="keyD__vD__Ab__cD"
  local expected_settings=(--set-string meta.FOO.BAR="FOO__BAR" --set-string meta.KEYC.VC.Xy="KEYC__VC__Xy" --set-string meta.keyD.vD.Ab.cD="keyD__vD__Ab__cD")
  export helm_args=()
  pod_metadata_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#pod_metadata_settings converts keys in POD_METADATA_ENVIRONMENT with __ to use dots in place of __ with escaped commas and strings in values" {
  source ${profile_script}
  export POD_METADATA_ENVIRONMENT="FOO__BAR KEYC__VC__Xy keyD__vD__Ab__cD"
  export FOO__BAR="FOO.BAR"
  export KEYC__VC__Xy="KEYC.VC,value"
  export keyD__vD__Ab__cD="keyD.Vd value"
  local expected_settings=(--set-string meta.FOO.BAR="FOO.BAR" --set-string meta.KEYC.VC.Xy="KEYC.VC\,value" --set-string meta.keyD.vD.Ab.cD="keyD.Vd\ value")
  export helm_args=()
  pod_metadata_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "#(basename ${BATS_TEST_FILENAME})#pod_metadata_settings set helm_args with settings in OPTIONAL_POD_METADATA_ENVIRONMENT if set" {
  source ${profile_script}
  export OPTIONAL_POD_METADATA_ENVIRONMENT="FOO fooBar"
  export FOO="FOO"
  export fooBar="fooBar"
  local expected_settings=(--set-string meta.FOO="FOO" --set-string meta.fooBar="fooBar")
  export helm_args=()
  pod_metadata_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#pod_metadata_settings escapes commas and strings in OPTIONAL_POD_METADATA_ENVIRONMENT values" {
  source ${profile_script}
  export OPTIONAL_POD_METADATA_ENVIRONMENT="FOO fooComma fooSpace"
  export FOO="FOO"
  export fooComma="foo,comma"
  export fooSpace="foo space"
  local expected_settings=(--set-string meta.FOO="FOO" --set-string meta.fooComma="foo\,comma" --set-string meta.fooSpace="foo\ space")
  export helm_args=()
  pod_metadata_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#pod_metadata_settings converts keys in OPTIONAL_POD_METADATA_ENVIRONMENT with __ to use dots in place of __" {
  source ${profile_script}
  export OPTIONAL_POD_METADATA_ENVIRONMENT="FOO__BAR KEYC__VC__Xy keyD__vD__Ab__cD"
  export FOO__BAR="FOO__BAR"
  export KEYC__VC__Xy="KEYC__VC__Xy"
  export keyD__vD__Ab__cD="keyD__vD__Ab__cD"
  local expected_settings=(--set-string meta.FOO.BAR="FOO__BAR" --set-string meta.KEYC.VC.Xy="KEYC__VC__Xy" --set-string meta.keyD.vD.Ab.cD="keyD__vD__Ab__cD")
  export helm_args=()
  pod_metadata_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#pod_metadata_settings converts keys in OPTIONAL_POD_METADATA_ENVIRONMENT with __ to use dots in place of __ with escaped commas and strings in values" {
  source ${profile_script}
  export OPTIONAL_POD_METADATA_ENVIRONMENT="FOO__BAR KEYC__VC__Xy keyD__vD__Ab__cD"
  export FOO__BAR="FOO.BAR"
  export KEYC__VC__Xy="KEYC.VC,value"
  export keyD__vD__Ab__cD="keyD.Vd value"
  local expected_settings=(--set-string meta.FOO.BAR="FOO.BAR" --set-string meta.KEYC.VC.Xy="KEYC.VC\,value" --set-string meta.keyD.vD.Ab.cD="keyD.Vd\ value")
  export helm_args=()
  pod_metadata_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "#(basename ${BATS_TEST_FILENAME})#pod_metadata_settings skips a key in OPTIONAL_POD_METADATA_ENVIRONMENT if it is not set" {
  source ${profile_script}
  export OPTIONAL_POD_METADATA_ENVIRONMENT="OFOO OBAR"
  export OBAR="OBAR"
  local expected_settings=(--set-string meta.OBAR="OBAR")
  export helm_args=()
  pod_metadata_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "#(basename ${BATS_TEST_FILENAME})#pod_metadata_settings sets helm_args with settings from both POD_METADATA_ENVIRONMENT and OPTIONAL_POD_METADATA_ENVIRONMENT if set" {
  source ${profile_script}
  export POD_METADATA_ENVIRONMENT="FOO fooBar"
  export FOO="FOO"
  export fooBar="fooBar"
  export OPTIONAL_POD_METADATA_ENVIRONMENT="OFOO oFooBar"
  export OFOO="OFOO"
  export oFooBar="oFooBar"
  local expected_settings=(--set-string meta.FOO="FOO" --set-string meta.fooBar="fooBar" --set-string meta.OFOO="OFOO" --set-string meta.oFooBar="oFooBar")
  export helm_args=()
  pod_metadata_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#database_environment_settings does not add to helm_args if wants_database is false" {
  source ${profile_script}
  
  function wants_database() { echo "wants_database called" >&2; return 1; }

  export helm_args=()
  database_environment_settings
  assert_empty "$(echo "${helm_args[@]}")"
}

@test "$(basename ${BATS_TEST_FILENAME})#database_environment_settings sets helm_args DATABASE_URL if wants_database is true and DATABASE_URL is set" {
  source ${profile_script}

  function wants_database() { echo "wants_database called" >&2; }

  export DATABASE_URL="DATABASE_URL"
  local expected_settings=(--set-string database_url=${DATABASE_URL})
  export helm_args=()
  database_environment_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#database_environment_settings sets helm_args with settings if wants_database is true and DB__USER DB__PASSWORD DB__SERVER and DB__NAME are set" {
  source ${profile_script}
  
  function wants_database() { echo "wants_database called" >&2; }
  
  export DB__NAME="DB__NAME"
  export DB__USER="DB__USER"
  export DB__PASSWORD="DB__PASSWORD"
  export DB__SERVER="DB__SERVER"
 
  local expected_settings=(--set-string db.name=${DB__NAME} --set-string db.user=${DB__USER} --set-string db.password=${DB__PASSWORD} --set-string db.server=${DB__SERVER})
  export helm_args=()
  database_environment_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#database_environment_settings sets helm_args with settings with persistent db setting if wants_database is true and PERSIST_DB is set" {
  source ${profile_script}

  function wants_database() { echo "wants_database called" >&2; }
  
  export DB__NAME="DB__NAME"
  export DB__USER="DB__USER"
  export DB__PASSWORD="DB__PASSWORD"
  export DB__SERVER="DB__SERVER"
  export PERSIST_DB=1
 
  local expected_settings=(--set-string db.name=${DB__NAME} --set-string db.user=${DB__USER} --set-string db.password=${DB__PASSWORD} --set-string db.server=${DB__SERVER} --set-string db.persistent=1)
  export helm_args=()
  database_environment_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#database_environment_settings sets helm_args with settings with seedMockData set if wants_database is true and SEED_MOCK_DATA is 1" {
  source ${profile_script}

  function wants_database() { echo "wants_database called" >&2; }
  
  export DB__NAME="DB__NAME"
  export DB__USER="DB__USER"
  export DB__PASSWORD="DB__PASSWORD"
  export DB__SERVER="DB__SERVER"
  export SEED_MOCK_DATA=1
 
  local expected_settings=(--set-string db.name=${DB__NAME} --set-string db.user=${DB__USER} --set-string db.password=${DB__PASSWORD} --set-string db.server=${DB__SERVER} --set-string db.seedMockData=1)
  export helm_args=()
  database_environment_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#database_environment_settings sets helm_args with settings with db.image set if wants_database is true and DB_IMAGE is set" {
  source ${profile_script}

  function wants_database() { echo "wants_database called" >&2; }

  export DB_IMAGE="DB_IMAGE" 
  export DB__NAME="DB__NAME"
  export DB__USER="DB__USER"
  export DB__PASSWORD="DB__PASSWORD"
  export DB__SERVER="DB__SERVER"
 
  local expected_settings=(--set-string db.name=${DB__NAME} --set-string db.user=${DB__USER} --set-string db.password=${DB__PASSWORD} --set-string db.server=${DB__SERVER} --set-string db.image=${DB_IMAGE})
  export helm_args=()
  database_environment_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#database_environment_settings sets helm_args with settings with db.image, persistent db and seedMockData set if wants_database is true, DB_IMAGE, PERSIST_DB , and SEED_MOCK_DATA are set" {
  source ${profile_script}

  function wants_database() { echo "wants_database called" >&2; }
  
  export DB_IMAGE="DB_IMAGE"
  export DB__NAME="DB__NAME"
  export DB__USER="DB__USER"
  export DB__PASSWORD="DB__PASSWORD"
  export DB__SERVER="DB__SERVER"
  export SEED_MOCK_DATA=1
  export PERSIST_DB=1
 
  local expected_settings=(--set-string db.name=${DB__NAME} --set-string db.user=${DB__USER} --set-string db.password=${DB__PASSWORD} --set-string db.server=${DB__SERVER} --set-string db.image=${DB_IMAGE} --set-string db.persistent=1 --set-string db.seedMockData=1)
  export helm_args=()
  database_environment_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#database_environment_settings sets helm_args with database_url with persistent db setting if wants_database is true and PERSIST_DB is set" {
  source ${profile_script}

  function wants_database() { echo "wants_database called" >&2; }
  
  export DATABASE_URL="DATABASE_URL"
  export PERSIST_DB=1
 
  local expected_settings=(--set-string database_url=${DATABASE_URL} --set-string db.persistent=1)
  export helm_args=()
  database_environment_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#database_environment_settings sets helm_args with database_url with seedMockData set if wants_database is true and SEED_MOCK_DATA is 1" {
  source ${profile_script}

  function wants_database() { echo "wants_database called" >&2; }
  
  export DATABASE_URL="DATABASE_URL"
  export SEED_MOCK_DATA=1
 
  local expected_settings=(--set-string database_url=${DATABASE_URL} --set-string db.seedMockData=1)
  export helm_args=()
  database_environment_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#database_environment_settings sets helm_args with database_url with db.image set if wants_database is true and DB_IMAGE is set" {
  source ${profile_script}

  function wants_database() { echo "wants_database called" >&2; }

  export DB_IMAGE="DB_IMAGE" 
  export DATABASE_URL="DATABASE_URL"
 
  local expected_settings=(--set-string database_url=${DATABASE_URL} --set-string db.image=${DB_IMAGE})
  export helm_args=()
  database_environment_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#database_environment_settings sets helm_args with database_url with db.image, persistent db and seedMockData set if wants_database is true, DB_IMAGE, PERSIST_DB , and SEED_MOCK_DATA are set" {
  source ${profile_script}

  function wants_database() { echo "wants_database called" >&2; }
  
  export DB_IMAGE="DB_IMAGE"
  export DATABASE_URL="DATABASE_URL"
  export SEED_MOCK_DATA=1
  export PERSIST_DB=1
 
  local expected_settings=(--set-string database_url=${DATABASE_URL} --set-string db.image=${DB_IMAGE} --set-string db.persistent=1 --set-string db.seedMockData=1)
  export helm_args=()
  database_environment_settings
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#get_for_labels" {
  source ${profile_script}

  function kubectl() { echo "kubectl ${*}" >&2; }

  local object="pods"
  local labels="foo=bar"

  run get_for_labels "${object}" "${labels}"
  assert_output "kubectl get ${object} -l ${labels}"
}

@test "$(basename ${BATS_TEST_FILENAME})#get_pods prints pods for the deployed app using the orbs_labels if POD_LABEL_QUERY is not set" {
  source ${profile_script}

  local expected_label="orbs_labels"

  function orbs_labels() { echo "orbs_labels"; }
  function get_for_labels() { echo "get_for_labels ${*}" >&2; }

  run get_pods
  assert_output -p "get_for_labels pods ${expected_label}"
}

@test "$(basename ${BATS_TEST_FILENAME})#get_pods prints pods for the deployed app POD_LABEL_QUERY if it is set" {
  source ${profile_script}

  local expected_label="foo=bar"
  export POD_LABEL_QUERY="${expected_label}"

  function orbs_labels() { echo "orbs_labels"; }
  function get_for_labels() { echo "get_for_labels ${*}" >&2; }

  run get_pods
  assert_output -p "get_for_labels pods ${expected_label}"
}

@test "$(basename ${BATS_TEST_FILENAME})#get_pods prints pods for the deployed app POD_LABEL_QUERY with interpolated variables if set" {
  source ${profile_script}

  export FOO='bar'
  export POD_LABEL_QUERY='foo=${FOO}'
  local expected_label="foo=${FOO}"

  function orbs_labels() { echo "orbs_labels"; }
  function get_for_labels() { echo "get_for_labels ${*}" >&2; }

  run get_pods
  assert_output -p "get_for_labels pods ${expected_label}"
}

@test "$(basename ${BATS_TEST_FILENAME})#watch_deployment returns if SKIP_WATCH_DEPLOYMENT is set" {
  source ${profile_script}
  export SKIP_WATCH_DEPLOYMENT=1
  local expected_deployment_labels="orbs_labels"

  function orbs_labels() { echo "orbs_labels"; }
  function kubectl() { echo "kubectl ${*}" >&2; }

  run watch_deployment
  assert_success
  refute_output -p "waiting until all ${expected_deployment_labels} deployments are ready"
  refute_output -p "kubectl rollout status -l ${expected_deployment_labels} deploy -w"
}

@test "$(basename ${BATS_TEST_FILENAME})#watch_deployment prints information about expected orbs_labels and returns during dry_run" {
  source ${profile_script}
  local expected_deployment_labels="orbs_labels"

  function dry_run() { return; }
  function orbs_labels() { echo "orbs_labels"; }
  function kubectl() { echo "kubectl ${*}" >&2; }

  run watch_deployment
  assert_success
  assert_output -p "waiting until all ${expected_deployment_labels} deployments are ready"
  refute_output -p "kubectl rollout status -l ${expected_deployment_labels} deploy -w"
}

@test "$(basename ${BATS_TEST_FILENAME})#watch_deployment watches all orbs_label deployment rollouts" {
  source ${profile_script}
  local expected_deployment_labels="orbs_labels"

  function orbs_labels() { echo "orbs_labels"; }
  function kubectl() { echo "kubectl ${*}" >&2; }

  run watch_deployment
  assert_success
  assert_output -p "waiting until all ${expected_deployment_labels} deployments are ready"
  assert_output -p "kubectl rollout status -l ${expected_deployment_labels} deploy -w"
}

@test "$(basename ${BATS_TEST_FILENAME})#watch_deployment fails if kubectl rollout fails" {
  source ${profile_script}
  local expected_deployment_labels="orbs_labels"

  function orbs_labels() { echo "orbs_labels"; }
  function kubectl() { echo "kubectl ${*}" >&2; return 1; }
  function get_pods() { echo "get_pods" >&2; }

  run watch_deployment
  assert_failure
  assert_output -p "waiting until all ${expected_deployment_labels} deployments are ready"
  assert_output -p "kubectl rollout status -l ${expected_deployment_labels} deploy -w"
}

@test "$(basename ${BATS_TEST_FILENAME})#describe uses kubectl to describe the object for the app and environment" {
  source ${profile_script}
  function kubectl() { echo "kubectl ${*}" >&2; }

  local object='deployments'

  run describe ${object}
  assert_success
  assert_output "kubectl describe ${object} -l app=${CI_PROJECT_NAME},environment=${CI_ENVIRONMENT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#describe adds extra labels if provided" {
  source ${profile_script}

  function kubectl() { echo "kubectl ${*}" >&2; }

  local object='deployments'
  local extra_labels='foo=bar,baz=blip'
  run describe ${object} ${extra_labels}
  assert_success
  assert_output "kubectl describe ${object} -l app=${CI_PROJECT_NAME},environment=${CI_ENVIRONMENT_NAME},${extra_labels}"
}

@test "$(basename ${BATS_TEST_FILENAME})#describe fails if kubectl fails" {
  source ${profile_script}

  function kubectl() { echo "kubectl ${*}" >&2; return 1; }

  local object='deployments'

  run describe ${object}
  assert_failure
  assert_output "kubectl describe ${object} -l app=${CI_PROJECT_NAME},environment=${CI_ENVIRONMENT_NAME}"
}

@test "$(basename ${BATS_TEST_FILENAME})#collect_args calls the common argument generators" {
  source ${profile_script}

  export helm_args=()
  function image_settings() { helm_args+=(image_settings); }
  function image_pull_secret_settings() { helm_args+=(image_pull_secret_settings); }
  function deployment_environment_settings() { helm_args+=(deployment_environment_settings); }
  function repository_metadata_settings() { helm_args+=(repository_metadata_settings); }
  function database_environment_settings() { helm_args+=(database_environment_settings); }
  function container_env_settings() { helm_args+=(container_env_settings); }
  function application_specific_settings() { helm_args+=(application_specific_settings); }
  function pod_metadata_settings() { helm_args+=(pod_metadata_settings); }
  function tls_settings() { helm_args+=(tls_settings); }

  local expected_settings=(image_settings image_pull_secret_settings deployment_environment_settings repository_metadata_settings database_environment_settings container_env_settings application_specific_settings pod_metadata_settings tls_settings)
  collect_args
  assert_equal "$(echo "${helm_args[@]}")" "$(echo "${expected_settings[@]}")"
  assert [ "${#helm_args[@]}" == "${#expected_settings[@]}" ]
}

@test "$(basename ${BATS_TEST_FILENAME})#lint_template prints helm lint command if DRY_RUN is set" {
  source ${profile_script}

  function info() { echo "info ${*}" >&2; }
  function dry_run() { return; }
  function helm() { echo "helm called" >&2; }
  function helm_chart_dir() { echo "helm_chart_dir"; }
  function deployment_name() { echo "deployment_name"; }
  function collect_args() { helm_args+=(collect_args); }

  run lint_template
  assert_success
  refute_output -p "helm called"
  assert_output -p "info helm lint helm_chart_dir collect_args"
}

@test "$(basename ${BATS_TEST_FILENAME})#lint_template lints the helm template" {
  source ${profile_script}

  function helm() { echo "helm ${*}" >&2; }
  function helm_chart_dir() { echo "helm_chart_dir"; }
  function collect_args() { helm_args+=(collect_args); }

  run lint_template
  assert_success
  assert_output "helm lint helm_chart_dir collect_args"
}

@test "$(basename ${BATS_TEST_FILENAME})#lint_template fails if helm lint fails" {
  source ${profile_script}

  function helm() { echo "helm ${*}" >&2; return 1; }
  function helm_chart_dir() { echo "helm_chart_dir"; }
  function deployment_name() { echo "deployment_name"; }
  function collect_args() { helm_args+=(collect_args); }

  run lint_template
  assert_failure
  assert_output "helm lint helm_chart_dir collect_args"
}

@test "$(basename ${BATS_TEST_FILENAME})#print_template prints the helm template if the PRINT_TEMPLATE is set" {
  source ${profile_script}

  export PRINT_TEMPLATE=1

  function helm() { echo "helm ${*}" >&2; }
  function helm_chart_dir() { echo "helm_chart_dir"; }
  function deployment_name() { echo "deployment_name"; }
  function collect_args() { helm_args+=(collect_args); }

  run print_template
  assert_success
  assert_output "helm template deployment_name helm_chart_dir collect_args"
}

@test "$(basename ${BATS_TEST_FILENAME})#print_template does not print the helm template if the PRINT_TEMPLATE is not set" {
  source ${profile_script}

  function helm() { echo "helm ${*}" >&2; }
  function helm_chart_dir() { echo "helm_chart_dir"; }
  function deployment_name() { echo "deployment_name"; }
  function collect_args() { helm_args+=(collect_args); }

  run print_template
  assert_success
  refute_output "helm template deployment_name helm_chart_dir collect_args"
}

@test "$(basename ${BATS_TEST_FILENAME})#print_template fails if helm template fails" {
  source ${profile_script}

  export PRINT_TEMPLATE=1

  function helm() { echo "helm ${*}" >&2; return 1; }
  function helm_chart_dir() { echo "helm_chart_dir"; }
  function deployment_name() { echo "deployment_name"; }
  function collect_args() { helm_args+=(collect_args); }

  run print_template
  assert_failure
  assert_output "helm template deployment_name helm_chart_dir collect_args"
}

@test "$(basename ${BATS_TEST_FILENAME})#deploy_template prints information and returns during dry_run" {
  source ${profile_script}

  function info() { echo "info ${*}" >&2; }
  function helm() { echo "helm ${*}" >&2; }
  function dry_run() { return; }
  function helm_chart_dir() { echo "helm_chart_dir"; }
  function deployment_name() { echo "deployment_name"; }
  function collect_args() { helm_args+=(collect_args); }

  run deploy_template
  assert_success
  assert_output "info helm upgrade deployment_name helm_chart_dir --atomic --reset-values --wait --install collect_args"
}

@test "$(basename ${BATS_TEST_FILENAME})#deploy_template deploys the helm template and prints the resulting yaml if DEBUG_DEPLOY is set" {
  source ${profile_script}

  export DEBUG_DEPLOY=1

  function helm() { echo "helm ${*}" >&2; }
  function helm_chart_dir() { echo "helm_chart_dir"; }
  function deployment_name() { echo "deployment_name"; }
  function collect_args() { helm_args+=(collect_args); }

  run deploy_template
  assert_success
  assert_output "helm upgrade deployment_name helm_chart_dir --atomic --reset-values --wait --install --debug collect_args"
}

@test "$(basename ${BATS_TEST_FILENAME})#deploy_template deploys the helm template without printing the resulting yaml if DEBUG_DEPLOY is not set" {
  source ${profile_script}

  function helm() { echo "helm ${*}" >&2; }
  function helm_chart_dir() { echo "helm_chart_dir"; }
  function deployment_name() { echo "deployment_name"; }
  function collect_args() { helm_args+=(collect_args); }

  run deploy_template
  assert_success
  assert_output "helm upgrade deployment_name helm_chart_dir --atomic --reset-values --wait --install collect_args"
}

@test "$(basename ${BATS_TEST_FILENAME})#deploy_template fails if helm upgrade fails" {
  source ${profile_script}

  function helm() { echo "helm ${*}" >&2; return 1; }
  function helm_chart_dir() { echo "helm_chart_dir"; }
  function deployment_name() { echo "deployment_name"; }
  function collect_args() { helm_args+=(collect_args); }

  run deploy_template
  assert_failure
  assert_output "helm upgrade deployment_name helm_chart_dir --atomic --reset-values --wait --install collect_args"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main checks deployment requirements, logs into the cluster, lints, prints, and deploys the template, watches and describes the deployment" {
  source ${profile_script}

  function check_helm_deployment_requirements() { echo "check_helm_deployment_requirements called" >&2; }
  function cluster_login() { echo "cluster_login" >&2; }
  function lint_template { echo "lint_template" >&2; }
  function print_template { echo "print_template" >&2; }
  function deploy_template { echo "deploy_template" >&2; }
  function watch_deployment { echo "watch_deployment" >&2; }

  run run_main
  assert_success
  assert_output -p "check_helm_deployment_requirements called"
  assert_output -p "cluster_login"
  assert_output -p "lint_template"
  assert_output -p "print_template"
  assert_output -p "deploy_template"
  assert_output -p "watch_deployment"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if check_helm_deployment_requirements fails" {
  source ${profile_script}

  function check_helm_deployment_requirements() { echo "check_helm_deployment_requirements called" >&2; return 1;}
  function cluster_login() { echo "cluster_login" >&2; }
  function lint_template { echo "lint_template" >&2; }
  function print_template { echo "print_template" >&2; }
  function deploy_template { echo "deploy_template" >&2; }
  function watch_deployment { echo "watch_deployment" >&2; }

  run run_main
  assert_failure
  assert_output -p "check_helm_deployment_requirements called"
  refute_output -p "cluster_login"
  refute_output -p "lint_template"
  refute_output -p "print_template"
  refute_output -p "deploy_template"
  refute_output -p "watch_deployment"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if cluster_login fails" {
  source ${profile_script}

  function check_helm_deployment_requirements() { echo "check_helm_deployment_requirements called" >&2;}
  function cluster_login() { echo "cluster_login" >&2; return 1; }
  function lint_template { echo "lint_template" >&2; }
  function print_template { echo "print_template" >&2; }
  function deploy_template { echo "deploy_template" >&2; }
  function watch_deployment { echo "watch_deployment" >&2; }

  run run_main
  assert_failure
  assert_output -p "check_helm_deployment_requirements called"
  assert_output -p "cluster_login"
  refute_output -p "lint_template"
  refute_output -p "print_template"
  refute_output -p "deploy_template"
  refute_output -p "watch_deployment"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if lint_template fails" {
  source ${profile_script}

  function check_helm_deployment_requirements() { echo "check_helm_deployment_requirements called" >&2;}
  function cluster_login() { echo "cluster_login" >&2; }
  function lint_template { echo "lint_template" >&2; return 1; }
  function print_template { echo "print_template" >&2; }
  function deploy_template { echo "deploy_template" >&2; }
  function watch_deployment { echo "watch_deployment" >&2; }

  run run_main
  assert_failure
  assert_output -p "check_helm_deployment_requirements called"
  assert_output -p "cluster_login"
  assert_output -p "lint_template"
  refute_output -p "print_template"
  refute_output -p "deploy_template"
  refute_output -p "watch_deployment"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if print_template fails" {
  source ${profile_script}

  function check_helm_deployment_requirements() { echo "check_helm_deployment_requirements called" >&2;}
  function cluster_login() { echo "cluster_login" >&2; }
  function lint_template { echo "lint_template" >&2; }
  function print_template { echo "print_template" >&2; return 1; }
  function deploy_template { echo "deploy_template" >&2; }
  function watch_deployment { echo "watch_deployment" >&2; }

  run run_main
  assert_failure
  assert_output -p "check_helm_deployment_requirements called"
  assert_output -p "cluster_login"
  assert_output -p "lint_template"
  assert_output -p "print_template"
  refute_output -p "deploy_template"
  refute_output -p "watch_deployment"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if deploy_template fails" {
  source ${profile_script}

  function check_helm_deployment_requirements() { echo "check_helm_deployment_requirements called" >&2;}
  function cluster_login() { echo "cluster_login" >&2; }
  function lint_template { echo "lint_template" >&2; }
  function print_template { echo "print_template" >&2; }
  function deploy_template { echo "deploy_template" >&2; return 1; }
  function watch_deployment { echo "watch_deployment" >&2; }

  run run_main
  assert_failure
  assert_output -p "check_helm_deployment_requirements called"
  assert_output -p "cluster_login"
  assert_output -p "lint_template"
  assert_output -p "print_template"
  assert_output -p "deploy_template"
  refute_output -p "watch_deployment"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if watch_deployment fails" {
  source ${profile_script}

  function check_helm_deployment_requirements() { echo "check_helm_deployment_requirements called" >&2;}
  function cluster_login() { echo "cluster_login" >&2; }
  function lint_template { echo "lint_template" >&2; }
  function print_template { echo "print_template" >&2; }
  function deploy_template { echo "deploy_template" >&2; }
  function watch_deployment { echo "watch_deployment" >&2; return 1; }

  run run_main
  assert_failure
  assert_output -p "check_helm_deployment_requirements called"
  assert_output -p "cluster_login"
  assert_output -p "lint_template"
  assert_output -p "print_template"
  assert_output -p "deploy_template"
  assert_output -p "watch_deployment"
}
