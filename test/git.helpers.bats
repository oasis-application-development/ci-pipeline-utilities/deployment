#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/lib/helpers/git"

setup() {
  source "/usr/local/lib/helpers/common"
  export LOG_LEVEL="info"
}

setup_clone_environment() {
  export CI_API_V4_URL=CI_API_V4_URL
  export CI_PROJECT_ID=CI_PROJECT_ID
  export ORBS_BOT_NETID=ORBS_BOT_NETID
  export ORBS_BOT_TOKEN=ORBS_BOT_TOKEN
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_gitlab_environment fails if check_required_environment fails" {
  source ${profile_script}
  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }

  run check_required_gitlab_environment
  assert_failure
  assert_output -p "check_required_environment CI_API_V4_URL CI_PROJECT_ID ORBS_BOT_NETID ORBS_BOT_TOKEN"
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_gitlab_environment succeeds if required environment is present" {
  source ${profile_script}
  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_required_gitlab_environment
  assert_success
  assert_output -p "check_required_environment CI_API_V4_URL CI_PROJECT_ID ORBS_BOT_NETID ORBS_BOT_TOKEN"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_repo_url fails if web_url is not present' {
  setup_clone_environment
  source ${profile_script}
  
  function curl() { echo "curl ${*}" >&2; echo '{}'; }

  run git_repo_url
  assert_failure
  assert_output -p "curl -H Authorization: Bearer ${ORBS_BOT_TOKEN} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}"
  assert_output -p "could not get repo_url from gitlab for project ${CI_PROJECT_ID}"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_repo_url returns web_url if present' {
  setup_clone_environment
  source ${profile_script}
  
  function curl() { echo '{"web_url":"http://git_repo_url.com"}'; }

  run git_repo_url
  assert_success
  assert_output "http://git_repo_url.com"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_authenticate fails if git_repo_url fails" {
  export ORBS_BOT_NETID=ORBS_BOT_NETID
  export ORBS_BOT_TOKEN=ORBS_BOT_TOKEN
  source ${profile_script}

  function git_repo_url() { echo "git_repo_url called" >&2; return 1; }

  run git_authenticate
  assert_failure
  assert_output -p "git_repo_url called"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_authenticate returns git_repo_url with basic authentication if not already present" {
  export ORBS_BOT_NETID=ORBS_BOT_NETID
  export ORBS_BOT_TOKEN=ORBS_BOT_TOKEN
  source ${profile_script}

  function git_repo_url() { echo "https://gitlab.dhe.duke.edu"; }

  run git_authenticate
  assert_success
  assert_output "https://${ORBS_BOT_NETID}:${ORBS_BOT_TOKEN}@gitlab.dhe.duke.edu" 
}

@test "$(basename ${BATS_TEST_FILENAME})#git_clone fails if check_required_gitlab_environment fails" {
  export ORBS_BOT_NETID=ORBS_BOT_NETID
  export ORBS_BOT_TOKEN=ORBS_BOT_TOKEN
  source ${profile_script}

  function check_required_gitlab_environment() { echo "check_required_gitlab_environment called" >&2; return 1; }
  function git_authenticate() { echo "git_authenticate ${*}" >&2; }
  function git() { echo "git ${*}" >&2; }

  run git_clone "https://gitlab.dhe.duke.edu/project"
  assert_failure
  assert_output -p "check_required_gitlab_environment called"
  refute_output -p "git_authenticate https://gitlab.dhe.duke.edu/project"
  refute_output -p "git clone https://${ORBS_BOT_NETID}:${ORBS_BOT_TOKEN}@gitlab.dhe.duke.edu/project"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_clone fails if git_authenticate fails" {
  export ORBS_BOT_NETID=ORBS_BOT_NETID
  export ORBS_BOT_TOKEN=ORBS_BOT_TOKEN
  source ${profile_script}

  function check_required_gitlab_environment() { echo "check_required_gitlab_environment called" >&2; }
  function git_authenticate() { echo "git_authenticate ${*}" >&2; return 1; }
  function git() { echo "git ${*}" >&2; }

  run git_clone "https://gitlab.dhe.duke.edu/project"
  assert_failure
  assert_output -p "check_required_gitlab_environment called"
  assert_output -p "git_authenticate https://gitlab.dhe.duke.edu/project"
  refute_output -p "git clone https://${ORBS_BOT_NETID}:${ORBS_BOT_TOKEN}@gitlab.dhe.duke.edu/project"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_clone fails if git clone fails" {
  export ORBS_BOT_NETID=ORBS_BOT_NETID
  export ORBS_BOT_TOKEN=ORBS_BOT_TOKEN
  source ${profile_script}

  function check_required_gitlab_environment() { echo "check_required_gitlab_environment called" >&2; }
  function git_authenticate() { echo "git_authenticate ${*}" >&2; echo "https://${ORBS_BOT_NETID}:${ORBS_BOT_TOKEN}@gitlab.dhe.duke.edu/project"; }
  function git() { echo "git ${*}" >&2; return 1; }

  run git_clone "https://gitlab.dhe.duke.edu/project"
  assert_failure
  assert_output -p "check_required_gitlab_environment called"
  assert_output -p "git_authenticate https://gitlab.dhe.duke.edu/project"
  assert_output -p "git clone https://${ORBS_BOT_NETID}:${ORBS_BOT_TOKEN}@gitlab.dhe.duke.edu/project"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_clone calls git_authenticate and clones" {
  export ORBS_BOT_NETID=ORBS_BOT_NETID
  export ORBS_BOT_TOKEN=ORBS_BOT_TOKEN
  source ${profile_script}

  function check_required_gitlab_environment() { echo "check_required_gitlab_environment called" >&2; }
  function git_authenticate() { echo "git_authenticate ${*}" >&2;  echo "https://${ORBS_BOT_NETID}:${ORBS_BOT_TOKEN}@gitlab.dhe.duke.edu/project"; }
  function git() { echo "git ${*}" >&2; }

  run git_clone "https://gitlab.dhe.duke.edu/project"
  assert_success
  assert_output -p "check_required_gitlab_environment called"
  assert_output -p "git_authenticate https://gitlab.dhe.duke.edu/project"
  assert_output -p "git clone https://${ORBS_BOT_NETID}:${ORBS_BOT_TOKEN}@gitlab.dhe.duke.edu/project"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_new_branch fails if name is not supplied" {
  source ${profile_script}

  run git_new_branch
  assert_failure
  assert_output -p "name required"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_new_branch fails if git fails" {
  source ${profile_script}

  function git() { echo "git ${*}" >&2; return 1; }

  run git_new_branch test-branch-name
  assert_failure
  assert_output -p "git switch -c test-branch-name"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_new_branch uses git to create new branch" {
  source ${profile_script}

  function git() { echo "git ${*}" >&2; }

  run git_new_branch test-branch-name
  assert_success
  assert_output -p "git switch -c test-branch-name"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_set_user sets global git user" {
  export ORBS_BOT_NETID=ORBS_BOT_NETID
  export ORBS_BOT_TOKEN=ORBS_BOT_TOKEN
  source ${profile_script}

  function git() { echo "git ${*}" >&2; }

  run git_set_user
  assert_success
  assert_output -p "git config --global user.email ${ORBS_BOT_NETID}@duke.edu"
  assert_output -p "git config --global user.name ${ORBS_BOT_NETID}"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_commit fails if message not provided" {
  source ${profile_script}

  run git_commit
  assert_failure
  assert_output -p "message required"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_commit fails if git_set_user fails" {
  source ${profile_script}

  function git_set_user() { echo "git_set_user called" >&2; return 1; }
  function git() { echo "git ${*}" >&2; }  

  run git_commit test-message
  assert_failure
  assert_output -p "git_set_user called"
  refute_output -p "git commit -m test-message"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_commit fails if git fails" {
  source ${profile_script}

  function git_set_user() { echo "git_set_user called" >&2; }
  function git() { echo "git ${*}" >&2; return 1; } 

  run git_commit test-message
  assert_failure
  assert_output -p "git_set_user called"
  assert_output -p "git commit -m test-message"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_commit commits with message" {
  source ${profile_script}

  function git_set_user() { echo "git_set_user called" >&2; }
  function git() { echo "git ${*}" >&2; } 

  run git_commit test-message
  assert_success
  assert_output -p "git_set_user called"
  assert_output -p "git commit -m test-message"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_push fails if name not supplied" {
  source ${profile_script}

  run git_push
  assert_failure
  assert_output -p "name required"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_push fails if git fails" {
  source ${profile_script}

  function git() { echo "git ${*}" >&2; return 1; }

  run git_push test-name
  assert_failure
  assert_output -p "git push --set-upstream origin test-name"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_push pushes named branch" {
  source ${profile_script}  

  function git() { echo "git ${*}" >&2; }

  run git_push test-name
  assert_success
  assert_output -p "git push --set-upstream origin test-name"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_merge_request fails if source_branch is not provided" {
  source ${profile_script}

  run git_merge_request test-project-id
  assert_failure
  assert_output -p "git_merge_request [source_branch] [target_branch] [title] [description]"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_merge_request fails if target_branch is not provided" {
  source ${profile_script}
  
  run git_merge_request test-project-id test-source-branch
  assert_failure
  assert_output -p "git_merge_request [source_branch] [target_branch] [title] [description]"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_merge_request fails if title is not provided" {
  source ${profile_script}

  run git_merge_request test-project-id test-source-branch test-target-branch
  assert_failure
  assert_output -p "git_merge_request [source_branch] [target_branch] [title] [description]"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_merge_request fails if description is not provided" {
  source ${profile_script}
  
  run git_merge_request test-project-id test-source-branch test-target-branch test-title
  assert_failure
  assert_output -p "git_merge_request [source_branch] [target_branch] [title] [description]"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_merge_request fails if check_required_gitlab_environment fails" {
  source ${profile_script}

  function check_required_gitlab_environment() { echo "check_required_gitlab_environment called" >&2; return 1; }

  run git_merge_request test-project-id test-source-branch test-target-branch test-title test-description
  assert_failure
  assert_output -p "check_required_gitlab_environment called"
}

@test "$(basename ${BATS_TEST_FILENAME})#git_merge_request fails if response does not have a web_url" {
  source ${profile_script}
  
  function check_required_gitlab_environment() { echo "check_required_gitlab_environment called" >&2; }
  function curl() { echo "curl ${*}" >&2; echo '{"message":"unauthorized"}'; }

  run git_merge_request test-project-id test-source-branch test-target-branch test-title test-description
  assert_failure
  assert_output -p "check_required_gitlab_environment called"
  assert_output -p '{"message":"unauthorized"}'
}

@test "$(basename ${BATS_TEST_FILENAME})#git_merge_request posts merge_request and prints web_url" {
  source ${profile_script}
  
  function check_required_gitlab_environment() { echo "check_required_gitlab_environment called" >&2; }
  function curl() { echo '{"web_url":"https://url/merge_request/2"}'; }

  run git_merge_request test-project-id test-source-branch test-target-branch test-title test-description
  assert_success
  assert_output -p "check_required_gitlab_environment called"
  assert_output -p 'https://url/merge_request/2'
}
