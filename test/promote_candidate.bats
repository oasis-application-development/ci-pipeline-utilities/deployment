#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/bin/promote_candidate"

setup() {
  export LOG_LEVEL="info"
  export CANDIDATE_IMAGE="CANDIDATE_IMAGE"
  export PROMOTION_IMAGE="PROMOTION_IMAGE"
  export CI_REGISTRY_IMAGE="CI_REGISTRY_IMAGE"
  export CI_JOB_TOKEN="CI_JOB_TOKEN"
}

@test "$(basename ${BATS_TEST_FILENAME}) tag_image takes candidate and promotion image and uses skopeo to tag CANDIDATE_IMAGE to PROMOTION_IMAGE" {
  source ${profile_script}

  local expected_image_user='gitlab-ci-token'
  local expected_candidate_image="expected_candidate_image"
  local expected_promoted_image="expected_promoted_image"
  function skopeo() { echo "skopeo ${*}" >&2; }

  run tag_image "${expected_candidate_image}" "${expected_promoted_image}"
  assert_success
  assert_output -p "skopeo copy --src-creds=${expected_image_user}:${CI_JOB_TOKEN} --dest-creds=${expected_image_user}:${CI_JOB_TOKEN} docker://${expected_candidate_image} docker://${expected_promoted_image}"
}

@test "$(basename ${BATS_TEST_FILENAME}) tag_image uses skopeo to tag CANDIDATE_IMAGE to PROMOTION_IMAGE with IMAGE_USER_OVERRIDE if present" {
  source ${profile_script}

  export IMAGE_USER_OVERRIDE='IMAGE_USER_OVERRIDE'
  local expected_candidate_image="expected_candidate_image"
  local expected_promoted_image="expected_promoted_image"
  function skopeo() { echo "skopeo ${*}" >&2; }

  run tag_image "${expected_candidate_image}" "${expected_promoted_image}"
  assert_success
  assert_output -p "skopeo copy --src-creds=${IMAGE_USER_OVERRIDE}:${CI_JOB_TOKEN} --dest-creds=${IMAGE_USER_OVERRIDE}:${CI_JOB_TOKEN} docker://${expected_candidate_image} docker://${expected_promoted_image}"
}

@test "$(basename ${BATS_TEST_FILENAME}) tag_image fails if candidate_image not supplied" {
  source ${profile_script}

  run tag_image
  assert_failure
}

@test "$(basename ${BATS_TEST_FILENAME}) tag_image fails if promoted_image not supplied" {
  source ${profile_script}

  run tag_image "candidate_image"
  assert_failure
}

@test "$(basename ${BATS_TEST_FILENAME}) tag_image fails if skopeo fails" {
  source ${profile_script}

  local expected_image_user='gitlab-ci-token'
  local expected_candidate_image="expected_candidate_image"
  local expected_promoted_image="expected_promoted_image"
  function skopeo() { echo "skopeo ${*}" >&2; return 1; }

  run tag_image "${expected_candidate_image}" "${expected_promoted_image}"
  assert_failure
  assert_output -p "skopeo copy --src-creds=${expected_image_user}:${CI_JOB_TOKEN} --dest-creds=${expected_image_user}:${CI_JOB_TOKEN} docker://${expected_candidate_image} docker://${expected_promoted_image}"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main runs check_required_environment and tag_image" {
  source ${profile_script}

  local expected_environment_variables="CI_REGISTRY_IMAGE CANDIDATE_IMAGE PROMOTION_IMAGE CI_JOB_TOKEN"
  local expected_candidate_image="${CI_REGISTRY_IMAGE}/${CANDIDATE_IMAGE}"
  local expected_promoted_image="${CI_REGISTRY_IMAGE}/${PROMOTION_IMAGE}"
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  function tag_image() { echo "tag_image ${*} called " >&2; }

  run run_main
  assert_success
  assert_output -p "check_required_environment ${expected_environment_variables} called"
  assert_output -p "tag_image ${expected_candidate_image} ${expected_promoted_image} called"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if check_required_environment fails" {
  source ${profile_script}

  local expected_environment_variables="CI_REGISTRY_IMAGE CANDIDATE_IMAGE PROMOTION_IMAGE CI_JOB_TOKEN"
  local expected_candidate_image="${CI_REGISTRY_IMAGE}/${CANDIDATE_IMAGE}"
  local expected_promoted_image="${CI_REGISTRY_IMAGE}/${PROMOTION_IMAGE}"

  function check_required_environment() { echo "check_required_environment ${*} called" >&2; return 1; }
  function tag_image() { echo "tag_image ${*} called " >&2; }

  run run_main
  assert_failure
  assert_output -p "check_required_environment ${expected_environment_variables} called"
  refute_output -p "tag_image ${expected_candidate_image} ${expected_promoted_image} called"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if tag_image fails" {
  source ${profile_script}

  local expected_environment_variables="CI_REGISTRY_IMAGE CANDIDATE_IMAGE PROMOTION_IMAGE CI_JOB_TOKEN"
  local expected_candidate_image="${CI_REGISTRY_IMAGE}/${CANDIDATE_IMAGE}"
  local expected_promoted_image="${CI_REGISTRY_IMAGE}/${PROMOTION_IMAGE}"

  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  function tag_image() { echo "tag_image ${*} called " >&2; return 1; }

  run run_main
  assert_failure
  assert_output -p "check_required_environment ${expected_environment_variables} called"
  assert_output -p "tag_image ${expected_candidate_image} ${expected_promoted_image} called"
}
