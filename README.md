# deployment

Utility image and gitlab ci extensions to be used in gitlab ci to
deploy an application to a kubernetes cluster, or a helm chart to a helm
chart repository.

Image Version: v3.4.7

[[_TOC_]]

## General Usage

This utility facilitates the following deployment activities:

#### Certificate Generation with cfssl
[cfssl](https://github.com/cloudflare/cfssl) is installed, which can be used to generate
certificates, ca certificates, etc.

#### Ansible
[ansible](https://www.ansible.com/) is installed, and can be used to automate many tasks within a CI
context. These should not require `become`, as the ci jobs do not run with a root user. They can
generate files/directories targetted as CI job artifacts, such as Certs (see above).

#### aws cli operations
Amazon Web Services CLI [aws](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html) is installed and can be used to automate interactions with aws, such as s3 operations.

### gitlab-ci.yml generation
bin/generate_gitlab_ci_yml.py can be run to generate a new .gitlab-ci.yml file, or update an
existing one, in a project. This is best run using the deployment image (see below), and a
volume mount to your project. You will need to install oyaml on your machine for it to work
outside of the container.

This is fully atomic. If sections that it wants to generate, or update, already exist and do not
need update, nothing is changed.

supports the following options:
- -b: adds the stage, include, and build stage to use the kaniko_builder to build your Dockerfile
- -d name: adds a deploy stage, include for this repository, a deploy_{name} job that deploys the image using this deployment image, and a decommission_name job that decommissions the deployment with helm.
  - if you run this multiple times with different versions of this deployment image, the ref
    for the include will be changed
  - if you add -s or --skip-decom, the decommission_job is not generated

### Merge Bot Utilities

lib/helpers/git contains the following functions that can be used by other systems to augment code in a project using merge requests:

#### check_required_gitlab_environment
This method is called by other git methods, but can also be called as a fast failing sanity check. returns 1 (nonzero) if any of the following environment variables are missing:
- GITLAB_API_V4_URL: url to gitlab, with /api/v4 appended. Supplied by gitlab when run as a gitlab CI job
- CI_PROJECT_ID: either the numeric id of the project, or [the urlencoded path to the project](https://docs.gitlab.com/ee/api/index.html#namespaced-path-encoding)
- ORBS_BOT_NETID: netid for the service account being used. Service account must have Developer or greater access to the project, and be able to login to the gitlab being accessed (e.g. have kerberos attributes in IDMS profile)
- ORBS_BOT_TOKEN: [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) for ORBS_BOT_NETID

#### git_repo_url
Uses curl and jq to get the web_url for the CI_PROJECT_ID. Fails if the response from
curl does not contain a web_url, such as for an access denial. Called by git_authenticate.

#### git_authenticate
calls git_repo_url, and adds basic authentication using ORBS_BOT_NETID and ORBS_BOT_TOKEN
so that it can be used to make authenticated calls to gitlab. Fails if git_repo_url fails.

#### git_clone
calls check_required_gitlab_environment, and then clones the git_authenticate amended git_repo_url to the present working directory. Fails if check_required_gitlab_environment or
git_authenticate fails.

#### git_new_branch
takes a required name argument, and creates a new branch with that name in the current repo.
fails if a name is not required, or git fails.

#### git_set_user
sets the global git user.email and user.name in the running environment. fails if git fails

#### git_commit
Takes a message, sets_user info, then creates a commit in the current repo using the
message. Fails if message is not supplied, set_user_info fails, or git fails.

#### git_push
Takes a required branch name, and pushes that branch to gitlab, setting the upstream to
origin in the process. Fails if name is not supplied, or git fails.

#### git_merge_request
Takes required aruguments (see below), calls check_required_gitlab_environment, then
attempts to create a merge request in the project. Returns the merge request web_url if
successful. Fails if required arguments are missing, check_required_gitlab_environment fails, or the
request to create the merge request does not return a web_url, such as for access denial.

required arguments:
- source_branch: name of source branch for merge request. Most likely the same as passed to git_new_branch and git_push
- target_branch: name of target branch for merge request
- title: brief title of merge request
- description: description of merge request

## Cluster Login Environment

Many utilities below require the following environment in order to authenticate to the
cluster:

- HELM_USER: This is the user associated with HELM_TOKEN, typically
helm-deployer. This is not actually used for authentication, but it is used to store
the user in the kube config on your machine if you use this system locally. Required.
- PROJECT_NAMESPACE: namespace of the kubernetes project. Required.
- HELM_TOKEN: This is a token used to interact with the kubernetes cluster using its API within the PROJECT_NAMESPACE. The HELM_TOKEN must belong to a service account
with the correct admin RBAC role in PROJECT_NAMESPACE. DHTS will typically provide you with an appropriate HELM_TOKEN. Required.
- CLUSTER_SERVER: The URL to the Kubernetes API. Required.
- CLUSTER_NAME: optional. Sets the kubectl cluster name in the local .kube/config. Default ci_kube
- CONTEXT_NAME: optional. Sets the kubectl context name in the local .kube/config. Default $HELM_USER-$CLUSTER_NAME-$PROJECT_NAMESPACE

### Helm Deployment and Decommission
This activity facilitates the automated deployment and decommissioning
of an application to a kubernetes cluster using [helm](https://helm.sh/).

The utility is designed to run in a [gitlab ci environment](https://docs.gitlab.com/ee/ci/environments.html). The gitlab ci environment provides a
number of useful benefits, but most importantly, a set of [predefined environment variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) that are used in the deployment.

#### Helm Deployment ENVIRONMENT

This utility must have certain environment variables defined when it
runs. Some of these will be supplied by gitlab ci, but the following
must be created in the gitlab CI/CD Environment Variables:

- Cluster Login Environment (see above)
- HELM_CHART_NAME: Should match the name of the chart in helm-chart (see below). 
This is also used in naming many kubernetes objects and dns subdomain names. For
this reason, there is a [limited set of characters](https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#dns-label-names) that can be used.
- CONTAINER_ENV: Optional. Can be set to a space separated list of other environment variables which must be present in the gitlab ci environment variables. These will
be passed as --set-string containerEnv.$KEY=$VALUE (see below).
- APPLICATION_SPECIFIC_ENVIRONMENT: Optional, Can set to a space seperated list
of environment variables which must be present in the gitlab ci environment
variables. These will be passed as --set-string $KEY=$VALUE (see below).
- POD_METADATA_ENVIRONMENT: Optional, Can set to a space seperated list
of environment variables which must be present in the gitlab ci environment
variables. These will be passed as --set-string meta.KEY=VALUE (see below).
**NOTE** your values.yaml MUST define a meta: dictionary section, e.g.
```bash
meta: {}
```
or

```bash
meta:
  someKey: ''
```

- WANTS_DATABASE: if not set, and this application does not expect any 
database environment settings.
- tls__key, tls__certificate, and tls__cacertificate: optional. If tls__key is set,
tls__certificate must also be set. tls__cacertificate is optional. All of these must be set to
a file on the file system (e.g. in Gitlab these must be set as [File Variables](https://docs.gitlab.com/ee/ci/variables/#cicd-variable-types)). See tls_settings below for information on using these in your helm chart.

#### Helm Chart

The utility requires the existence of a [helm chart](https://helm.sh/docs/developing_charts/) in the repo,
located in `helm-chart/$HELM_CHART_NAME` relative to the root of the repo.

The chart can deploy anything that is required to host an application
in kubernetes. Typically, this will include a Deployment, Service,
and Route. It will most likely also include Secrets and/or ConfigMaps.
Of special note is how the CI Environment variables are translated to
helm variables. `bin/deploy` is designed to set these values in a very
opinionated way. Each application helm chart can use these or ignore them as
applicable. These variables fall into the following categories:

##### Object Labels

Kubernetes objects, including Deployments, Services, ConfigMaps, Pods/Pod Templates,
etc., should include the following recommended labels to extend .deploy, or use
functions in `bin/deploy` and the `lib/helpers/deploy` helper library:

- app.kubernetes.io/instance: This must be set to {{ .Release.Name }} if
POD_LABEL_QUERY is not set. `bin/deploy` will set .Release.Name with the value
returned from deployment_name (see below). If you do not set this, you
must define a POD_LABEL_QUERY environment variable that describes the label or labels
to use to query pods after the deployment. This must be in the form lablename=expected 
value, and will be passed to kubectl get pods -l $POD_LABEL_QUERY.
POD_LABEL_QUERY can include other environment variables, which will be
interpolated into the label used to get the pods.

- environment: set to {{ .Values.environment }}, which `bin/deploy` sets to
$CI_ENVIRONMENT_SLUG.

- git_ref: set to {{ .Values.git_ref }}, which `bin/deploy` sets to $CI_COMMIT_REF_SLUG.

- git_commit: set to {{ .Values.git_commit }}, which `bin/deploy` sets to
$CI_COMMIT_SHORT_SHA.

##### image_settings 
Requires either DEPLOYMENT_IMAGE set to a full image registry url, including tag
unless latest is assumed, or CI_REGISTRY_IMAGE, CI_PROJECT_NAME, and CI_COMMIT_SHORT_SHA, 
which are all provided by gitlab ci by default.

If DEPLOYMENT_IMAGE is set, it is passed as `--set-string` argument:
- image.full=$DEPLOYMENT_IMAGE

If DEPLOYMENT_IMAGE is not set CI_REGISTRY_IMAGE, CI_PROJECT_NAME, and CI_COMMIT_SHORT_SHA are passed to your helm chart as `--set-string` arguments:
- image.repository=$CI_REGISTRY_IMAGE/$CI_PROJECT_NAME
- image.tag=$CI_COMMIT_SHORT_SHA

Your helm chart should use these to
create the image used in the
Deployment pod template container definition.

##### image_pull_secret_settings
Not required or used if your gitlab project is public. If your gitlab project is
private or internal, you must create s [gitlab deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/#gitlab-deploy-token)
which gitlab ci will automatically use in the CI_DEPLOY_USER and CI_DEPLOY_PASSWORD 
environment variables in the pipeline. These are used in `--set-string` arguments
to the helm chart:
- registry.root=$CI_REGISTRY
- registry.secret.username=$CI_DEPLOY_USER
- registry.secret.password=$CI_DEPLOY_PASSWORD

Your helm chart should use these to create a dockerconfig type Kubernetes Secret.
See example helm charts for how this should be done.

##### deployment_name
Used by `bin/deploy` to set the Release.Name of the helm deployment.
Until [this issue](https://github.com/helm/helm/pull/8384) is merged, this must
be <= 53 characters in length. It must also be unique within the PROJECT_NAMESPACE.

This can either be:
- a DEPLOYMENT_NAME environment variable in the gitlab ci environment variables
(job, pipeline, repo, or group). If this is greater than 53 characters, or is not
unique, the deployment will fail.
- $CI_ENVIRONMENT_NAME-$HELM_CHART_NAME using variables in the gitlab ci
environment (job, pipeline, repo, or group). If this is <= 53, and is not unique,
the deployment will fail. You should set a unique CI_ENVIRONMENT_NAME. If this is
greater than 53 a warning will be printed in the job output, and you will end up
with a release name truncated by truncating the CI_ENVIRONMENT_NAME and adding a
short string that guarantees uniqueness.

##### deployment_environment_settings
Uses the gitlab ci provided CI_ENVIRONMENT_URL and CI_ENVIRONMENT_SLUG to produce
the following `--set-string` arguments:
- url=$CI_ENVIRONMENT_URL
- environment=$CI_ENVIRONMENT_SLUG

if a job that extends deploy sets the SKIP_INGRESS to a value, the CI_ENVIRONMENT_URL variable is not
required, and the url --set-string argument will not be passed to helm deploy. If it is not set, it is
required and set.

Your helm template can can use the url in its Route.host, but it should be prepared
to remove the `https://` url prefix.

##### repository_metadata_settings
Uses the gitlab ci provided CI_COMMIT_SHORT_SHA, and CI_COMMIT_REF_SLUG
environment variables to pass the following `--set-string` arguments to helm:
- git_commit=$CI_COMMIT_SHORT_SHA
- git_ref=$CI_COMMIT_REF_SLUG

Your templates should use these in the metadata.labels for your kubernetes objects.
Here is an example of how they might be used:

```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "fullname" . }}
  labels:
    git_commit: "{{ .Values.git_commit }}"
    git_ref: "{{ .Values.git_ref }}"
```

##### database_environment_settings
If your deployment has set the WANTS_DATABASE environment variable, this requires
either:

- DATABASE_URL, which will be passed as `--set-string database_url=$DATABASE_URL`

or the following environment variables:
- DB__NAME: name of the db expected by the application
- DB__USER: user of the DB__NAME db
- DB__PASSWORD: password for DB__USER
- DB__SERVER: server hosting the db

to produce the following `--set-string` arguments to helm:
- db.name=$DB__NAME
- db.user=$DB__USER
- db.password=$DB__PASSWORD
- db.server=$DB__SERVER

It can also take the following optional variables:
- DB_IMAGE: if present, causes production of a `--set-string` db.image=$DB_IMAGE
- PERSIST_DB: if present, causes the production of a `--set-string` db.persistent=1 argument. Your helm chart can use this to determine if a kubernetes PVC should be
created for the database.
- SEED_MOCK_DATA: if present, causes the production of a `--set-string` db.seedMockData=1 
argument. This may be passed to the application config or secret environment used
during db seeding.

Your helm chart can use the above to create kubernetes deployments and services
for a database in the cluster. This should only be used for development like
environments, such as review applications.

If WANTS_DATABASE is not set, then it does not pass any --set-string db arguments
to your helm chart.

##### container_env_settings
These allow for application container specific environment variables to be passed
to a helm chart as a series of containerEnv arguments. If CONTAINER_ENV is set to
a space separated list of environment variables, each environment variable listed
is required to be present in the environemnt, and will be passed as `--set-string` 
containerEnv.NAME=VALUE.

If OPTIONAL_CONTAINER_ENV is set to a space separated list of environment variables,
then these are passed as container.NAME=VALUE, if the environment
variable exists and is defined. These can be used with or without CONTAINER_ENV,
and all are merged into a single --set-string setting.

**NOTE** Values with spaces or commas in them will be escaped, e.g. "the,value" will
be printed as the\,value and "the value" will be printed as the\ value. 
If the value that you are storing for a CONTAINER_ENV or OPTIONAL_CONTAINER_ENV
environment variable has a `\`, then you must escape the slash in the value or it
will be removed before it is passed to containerEnv. For example, the value "oafx2c4\4" must be stored as "oafx2c4\\4" (even in the gitlab CI variables). This may be true
for other characters that are affected by shells during string interpolation.

Your helm chart can use a containerEnv hash to pass these on to your application
container, either as container environment, Secret, or Config environment mapping.

##### application_specific_settings
These allow for application specific environment variables to be passed
to a helm chart as a series of arguments. If APPLICATION_SPECIFIC_ENVIRONMENT
is set to a space separated list of environment variables, each environment
variable in the list is required to be present, and will be passed to the helm
deployment as
`--set-string $KEY1=$VALUE1,$KEY2=$VALUE2,...`.
In addition, if a KEY includes one or more double underscore `__` separating other strings, the double underscores are all turned into a dot, e.g.
`parent__child=foo` gets turned into `parent.child=foo`,
`grandparent__parent__child=foo` gets turned into `grandparent.parent.child`.

If OPTIONAL_APPLICATION_SPECIFIC_ENVIRONMENT is set to a space separated list of 
environment variables, then these are passed as the non optional are, if
the environment variable exists and is defined. These can be used with or without
APPLICATION_SPECIFIC_ENVIRONMENT, and all are merged into a single --set-string
setting. Supports underscore to dot conversion similar to 
APPLICATION_SPECIFIC_ENVIRONMENT.

**NOTE** Values with spaces or commas in them will be escaped, e.g. "the,value"
will be printed as the\,value and "the value" will be printed as the\ value.
If the value that you are storing for a APPLICATION_SPECIFIC_ENVIRONMENT
or OPTIONAL_APPLICATION_SPECIFI_ENVIRONMENT environment variable has a `\`, then
you must escape the slash in the value or it will be removed before it is passed
to --set-string. For example, the value "oafx2c4\4" must be stored as "oafx2c4\\4"
(even in the gitlab CI variables). This may be true for other characters that are
affected by shells during string interpolation.

Your helm chart can use these in any way needed.


##### pod_metadata_settings
These allow for pod metadata environment variables to be passed
to a helm chart as a series of arguments. If POD_METADATA_ENVIRONMENT
is set to a space separated list of environment variables, each environment
variable in the list is required to be present, and will be passed to the helm
deployment as
`--set-string meta.$KEY1=$VALUE1,$KEY2=$VALUE2,...`.
In addition, if a KEY includes one or more double underscore `__` separating other strings, the double underscores are all turned into a dot, e.g.
`parent__child=foo` gets turned into `meta.parent.child=foo`,
`grandparent__parent__child=foo` gets turned into `meta.grandparent.parent.child`.

If OPTIONAL_POD_METADATA_ENVIRONMENT is set to a space separated list of 
environment variables, then these are passed as the non optional are, if
the environment variable exists and is defined. These can be used with or without
POD_METADATA_ENVIRONMENT. Supports underscore to dot conversion similar to 
POD_METADATA_ENVIRONMENT.

**NOTE** Values with spaces or commas in them will be escaped, e.g. "the,value"
will be printed as the\,value and "the value" will be printed as the\ value.
If the value that you are storing for a POD_METADATA_ENVIRONMENT
or OPTIONAL_POD_METADATA_ENVIRONMENT environment variable has a `\`, then
you must escape the slash in the value or it will be removed before it is passed
to --set-string. For example, the value "oafx2c4\4" must be stored as "oafx2c4\\4"
(even in the gitlab CI variables). This may be true for other characters that are
affected by shells during string interpolation.

To use this, your values.yaml file MUST contain a meta: dictionary section, either
set to an empty dictionary:

```bash
meta: {}
```

or set with at least one key/value

```bash
meta:
  key1: ''
```

Your helm chart can use these in any way needed. A recommended way to use them is
to set these as pod labels in your Deployment/Statefulset/Job/etc. 
spec.template.metadata.labels. These can then be referenced using the [Kubernetes Downward API](https://kubernetes.io/docs/concepts/workloads/pods/downward-api/) as either [Environment Variables](https://kubernetes.io/docs/tasks/inject-data-application/environment-variable-expose-pod-information/#the-downward-api), or [Files](https://kubernetes.io/docs/tasks/inject-data-application/downward-api-volume-expose-pod-information/).

##### tls_settings

If your application needs to configure site tls needs to be configured to use a tls secret,
certificate, and, optionally, ca certificate, it can do so using the tls__key, 
tls__certificate, and tls__cacertificate environment variables, which must be set to paths on
the file system containing the respective data (see above). When these variables are present,
the deployment system will add the following to the arguments passed to helm lint, helm template, and helm install:

```bash
--set-file tls.key=$tls__key --set-file tls.certificate=$tls__certificate
```

if tls.cacertificate is pressent it will also add a similar --set-file for it.

These can be templated in a variety of ways:

- An Edge Terminated Openshift Route:

```bash
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  name: {{ include "fullname" . }}
spec:
  {{- if .Values.url }}
  host: {{ .Values.url | replace "https://" "" }}
  {{- end }}
  port:
    targetPort: http
  tls:
    insecureEdgeTerminationPolicy: Redirect
    termination: edge
    {{- if and .Values.tls.certificate .Values.tls.key }}
    key: |-
      {{ .Values.tls.key }}
    certificate: |-
      {{ .Values.tls.certificate }}
    {{- end }}
    {{- if .Values.tls.cacertificate }}
    caCertificate: |-
      {{ .Values.cacertificate}}
    {{- end }}
  to:
    kind: Service
    name: {{ include "fullname" . }}
    weight: 100
  wildcardPolicy: None
status:
  ingress:
    - wildcardPolicy: None
```

- A tls secret (does not support caCertificates)

```bash
apiVersion: v1
stringData:
  tls.crt: |-
    {{ .Values.tls.certificate }}
  tls.key: |-
    {{ .Values.tls.key }}
kind: Secret
metadata:
  creationTimestamp: null
  name: foo
type: kubernetes.io/tls
```

You can use this tls secret either in your [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/#tls), or directly mounted into your application containers where
it expects to find them.

##### printing chart information

By default, deployments will not print the kubernetes objects that are created for
the deployment. If you wish to see this information, such as when debugging a failed
load, you can set one or both of the PRINT_TEMPLATE or DEBUG_DEPLOY environment
variables to any value, e.g. 1.

**NOTE** this will print secrets, such as database user/passwords, and these will
be saved in the gitlab ci history, so this is not recommended.

##### Example Helm Charts
This repository contains a couple of example helm charts for ruby and nodejs
applications in example-helm-charts. Remember that the name of the chart in your
project must conform to the application chart naming convention.

### Publish Helm Chart to a Chart Repository

This utility facilitates the automated publication of a new version of a Helm Chart
in an application to a Chart Repository.

It requires a [helm chart](https://helm.sh/docs/developing_charts/) in the `helm-chart` 
subdirectory of the repository root, named for the hosted git project name (e.g.
$CI_PROJECT_NAME).

It will only publish if the helm-chart/$CI_PROJECT_NAME/Chart.yaml file has
changed in a given commit.

#### Publish Chart ENVIRONMENT

This utility must have certain environment variables defined when it runs. Some
of these will be supplied by gitlab ci, but the following must be created in the
gitlab CI/CD Environment Variables:

- CHART_MUSEUM_URL: full url to the chart repository, such as the DHTS
ChartMuseum instance.

### Promote Candidate Image

This utility facilitates the creation of a tagged `promotion` image from the 
`candidate` image within the project docker registry.

#### Promote Candidate Image ENVIRONMENT

This utility must have certain environment variables defined when it runs. Some
of these will be supplied by gitlab ci, but the following must be created,
typically within the gitlab-ci yaml Variables:

- CANDIDATE_IMAGE: image and tag in the
CI_REGISTRY_IMAGE registry to be promoted with a new tag
- PROMOTION_IMAGE: image and tag to be created in the
CI_REGISTRY_IMAGE registry from the CANDIDATE_IMAGE

### Clean Repo Image Tag

This utility allows a single tagged image in a designated image repository within
the gitlab project to be deleted.

#### Clean Repo Image Tag ENVIRONMENT

This utility must have certain environment variables defined when it runs. Some
of these will be supplied by gitlab ci, but the following must be created,
typically within the gitlab-ci yaml Variables:

- BOT_KEY: Personal Access Token for a User (Bot) with the Maintainer Role in the
Project
- DELETE_FROM_REPO: The Name of the registry repo within which the tag will be
deleted. This is the main name listed in the gitlab 'registries' page. It is
expanded to show the tags of the images within that repo
- DELETE_IMAGE_TAG: The tag of the image within the repo to be deleted

### Protect Service with Shibboleth

This utility facilates the creation of a [Duke-Shibboleth](https://gitlab.oit.duke.edu/duke_openshift_users/community-supported-resources/duke-shibboleth) proxy
in front of an application service. It will configure and deploy the proxy
service and create an externally accessible route (openshift) or
ingress (vanilla k8s) to the application service.

In the process of creating the configuration for the proxy, new self-signed
certificates (crt, csr, and key) are created by the process and stored in
$CI_PROJECT_DIR/shib_certificate. This utility saves this directory as a
[Job Artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) for 1 week. You must register the crt file, without the BEGIN and END blocks, in the
[OIT shibboleth endpoint management system](https://authentication.oit.duke.edu/manager/sites), after the deployment runs.

The utility is cloud native, in that it interfaces with the kubernetes system to
- find services that need to be protected
- find routes/ingresses that already exist for a protected service

Services must be annotated with the following labels or this utility will not
produce any shibboleth proxy deployments:

- duke.shibboleth.proxy.edu/service_should_be_protected: set to 'true' to label a service to be protected
- duke.shibboleth.proxy.edu/service_entity_id: the entity_id (without scheme) used
as the CN in the self-signed certificate. The registration for this service in
the OIT management system can use the scheme with this value as its entity_id.
- duke.shibboleth.proxy.edu/service_port: the port of the service to be protected.
Only one port per service can be protected.

Routes/Ingresses that this utility generates will be labled with duke.shibboleth.proxy.edu/protected_proxy: 'true'. If a route/ingress with this label named for the protected
service exists, this utility will skip it.

Your git repository must include a file `proxy.conf.tmpl` in the root. There are
examples in this repository for a plain proxy, and a proxy that further restricts
access to users in an OIT grouper/group manager group. You should copy 
and use proxy.conf.tmpl.plain as-is without modification to use it in your 
project. You must change the $group_name in the proxy.conf.tmpl.grouper to
a valid grouper group to use it in your project. Do not change any other part of
the file. If you use the grouper configuration, you must also copy attribute-map.grouper.xml to a file in the root of your project named attribute-map.xml.

#### Protect Service with Shibboleth ENVIRONMENT

This utility requires the following ENVIRONMENT variables:
- Cluster Login Environment (see above)
- ORG: this is the organization that is set in the self signed certificate.
It is probably Duke University or Duke Health Technology Solutions
- ORGUNIT: this is the organizational unit that is set in the self signed certificate
- EMAIL: the email registered into the self signed certificate
- CHART_MUSEUM_URL: The url to the DHTS chartmuseum

In addition you can set the following variables:
- SERVICE_APP: this value will set the metadata.app of the proxy deployment.
This can be used to ensure the proxy deployment is included in the same
application as the deployment for the service itself in the Openshift web UI.

### Decommission a Protected Service

This utility takes a PROTECTED_ENTITY_ID (see below), gets the service definition for that
service in the namespace, if it exists, and deletes the duke-shibboleth helm chart deployment for
just that PROTECTED_ENTITY_ID.

#### Decommission Protected Service ENVIRONMENT

This utility requires the following:
- Cluster Login Environment (see above)
- PROTECTED_ENTITY_ID: This should match the environment.url (without http or https) for a gitlab deployment job for which protect_services has been applied.

For example, if you have a deploy job with

```bash
deploy_foo:
  stage: deploy
  extends: .deploy
  environment:
    name: foo
    url: https://$PROJECT_NAMESPACE-$CI_ENVIRONMENT_SLUG.dhe.duke.edu
```

Then your PROTECTED_ENTITY_ID must be $PROJECT_NAMESPACE-$CI_ENVIRONMENT_SLUG.dhe.duke.edu

### Scale Deployment

This utility allows you to write a job that does a rolling deployment of all deployments labelled with the following labels:
- oa_application_name: Usually the Chart.name, which should match the value of the HELM_CHART_NAME environment variable.
- oa_application_environment: Set with CI_ENVIRONMENT_SLUG during standard
deployments with this utility. Scale deployment jobs should also be defined to run in an existing environment that matches the deploy job for the environment to be scaled.
- oa_scale_deployment_restart: true. This allows you to prevent deployments like databases and redis that should not be
restarted by this job.

You can use example-helm-charts/ruby as a basis. There is an _orbs_helpers.tpl that you
can use in your project helm charts as well.

#### Scale Deployment Environment

This utility uses the following environment variables:
- Cluster Login Environment (see above)
- HELM_CHART_NAME. Required.
- CI_ENVIRONMENT_SLUG. Required.

### Refresh IDMS Token Exchange Long Lived Token

This utility is for applications that make use of the IDMS OAuth [Client Secrets Management](https://authentication.oit.duke.edu/manager/documentation)
system to generate long lived tokens and refresh tokens for the application to use to interact with other APIs that use OAuth for authentication. Long Lived Tokens can be created using the [IDMS Self Service Application](https://idms-web-selfservice.oit.duke.edu/) (see Advanced IT Options).

Applications must store the access token and refresh token for their application in the following gitlab CI variables created with the environment set to each CI_ENVIRONMENT_NAME for the environments that are to be managed with this utility:
- IDMS_TOKEN_EXCHANGE_REFRESH_TOKEN: stores the latest refresh token that is used to refresh the access token using the IDMS
refresh endpoint.
- IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN: stores the latest access token

Applications must also place the IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN into the environment that is loaded into their container environment via a Secret named with the suffix `container-env`, and labeled with the orbs oa_application_name set to the HELM_CHART_NAME, and the orbs oa_application_environment set to the CI_ENVIRONMENT_SLUG. Each deployment uses this container-env secret must have the label oa_long_lived_token_restart=true. The example ruby helm-chart shows how this secret can be created using the REQUIRED_CONTAINER_ENV and OPTIONAL_CONTAINER_ENV gitlab CI variables, and used in an
appropriately labeled deployment.

When the refresh token endpoint is used to refresh the access token, a new refresh token and access token are generated (even if the access token expired). These must be:
- stored as the latest values for the above gitlab CI variables and CI_ENVIRONMENT_NAME
- updated in the container-env secret
When the container-env secret is updated, any deployment with orbs labels oa_application_name HELM_CHART_NAME, oa_application_environment CI_ENVIRONMENT_SLUG, and oa_long_lived_token_restart true must be restarted with a rolling
update to prevent an outage to the environment. This utility accomplishes all of the above.

#### Refresh IDMS Token Exchange Long Lived Token Environment

This utility requires the following environment to run:
- Cluster Login Environment (see above)
- HELM_CHART_NAME. Required.
- CI_ENVIRONMENT_SLUG. Required. Set by gitlab CI if job is run with an environment and name.
- CI_ENVIRONMENT_NAME. Required. Set by gitlab CI if job is run with an environment and name.
- IDMS_TOKEN_EXCHANGE_REFRESH_TOKEN
- IDMS_TOKEN_EXCHANGE_LONG_LIVED_TOKEN
- CI_API_V4_URL: url with /api/v4 to the gitlab where the project is hosted. Set by Gitlab CI if run in a gitlab job
- CI_PROJECT_ID: integer ID for the project. Set by Gitlab CI if run in a gitlab job. Also found on the repo landing page for the project
- One of PRIVATE_TOKEN or BOT_KEY: Stores either a personal access token for a user with Maintainer rights to the project, 
or a group or project access token with api scope and Maintainer rights to the project

### Add and Remove Oauth Redirect URIs

Scripts have been added to allow projects to use the IDM Oauth API to add and remove a redirect_uri to the oauth client registration
for an environment. These can be run before or after the application is
deployed to that environment.

- bin/add_oauth_redirect_uri: concatenates the CI_ENVIRONMENT_URL and OAUTH_SIGN_IN_PATH,
and adds it to the list of redirect_uris for the oauth registration of the specified OIDC_CLIENT_ID.
This is an idempotent action.
- bin/remove_oauth_redirect_uri: concatenates the CI_ENVIRONMENT_URL and OAUTH_SIGN_IN_PATH,
and adds it to the list of redirect_uris for the oauth registration of the specified OIDC_CLIENT_ID.
This is an idempotent action.
- lib/helpers/oauth_api: set of curl wrapper functions that allow other operators against the API. This is used by the bin scripts.

#### Required Oauth Environment
The oauth_api helper library requires the following environment:
- OAUTH_HOST_URL: The url to the IDMS Oauth API. The production url is
idms-web-ws.oit.duke.edu
- OAUTH_USER: This must be a service account that has been blessed to use the Oauth API
- OAUTH_PASSWORD: This must be the password for the service account

The bin scripts require the same environment variables be set in addition
to the variables required by the oauth-api:
- OIDC_CLIENT_ID: the name of the oauth client used by your application and environment
- CI_ENVIRONMENT_URL: the full url to the application
- OAUTH_SIGN_IN_PATH: the path to append to CI_ENVIRONMENT_URL that is set up to handle oauth redirects
to your application

## Using deployment.yml in your project .gitlab-ci.yml

Include deployment.yml using the following in the base of your .gitlab-ci.yml
(or whatever you use) file (note, if you have already included other projects,
just add another entry):

```bash
include:
- project: 'orbs/shared/ci-pipeline-utilities/deployment'
  file: '/deployment.yml'
  ref: 'v3.4.7'
```

You can refer to any [release tag](https://gitlab.dhe.duke.edu/orbs/shared/ci-pipeline-utilities/deployment/-/releases), as long as the
[container](https://gitlab.dhe.duke.edu/orbs/shared/ci-pipeline-utilities/deployment/container_registry) with that tag as the image tag still exists.

#### .deploy

Extend the .deploy function in one or more deployment jobs to deploy an application
to kubernetes. Deploy jobs must be defined in a gitlab ci environment, but you
can use the default or override url in the environment definition to get a link
in the gitlab UI to the running application after it is deployed:

```bash
deploy:
  stage: deploy
  extends: .deploy
  environment:
    name: development
    url: https://$CI_ENVIRONMENT_SLUG-$CI_PROJECT_NAME.$ROUTE_DOMAIN
  only:
    - master
```

#### .decommission

Extend the .decommission function in one or more decommission jobs to decommission
an application deployed by the .deploy function to kubernetes. This should be a
[manual](https://docs.gitlab.com/ee/ci/yaml/#whenmanual) job, to prevent it from
running automatically, and allow users to use the gitlab UI to manually decommission
the application as needed.

```bash
decommission:
  stage: deploy
  extends: .decommission
  environment:
    name: development
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
      when: manual
```

#### .publish_chart

Extend the .publish_chart function in one or more jobs to publish a helm chart
to the chart repository:

```bash
publish_chart:
  stage: publish
  extends: .publish_chart
  only:
    - master
```

#### .promote_candidate

Extend the .promote_candidate function in one or more jobs to create a tag from
a candidate image to a promotion image in the CI_REGISTRY_IMAGE registry:

```bash
promote_image:
  stage: publish
  variables:
    CANDIDATE_IMAGE: "${CI_PROJECT_NAME}:${CI_COMMIT_SHORT_SHA}"
    PROMOTION_IMAGE: "${CI_PROJECT_NAME}:${CI_COMMIT_REF_SLUG}"
  extends: .promote_candidate
  only:
    - master
```

#### .cleanup_repo_image

Extend the .cleanup_repo_image function in one or more jobs to delete a tag from
a given repo:

```bash
clean_ci_image:
  stage: cleanup
  variables:
    DELETE_FROM_REPO="${CI_PROJECT_NAME}-ci"
    DELETE_IMAGE_TAG="${CI_COMMIT_SHORT_SHA}"
  extends: .cleanup_repo_image
  only:
    - branches
```

#### .protect_service

Extend the .protect_service function in a job to deploy protected services:

```bash
deploy_routes:
  stage: deploy
  variables:
    ORG: Duke
    ORGUNIT: Oasis
    EMAIL: me@duke.edu
  extends: .protect_service
  only:
    - master
```

### Caveats:

- do not override the job image, unless you have to (see Using an Older Version)
- do not override the job script section
- do not override the job tag, unless it refers to another docker
executor runner

#### .decommission_protected_service

Extend the .decommission_protected_service function in a job to decommission a
PROTECTED_ENTITY_ID. This should be a manual job so that it is only run when you need
it to run.

```bash
deploy_routes:
  stage: deploy
  variables:
    PROTECTED_ENTITY_ID: ${CI_ENVIRONMENT_SLUG}-${CI_PROJECT_NAME}.${INTERNAL_ROUTE_DOMAIN}
  extends: .decommission_protected_service
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
      when: manual
```

### Caveats:

- do not override the job image, unless you have to (see Using an Older Version)
- do not override the job script section
- do not override the job tag, unless it refers to another docker
executor runner

#### .refresh_long_lived_token

Extend the .refresh_long_lived_token function in one or more jobs to refresh
the long lived token. This should be a manual job:

```bash
refresh_long_lived_token:
  stage: .post
  extends: .refresh_long_lived_token
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: manual
```

#### .add_oauth_redirect_uri

Extend the .add_oauth_redirect_uri function in one or more jobs to add a new redirect_uri to the
client:

```bash
add_oauth_redirect_uri:
  stage: .post
  extends: .add_oauth_redirect_uri
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
```

#### .remove_oauth_redirect_uri

Extend the .remove_oauth_redirect_uri function in one or more jobs to remove a new redirect_uri to the
client:

```bash
remove_oauth_redirect_uri:
  stage: .post
  extends: .remove_oauth_redirect_uri
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
```

## Using an Older Version in your CI Build

In most cases, you will not want to override the job `image`. If you are
experiencing problems with the deployment image, you can try reverting to
a [previously built semantic version](https://gitlab.dhe.duke.edu/orbs/shared/ci-pipeline-utilities/deployment/container_registry) by setting image to:

```bash
image: $CI_REGISTRY/orbs/shared/ci-pipeline-utilities/deployment:$VERSION
```

## Improving this image
To get involved improving this image, consult CONTRIBUTING.md.
