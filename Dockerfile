FROM fedora:38
#force
LABEL name 'ori-deployments'
LABEL base 'fedora:40'

ARG IMAGE_VERSION=unspecified
LABEL release ${IMAGE_VERSION}

ARG CI_COMMIT_SHA=unspecified
LABEL git_commit=${CI_COMMIT_SHA}

ARG CI_PROJECT_URL=unspecified
LABEL git_repository_url=${CI_PROJECT_URL}

ENV DEPLOYMENT_IMAGE_VERSION=v3.4.7
ENV JQ_VERSION=1.7.1
ENV KUBECTL_VERSION=v1.26.13
ENV HELM_VERSION=v3.14.4
ENV BATS_CORE_VERSION='1.11.0'
ENV BATS_SUPPORT_VERSION='0.3.0'
ENV BATS_ASSERT_VERSION='2.1.0'
ENV CFSSL_VERSION='1.4.1'

ADD bin/deploy /usr/local/bin/deploy
ADD bin/generate_shib_certificate /usr/local/bin/generate_shib_certificate
ADD bin/shibboleth_service_proxy /usr/local/bin/shibboleth_service_proxy
ADD bin/scale_deployment.sh /usr/local/bin/scale_deployment.sh
ADD bin/publish_chart /usr/local/bin/publish_chart
ADD bin/decommission /usr/local/bin/decommission
ADD bin/promote_candidate /usr/local/bin/promote_candidate
ADD bin/cleanup_image /usr/local/bin/cleanup_image
ADD bin/generate_gitlab_ci_yml.py /usr/local/bin/generate_gitlab_ci_yml.py
ADD bin/refresh_long_lived_token /usr/local/bin/refresh_long_lived_token
ADD bin/add_oauth_redirect_uri /usr/local/bin/add_oauth_redirect_uri
ADD bin/remove_oauth_redirect_uri /usr/local/bin/remove_oauth_redirect_uri
ADD lib/helpers /usr/local/lib/helpers
ADD test /usr/local/bin/test

ENV HELM_CACHE_HOME="/.helm/.cache/helm"
ENV HELM_CONFIG_HOME="/.helm/.config/helm"
ENV HELM_DATA_HOME="/.helm/.local/share/helm"
ENV HELM_DEBUG="false"
ENV HELM_PLUGINS="/.helm/.local/share/helm/plugins"
ENV HELM_REGISTRY_CONFIG="/.helm/.config/helm/registry.json"
ENV HELM_REPOSITORY_CACHE="/.helm/.cache/helm/repository"
ENV HELM_REPOSITORY_CONFIG="/.helm/.config/helm/repositories.yaml"

RUN dnf install --setopt=tsflags=nodocs -y unzip python-pip findutils wget git tar gzip openssl skopeo python dnf-plugins-core \
    && dnf clean all \
    && pip install ansible oyaml \
    && pip cache purge \
    && curl -L -s -S https://github.com/stedolan/jq/releases/download/jq-${JQ_VERSION}/jq-linux64 -o /usr/local/bin/jq \
    && chmod +x /usr/local/bin/jq \
    && curl -L -s -S https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl \
    && mkdir /.kube && chmod -R 777 /.kube \
    && curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash -s -- --version ${HELM_VERSION} \
    && /usr/local/bin/helm plugin install https://github.com/chartmuseum/helm-push \
    && chmod -R g=rwx /.helm \
    && cd /tmp \
    && curl -L -s -S https://github.com/bats-core/bats-core/archive/v${BATS_CORE_VERSION}.tar.gz -o bats-core-${BATS_CORE_VERSION}.tar.gz \
    && tar -zxvf bats-core-${BATS_CORE_VERSION}.tar.gz \
    && cd bats-core-${BATS_CORE_VERSION} \
    && ./install.sh /usr/local/ \
    && cd /tmp \
    && rm -rf bats-core-${BATS_CORE_VERSION}* \
    && curl -L -s -S https://github.com/ztombol/bats-support/archive/v${BATS_SUPPORT_VERSION}.tar.gz > bats-support-${BATS_SUPPORT_VERSION}.tar.gz \
    && tar -zxvf bats-support-${BATS_SUPPORT_VERSION}.tar.gz \
    && mv bats-support-${BATS_SUPPORT_VERSION} /usr/local/libexec/bats-support \
    && rm bats-support-${BATS_SUPPORT_VERSION}.tar.gz \
    && curl -L -s -S https://github.com/bats-core/bats-assert/archive/v${BATS_ASSERT_VERSION}.tar.gz > bats-assert-${BATS_ASSERT_VERSION}.tar.gz \
    && tar -zxvf bats-assert-${BATS_ASSERT_VERSION}.tar.gz \
    && mv bats-assert-${BATS_ASSERT_VERSION} /usr/local/libexec/bats-assert \
    && rm bats-assert-${BATS_ASSERT_VERSION}.tar.gz \
    && for item in cfssl cfssl-bundle cfssl-certinfo cfssl-newkey cfssl-scan cfssljson mkbundle multirootca; do curl -L https://github.com/cloudflare/cfssl/releases/download/v${CFSSL_VERSION}/${item}_${CFSSL_VERSION}_linux_amd64 > /usr/local/bin/${item}; chmod 755 /usr/local/bin/${item}; done \
    && curl -s "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscli.zip" \
    && unzip awscli.zip \
    && ./aws/install \
    && rm -rf aws awscli.zip \
    && chmod +x /usr/local/bin/deploy \
    && chmod +x /usr/local/bin/test/*
