{{/*
Oasis Standard Application Labels
*/}}
{{- define "orbs.application.labels" -}}
oa_application_name: "{{ .Chart.Name }}"
oa_application_environment: "{{ .Values.environment }}"
{{- end -}}
